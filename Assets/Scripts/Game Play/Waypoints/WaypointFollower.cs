// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core;
using UnityEngine;
using System.Collections.Generic;
using System;
using DI.Core.Events;

namespace DI.Waypoints
{
	public class WaypointFollower : DI_MonoBehaviour
	{
		public WaypointRoute route;

		public Rigidbody rigidBody;
		public bool isMoving = false;
		public Vector3 startPosition;
		public Vector3 endPosition;
		private float startTime;
		private float timeToReachWaypoint;
		private float waitForSecondsTimer = 0.0f;
		private bool needToResetStartTime = false;
		private bool shouldTeleport = false;
		private bool firstWaypoint = true;
		private float speed = 0.0f;
		private bool shouldRotate = false;
		private Quaternion startRotation;
		private Quaternion endRotation;
		public List<Rigidbody> riders;

		public void Update()
		{
			if (!isMoving) {

				// On start up, check if we have an assigned waypoint.
				// If we do, then fill in the values needed.
				// Else, we can't do anything without a waypoint.

				if (route != null) {
					if (firstWaypoint) {
						speed = route.getCurrentWaypoint().getSpeed();
						startPosition = transform.position;
						endPosition = new Vector3(route.getCurrentWaypoint().transform.position.x, route.getCurrentWaypoint().transform.position.y, route.getCurrentWaypoint().transform.position.z);
						timeToReachWaypoint = calcuateSpeed();
						needToResetStartTime = true;
						shouldTeleport = route.getNextWaypoint().getShouldTeleport();
						waitForSecondsTimer = route.getCurrentWaypoint().getWaitForSeconds();
						shouldRotate = route.getCurrentWaypoint().getShouldRotate();
						firstWaypoint = false;
						startRotation = transform.rotation;
						endRotation = route.getCurrentWaypoint().transform.rotation;
					}

					log (DI.Core.Debug.DI_DebugLevel.INFO, this.name + ": Moving to: " + route.getCurrentWaypoint().getName()
								+ " Speed: " + route.getCurrentWaypoint().getSpeed() + " m/s" 
								+ " ETA: " + timeToReachWaypoint 
								+ " Waiting: " + waitForSecondsTimer + " seconds before departing."
								+ " Teleportation Enabled: " + shouldTeleport
								);
					isMoving = true;
				}
				else {
					// We have run out of waypoints
					this.enabled = false;
				}
			}

			// Count down the hold timer.
			if (waitForSecondsTimer > 0.0f) {
				waitForSecondsTimer -= DI_Time.getTimeDelta();
			}
		}

		public void FixedUpdate()
		{
			if (isMoving) {
				// If the hold timer has expired.
				if (waitForSecondsTimer <= 0.0f) {

					// If the waypoint is a teleport waypoint, we can skip the lerping and just move there.
					if (shouldTeleport) {
						log (DI.Core.Debug.DI_DebugLevel.INFO, this.name + ": Teleporting to: " + route.getCurrentWaypoint().getName());
						DI_EventCenter<Waypoint, GameObject>.invoke("OnWaypointExit", route.getCurrentWaypoint(), this.gameObject);
						rigidBody.transform.position = route.getCurrentWaypoint().transform.position;
						if (shouldRotate) {
							rigidBody.transform.localRotation = route.getCurrentWaypoint().transform.localRotation;
						}
						setNextWaypoint();
					}
					else {
						// Due to the wait for seconds setting.
						if (needToResetStartTime) {
							startTime = DI_Time.getTimeSinceLevelLoad();
							needToResetStartTime = false;
							log (DI.Core.Debug.DI_DebugLevel.INFO, this.name + ": Departing: " + route.getCurrentWaypoint().getName());
							DI_EventCenter<Waypoint, GameObject>.invoke("OnWaypointExit", route.getCurrentWaypoint(), this.gameObject);
						}

						float timeSinceStarted = DI_Time.getTimeSinceLevelLoad() - startTime;
						float percentageComplete = timeSinceStarted / timeToReachWaypoint;
						// Move towards the next waypoint.
						Vector3 newPosition = Vector3.Lerp (startPosition, endPosition, percentageComplete);
						try {
							if (!float.IsNaN(newPosition.x) && !float.IsNaN(newPosition.y) && !float.IsNaN(newPosition.z)) {
								rigidBody.MovePosition(newPosition);
							}
						}
						catch (Exception err) {
							log (DI.Core.Debug.DI_DebugLevel.HIGH, err.ToString());
						}

						if (shouldRotate) {
							try {
								transform.rotation = Quaternion.Lerp(startRotation, endRotation, percentageComplete);
							}
							catch (Exception err) {
								log (DI.Core.Debug.DI_DebugLevel.HIGH, err.ToString());
							}
						}

						// If we've finished moving, select the next waypoint.
						if(percentageComplete >= 1.0f)
						{
							setNextWaypoint();
						}
					}
				}
			}
		}

		protected float calcuateSpeed()
		{
			// Time = Distance ÷ Speed
			float distance = (startPosition - endPosition).magnitude;
			return distance / speed;
		}

		protected float getSpeed()
		{
			if (route.getCurrentWaypoint() != null) {
				return route.getCurrentWaypoint().getSpeed();
			}
			else {
				return 0.0f;
			}
		}

		protected bool getShouldRotate()
		{
			if (route.getNextWaypoint() != null) {
				return route.getNextWaypoint().getShouldRotate();
			}
			else {
				return false;
			}
		}

		protected bool getShouldTeleport()
		{
			if (route.getNextWaypoint() != null) {
				return route.getNextWaypoint().getShouldTeleport();
			}
			else {
				return false;
			}
		}

		protected float getWaitForSeconds()
		{
			if (route.getCurrentWaypoint() != null) {
				return route.getCurrentWaypoint().getWaitForSeconds();
			}
			else {
				return 0.0f;
			}
		}

		protected void setNextWaypoint()
		{
			log (DI.Core.Debug.DI_DebugLevel.INFO, this.name + ": Arrived at: " + route.getCurrentWaypoint().getName());
			DI_EventCenter<Waypoint, GameObject>.invoke("OnWaypointExit", route.getCurrentWaypoint(), this.gameObject);

			if (route.getNextWaypoint() != null) {
				shouldTeleport = getShouldTeleport();
				shouldRotate = getShouldRotate();
				waitForSecondsTimer = getWaitForSeconds();
				speed = getSpeed();
				route.routeAdvance();
				startPosition = transform.position;
				endPosition = new Vector3(route.getCurrentWaypoint().transform.position.x, route.getCurrentWaypoint().transform.position.y, route.getCurrentWaypoint().transform.position.z);
				timeToReachWaypoint = calcuateSpeed();
				needToResetStartTime = true;
				startRotation = transform.rotation;
				endRotation = route.getCurrentWaypoint().transform.rotation;

				log (DI.Core.Debug.DI_DebugLevel.INFO, this.name + ": Next Waypoint: " + route.getCurrentWaypoint().getName()
					+ " Speed: " + route.getCurrentWaypoint().getSpeed() + " m/s" 
					+ " ETA: " + timeToReachWaypoint 
					+ " Waiting: " + waitForSecondsTimer + " seconds before departing."
					+ " Teleportation Enabled: " + shouldTeleport
					);
			}
		}

		public void OnDrawGizmos()
		{
			if (route != null) {
				Gizmos.color = route.routeColor;
				Gizmos.DrawLine(transform.position, route.getCurrentWaypoint().transform.position);
			}
		}
		public void OnDrawGizmosSelected()
		{
			if (route != null) {
				Gizmos.DrawWireCube(route.getCurrentWaypoint().transform.position, new Vector3(0.25f, 0.25f, 0.25f));
			}
		}
	}
}