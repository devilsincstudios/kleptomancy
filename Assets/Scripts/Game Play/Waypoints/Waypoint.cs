// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using System;

namespace DI.Waypoints
{
	[Serializable]
	public class Waypoint
	{
		public Transform transform;
		public WaypointData waypointData;

		public string getName()
		{
			return transform.name;
		}

		public float getSpeed()
		{
			return waypointData.speed;
		}

		public bool getShouldTeleport()
		{
			return waypointData.shouldTeleport;
		}

		public bool getShouldRotate()
		{
			return waypointData.shouldRotate;
		}

		public float getWaitForSeconds()
		{
			return waypointData.waitForSeconds;
		}

		public Transform getTransform()
		{
			if (transform != null) {
				return transform.transform;
			}
			else {
				return null;
			}
		}
	}
}