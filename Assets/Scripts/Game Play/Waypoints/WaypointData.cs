// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI.Waypoints
{
	[Serializable]
	public struct WaypointData
	{
		[Tooltip("Should the waypoint follower teleport to this waypoint?")]
		//[Header("Teleport")]
		public bool shouldTeleport;
		
		[Tooltip("How fast, in meters per second, should the follower move when leaving here?")]
		//[Header("Speed")]
		public float speed;
		
		[Tooltip("How long should the follower wait at this waypoint before continuing?")]
		//[Header("Hold Time")]
		public float waitForSeconds;
		
		[Tooltip("Should the follower match this waypoints rotation?)]")]
		//[Header("Rotate")]
		public bool shouldRotate;
	}
}