// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using DI.Waypoints;
using System.Collections;
using UnityEditorInternal;


namespace DI.Editor
{
	[CustomEditor(typeof(WaypointRoute))]
	public class WaypointRouteEditor : UnityEditor.Editor
	{
		private WaypointRoute route;
		private ReorderableList list;

		public void OnEnable()
		{
			route = (WaypointRoute) target;
			if (route.waypoints == null) {
				route.waypoints = new System.Collections.Generic.List<Waypoint>();
			}

			list = new ReorderableList(serializedObject, serializedObject.FindProperty("waypoints"), true, true, true, true);

			GUIStyle headerStyle = new GUIStyle();
			headerStyle.alignment = TextAnchor.MiddleCenter;
			headerStyle.fontStyle = FontStyle.Bold;

			GUIStyle listStyle = new GUIStyle();
			listStyle.alignment = TextAnchor.MiddleCenter;

			list.drawHeaderCallback = (Rect rect) => {
				EditorGUI.LabelField(new Rect(rect.x, rect.y, 50f, EditorGUIUtility.singleLineHeight), "Speed", headerStyle);
				EditorGUI.LabelField(new Rect(rect.x + 55f, rect.y, 75f, EditorGUIUtility.singleLineHeight), "Hold Time", headerStyle);
				EditorGUI.LabelField(new Rect(rect.x + 135f, rect.y, 50f, EditorGUIUtility.singleLineHeight), "Rotate", headerStyle);
				EditorGUI.LabelField(new Rect(rect.x + 190f, rect.y, 50f, EditorGUIUtility.singleLineHeight), "Teleport", headerStyle);
				EditorGUI.LabelField(new Rect(rect.x + 245f, rect.y, rect.width - 220f, EditorGUIUtility.singleLineHeight), "Transform", headerStyle);
			};

			list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
				var element = list.serializedProperty.GetArrayElementAtIndex(index);
				//rect.y += 2;
				EditorGUI.PropertyField(
					new Rect(rect.x, rect.y, 50f, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("waypointData.speed"),
					GUIContent.none
				);

				EditorGUI.PropertyField(
					new Rect(rect.x + 55f, rect.y, 50f, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("waypointData.waitForSeconds"),
					GUIContent.none
				);

				EditorGUI.PropertyField(
					new Rect(rect.x + 135f, rect.y, 50f, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("waypointData.shouldRotate"),
					GUIContent.none
				);
				
				EditorGUI.PropertyField(
					new Rect(rect.x + 190f, rect.y, 50f, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("waypointData.shouldTeleport"),
					GUIContent.none
				);
				
				EditorGUI.PropertyField(
					new Rect(rect.x + 245f, rect.y, rect.width - 220f, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("transform"),
					GUIContent.none
				);
			};

			list.onAddCallback += AddItem;
			list.onRemoveCallback += RemoveItem;
		}


		public void AddItem(ReorderableList list)
		{
			route.waypoints.Add(new Waypoint());
			GameObject newGO = new GameObject("Waypoint: " + (list.count));
			if (route.waypoints.Count > 0) {
				newGO.transform.position = route.waypoints[route.waypoints.Count - 2].transform.position;
			}
			route.waypoints[route.waypoints.Count - 1].transform = newGO.transform;
			newGO.transform.parent = route.transform;
			EditorUtility.SetDirty(target);
		}

		public void RemoveItem(ReorderableList list)
		{
			DestroyImmediate (route.waypoints[list.index].transform.gameObject);
			route.waypoints.RemoveAt(list.index);
			EditorUtility.SetDirty(target);
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();
			list.DoLayoutList();
			serializedObject.ApplyModifiedProperties();

			EditorGUILayout.BeginHorizontal();
			route.currentWaypoint = EditorGUILayout.IntField("Current Waypoint" , route.currentWaypoint);
			if (GUILayout.Button("Advance")) {
				route.routeAdvance();
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			route.isLooped = EditorGUILayout.Toggle("Looped Route" , route.isLooped);
			if (route.isLooped) {
				route.reverseWhenFinished = false;
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			route.reverseWhenFinished = EditorGUILayout.Toggle("Ping Pong Route" , route.reverseWhenFinished);
			if (route.reverseWhenFinished) {
				route.isLooped = false;
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			route.routeColor = EditorGUILayout.ColorField("Route Color" , route.routeColor);
			EditorGUILayout.EndHorizontal();
		}
	}
}