﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;
using System;
using DI.Core.Behaviours;
using UnityEngine;
using DI.Core.Debug;

namespace DI.Waypoints
{
	[Serializable]
	public class WaypointRoute : DI_MonoBehaviour
	{
		public List<Waypoint> waypoints;
		public int currentWaypoint;
		public bool isLooped = true;
		public bool reverseWhenFinished = false;
		public Color routeColor = Color.green;
		private Color randomColor = Color.clear;

		public Waypoint getClosestWaypoint(Vector3 point)
		{
			Waypoint closest = null;
			float waypointDistance = 0f;

			for (int index = 0; index < waypoints.Count; index++) {
				float distance = Vector3.Distance(point, waypoints[index].transform.position);
				if (index == 0) {
					closest = waypoints[index];
					waypointDistance = distance;
				}
				else {
					if (distance < waypointDistance) {
						closest = waypoints[index];
					}
				}
			}

			return closest;
		}

		public Waypoint getNextWaypoint(int waypointId)
		{
			if (waypoints.Count > (waypointId + 1)) {
				return waypoints[waypointId + 1];
			}
			else {
				if (isLooped) {
					return waypoints[0];
				}
				if (reverseWhenFinished) {
					return waypoints[waypoints.Count - 1];
				}
			}

			return null;
		}

		public Waypoint getNextWaypoint()
		{
			if (waypoints.Count > (currentWaypoint + 1)) {
				return waypoints[currentWaypoint + 1];
			}
			else {
				if (isLooped) {
					return waypoints[0];
				}
				if (reverseWhenFinished) {
					return waypoints[waypoints.Count - 1];
				}
			}

			return null;
		}

		public Waypoint getPreviousWaypoint(int waypointId)
		{
			if ((waypointId - 1) > 0) {
				return waypoints[waypointId - 1];
			}
			else {
				if (isLooped) {
					return waypoints[0];
				}
				if (reverseWhenFinished) {
					return waypoints[waypoints.Count - 1];
				}
			}

			return null;
		}

		public Waypoint getPreviousWaypoint()
		{
			if ((currentWaypoint - 1) > 0) {
				return waypoints[currentWaypoint - 1];
			}
			else {
				if (isLooped) {
					return waypoints[0];
				}
				if (reverseWhenFinished) {
					return waypoints[waypoints.Count - 1];
				}
			}

			return null;
		}

		public int getWaypointId(Waypoint waypoint)
		{
			return waypoints.IndexOf(waypoint);
		}

		public Waypoint getCurrentWaypoint()
		{
			if (waypoints != null) {
				return waypoints[currentWaypoint];
			}
			return null;
		}

		public void addWaypoint(Waypoint newWaypoint)
		{
			waypoints.Add(newWaypoint);
		}

		public void insertWaypoint(Waypoint newWaypoint, int index)
		{
			waypoints.Insert(index, newWaypoint);
		}

		public void removeWaypoint(Waypoint removedWaypoint)
		{
			waypoints.Remove(removedWaypoint);
		}

		public void removeWaypoint(int index)
		{
			waypoints.RemoveAt(index);
		}

		public void routeAdvance()
		{
			if ((currentWaypoint + 1) == waypoints.Count) {
				if (isLooped) {
					currentWaypoint = 0;
				}
				if (reverseWhenFinished) {
					currentWaypoint = 0;
					reverseRoute();
				}
			}
			else {
				currentWaypoint++;
			}
		}

		public void reverseRoute()
		{
			waypoints.Reverse();
		}

		public void OnDrawGizmos()
		{
			if (randomColor == Color.clear) {
				randomColor = DI_Debug.RandomColor();
			}

			if (waypoints.Count > 0) {
				for (int index = 0; index < waypoints.Count; index++) {
					Gizmos.color = routeColor;
					if (index + 1 < waypoints.Count) {
						if (waypoints[index] != null) {
							Gizmos.DrawLine(waypoints[index].transform.transform.position, waypoints[index + 1].transform.transform.position);
						}
					}
					else {
						if (isLooped) {
							if (waypoints[index] != null) {
								Gizmos.DrawLine(waypoints[index].transform.transform.position, waypoints[0].transform.transform.position);	
							}
						}
					}

					Gizmos.color = randomColor;
					Gizmos.DrawWireCube(waypoints[index].transform.transform.position, new Vector3(0.1f, 0.1f, 0.1f));
				}
			}
		}

		public void OnDrawGizmosSelected()
		{
			Gizmos.color = routeColor;
			if (waypoints.Count > 0) {
				if (getNextWaypoint() != null) {
					Gizmos.DrawWireSphere(getNextWaypoint().transform.position, 1.0f);
				}
			}
		}
	}
}