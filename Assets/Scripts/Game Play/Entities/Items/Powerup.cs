// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Controllers;
using DI.Entities.Properties;
using UnityEngine;
using System.Collections;
using DI.SFX;
using DI.Core.Pooling;

namespace DI.Entities.Items
{
	public class Powerup : DI_MonoBehaviour
	{
		protected DI.Controllers.Player player;

		[Header("Item Properties")]
		public bool canUse = true;
		public MeshRenderer itemRenderer;
		public Collider itemCollider;
		[Header("Item Despawn Settings")]
		public bool despawns = true;
		public float lifeTime = 10f;
		[Header("Item Respawn Settings")]
		public bool respawns = false;
		public float respawnTimer = 15f;
		[Header("Spawn FX Settings")]
		public DI_SFXProperty spawnSFX;
		public bool hasSpawnVFX = false;
		public GameObject spawnVFX;
		[Header("Collect FX Settings")]
		public DI_SFXProperty collectSFX;
		public bool hasCollectVFX = false;
		public GameObject collectVFX;
		[Header("Despawn FX Settings")]
		public DI_SFXProperty despawnSFX;
		public bool hasDespawnVFX = false;
		public GameObject despawnVFX;

		public void OnEnable()
		{
			player = GameObject.Find("Player").GetComponent<DI.Controllers.Player>();

			if (despawns) {
				Debug.Log("Despawning in: " + lifeTime);
				StartCoroutine(despawnItem());
			}
		}

		public IEnumerator respawnItem()
		{
			Debug.Log("respawning in: " + respawnTimer);
			yield return new WaitForSeconds(respawnTimer);
			onSpawn();
		}

		public IEnumerator despawnItem()
		{
			yield return new WaitForSeconds(lifeTime);
			onDespawn();
		}

		public virtual void onUse()
		{
			canUse = false;
			if (hasCollectVFX) {
				GameObject vfx = DI_PoolManager.getPooledObject(collectVFX);
				vfx.transform.position = transform.position;
				vfx.SetActive(true);
			}

			if (collectSFX.hasSFX) {
				DI_SFX.playClipAtPoint(gameObject, transform.position, collectSFX.sfxs[UnityEngine.Random.Range(0, collectSFX.sfxs.Count)]);
			}
			if (respawns) {
				StartCoroutine(respawnItem());
			}

			if (respawns) {
				itemCollider.enabled = false;
				itemRenderer.enabled = false;
			}
			else {
				gameObject.SetActive(false);
			}
		}

		public virtual void onSpawn()
		{
			canUse = true;
			if (hasSpawnVFX) {
				GameObject vfx = DI_PoolManager.getPooledObject(spawnVFX);
				vfx.transform.position = transform.position;
				vfx.SetActive(true);
			}

			if (spawnSFX.hasSFX) {
				DI_SFX.playClipAtPoint(gameObject, transform.position, spawnSFX.sfxs[UnityEngine.Random.Range(0, spawnSFX.sfxs.Count)]);
			}
			itemCollider.enabled = true;
			itemRenderer.enabled = true;
		}

		public virtual void onDespawn()
		{
			canUse = false;
			if (hasDespawnVFX) {
				GameObject vfx = DI_PoolManager.getPooledObject(despawnVFX);
				vfx.transform.position = transform.position;
				vfx.SetActive(true);
			}

			if (despawnSFX.hasSFX) {
				DI_SFX.playClipAtPoint(gameObject, transform.position, despawnSFX.sfxs[UnityEngine.Random.Range(0, despawnSFX.sfxs.Count)]);
			}

			if (respawns) {
				itemCollider.enabled = false;
				itemRenderer.enabled = false;
			}
			else {
				gameObject.SetActive(false);
			}
		}
	}
}