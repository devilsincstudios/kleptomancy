// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Entities.Properties;
using UnityEngine;
using System;
using DI.Core.Behaviours;
using DI.Controllers;
using DI.SFX;

namespace DI.Entities.Items
{
	[Serializable]
	public class ManaPotion : Powerup
	{
		[Header("Mana Potion Settings")]
		public float manaGainPercent = 10f;
		public float manaRegenMultipler = 5f;
		public float manaRegenLength = 5f;

		public override void onUse()
		{
			if (canUse) {
				Debug.Log("On Use!");
				player.addMana(player.getMaxMana() * (manaGainPercent / 100));
				player.setManaRegenMultipler(manaRegenMultipler, manaRegenLength);
				base.onUse();
			}
		}

		public override void onSpawn()
		{
			base.onSpawn();
		}
	}
}
