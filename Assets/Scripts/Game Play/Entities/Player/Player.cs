﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Entities.Properties;
using UnityEngine;
using System;
using DI.Entities.Player;

namespace DI.Entities.Player
{
    [Serializable]
    public class Player : DI_PlayerEntity
	{
		public DI_MovementProperty walkingSpeed;
		public DI_MovementProperty runningSpeed;
		public DI_MovementProperty sprintingSpeed;
		public DI_MovementProperty crouchedWalkSpeed;
		public DI_MovementProperty crouchedRunSpeed;
		public DI_JumpProperty jumpSettings;
		public DI_StaminaProperty staminaSettings;

		public float strafeSpeedPenalty = 0.5f;
		public float stepHeight = 0.4f;
		//public float strideLength = 0.1f;
    }
}
