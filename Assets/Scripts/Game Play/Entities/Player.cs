﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Entities.Properties;
using UnityEngine;
using System;
using DI.Entities.Player;

namespace DI.Entities.Player
{
    [Serializable]
    public class Player : DI_PlayerEntity
    {
        [SerializeField]
        public DI_ManaProperty manaProperties;
    }
}
