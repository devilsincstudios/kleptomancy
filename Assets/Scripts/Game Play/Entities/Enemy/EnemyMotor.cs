﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using DI.Entities;
using DI.Core.Behaviours;

namespace DI.Entities.Enemy
{
	[Serializable]
	public class EnemyMotor : DI_MonoBehaviour
	{
		public Enemy settings;
		public bool initalized = false;
		public NavMeshAgent agent;
		public bool isGrounded = true;
		public float distanceFromGround = 0.0f;

		public void initalize()
		{
			initalized = true;
			settings = GameObject.Instantiate(settings);
			agent = GetComponent<NavMeshAgent>();
		}

		public void OnEnable()
		{
			if (!initalized) {
				initalize();
			}
		}

		public void move(Vector3 target, float speed)
		{
			agent.destination = target;
			agent.speed = speed;
		}

		public void LateUpdate()
		{
			//Debug.Log("Has Path:" + agent.hasPath);
			//NavMeshPath path = new NavMeshPath();
			//agent.CalculatePath(agent.destination, path);
			//Debug.Log(path.status.ToString());

//			if (settings.getEntity() != null) {
//				RaycastHit hit;
//
//				if (Physics.Raycast(settings.getEntity().transform.position + (Vector3.up * 0.1f), -settings.getEntity().transform.up, out hit)) {
//					if ((hit.distance - 0.1f) <= settings.groundCheckDistance) {
//						isGrounded = true;
//					}
//					else {
//						isGrounded = false;
//					}
//					distanceFromGround = (hit.distance - 0.1f);
//				}
//				else {
//					isGrounded = false;
//					distanceFromGround = -1f;
//				}
//			}
		}
	}
}