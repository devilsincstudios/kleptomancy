﻿
// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using Zenject;

using DI.Core.Behaviours;
using DI.Mechanics.Weapons;
using DI.Entities.Limbs;
using DI.Entities.Player;
using DI.Core.Events;
using DI.SFX;
using DI.Core;
using DI.Mechanics.Loot;
using DI.Core.Pooling;

using System.Collections.Generic;

namespace DI.Entities.Enemy
{
	[RequireComponent(typeof(EnemyMotor))]
	[RequireComponent(typeof(EnemyAnimator))]
	public class EnemyController : DI_MonoBehaviour
	{
		public Enemy settings;
		public EnemyAnimator enemyAnimator;
		public EnemyMotor enemyMotor;
		public GameObject player;
		public NavMeshAgent agent;
		protected bool initalized = false;
		public float aiDelay = 0.2f;

		// Serialized for debug view
		[SerializeField]
		protected float currentHealth = 0f;
		public float attackDelay;
		public float attackDelayRemaining;
		public float movementSpeed = 0f;

		public float periodicDamageTimeRemaining = 0.0f;
		public float periodicDamageTickDamage = 0.0f;
		public GameObject fireEffects;
		public float fireSpreadRange = 0.5f;
		public float fireSpreadChange = 20f;
		public float fireSpeedBonusMultipler = 1.5f;
		public LayerMask fireSpreadMask;
		public Collider enemyCollider;
		public Limb chest;

		public void initalize()
		{
			initalized = true;
			enemyAnimator = GetComponent<EnemyAnimator>();
			enemyMotor = GetComponent<EnemyMotor>();

			player = GameObject.FindGameObjectWithTag("Player");
			agent = GetComponent<NavMeshAgent>();
			settings = Instantiate(settings);
			settings.setEntityBody(this.GetComponent<Rigidbody>());
			attackDelay = settings.attackDelay;
		}

		public void OnEnable()
		{
			if (!initalized) {
				initalize();
			}
			// Make our own copy of the settings so we can change them as needed without editing the master copy.
			settings.onSpawn();
			DI_EventCenter<Enemy>.invoke("OnSpawn", settings);
			settings.setHealth(settings.getMaxHealth());
			enemyAnimator.ragdoll(false);
			agent.enabled = true;
			currentHealth = settings.getHealth();
			StartCoroutine(updateNavTarget());
			StartCoroutine(periodicDamage());
			movementSpeed = settings.getMaxMovementSpeed();
		}

		public bool isAlive()
		{
			return (settings.getHealth() > 0f);
		}

		public IEnumerator periodicDamage()
		{
			while (true) {
				if (periodicDamageTimeRemaining > 0f) {
					periodicDamageTimeRemaining -= 1f;
					enemyAnimator.setBool("Burning", true);
					spreadFire();
					if (settings.getHealth() > 0f) {
						takeDamage(periodicDamageTickDamage);
					}
				}
				else {
					movementSpeed = settings.getMaxMovementSpeed();
					fireEffects.SetActive(false);
					enemyAnimator.setBool("Burning", false);
				}
				yield return new WaitForSecondsRealtime(1f);
			}
		}

		public IEnumerator removeBody()
		{
			yield return new WaitForSecondsRealtime(settings.bodyLingerTime);
			fireEffects.SetActive(false);
			StopCoroutine(periodicDamage());
			gameObject.SetActive(false);
		}

		public void spreadFire()
		{
			var colliders = Physics.OverlapSphere(transform.position, fireSpreadRange, fireSpreadMask);
			for (int index = 0; index < colliders.Length; index++) {
				if (colliders[index].CompareTag("Enemy")) {
					if (colliders[index].gameObject.GetInstanceID() != gameObject.GetInstanceID()) {
						EnemyController controller = colliders[index].GetComponent<EnemyController>();
						if (controller != null) {
							controller.ignite(periodicDamageTickDamage, periodicDamageTimeRemaining);
						}
					}
				}
			}
		}

		public void ignite(float tickDamage, float timeRemaining)
		{
			fireEffects.SetActive(true);
			if (periodicDamageTickDamage < tickDamage) {
				periodicDamageTickDamage = tickDamage;
			}
			if (periodicDamageTimeRemaining < timeRemaining) {
				periodicDamageTimeRemaining = timeRemaining;
			}
			movementSpeed = settings.getMaxMovementSpeed() * fireSpeedBonusMultipler;
		}

		/// <summary>
		/// When you know next to nothing about how the damage came in just that you need to deal damage.
		/// </summary>
		/// <param name="amount">Amount.</param>
		/// <param name="ammo">Ammo.</param>
		public void takeDamage(float amount, WeaponDamageTypes type = WeaponDamageTypes.normal)
		{
			if (settings.getHealth() > 0f) {
				settings.removeHealth(amount);
				currentHealth = settings.getHealth();
				DI_EventCenter<Enemy, WeaponDamageTypes, float>.invoke("OnHit", settings, type, amount);
				if (periodicDamageTimeRemaining <= 0.0f) {
					enemyAnimator.setTrigger("Hit");
				}
			}
			// Entity is dead
			if (settings.getHealth() <= 0f) {
				settings.onDespawn();
				DI_EventCenter<Enemy>.invoke("OnDespawn", settings);
				// The enemy died and we have no idea how, give credit for a chest hit.
				DI_EventCenter<Enemy, Limb>.invoke("OnKill", settings, chest);
				StopCoroutine(updateNavTarget());
				agent.enabled = false;
				enemyAnimator.ragdoll(true);
				StartCoroutine(removeBody());
			}
			else {
				DI_SFX.playRandomClipAtPoint(gameObject, transform.position, Vector3.Distance(player.transform.position, transform.position), settings.painSoundFX);
			}
		}

		/// <summary>
		/// When you don't know which part got hit, assume it was the chest
		/// In the case of an explosion, its going to hit just about everything so deal with it as a single damage value.
		/// </summary>
		/// <param name="amount">Amount.</param>
		/// <param name="type">Type.</param>
		public void takeDamage(float amount, LootAmmo ammo)
		{
			takeDamage(chest, amount, ammo);
		}

		/// <summary>
		/// Causes the entity to take damage should be used whenever a limb is known.
		/// </summary>
		/// <param name="bodyPart">Body part.</param>
		/// <param name="amount">Amount.</param>
		/// <param name="ammo">Ammo.</param>
		public void takeDamage(Limb bodyPart, float amount, LootAmmo ammo)
		{
			if (bodyPart.isHead()) {
				amount *= settings.headDamage;
			}
			else if (bodyPart.isBody()) {
				amount *= settings.bodyDamage;
			}
			else {
				amount *= settings.limbDamage;
			}

			if (Random.Range(0, 100) <= ammo.ignitionChance) {
				ignite(Random.Range(ammo.minBurnDamage, ammo.maxBurnDamage), ammo.burnTime);
			}

			if (ammo.bulletHitSounds.hasSFX) {
				SFX.DI_SFX.playClipAtPoint(gameObject, bodyPart.transform.position, ammo.bulletHitSounds.sfxs[UnityEngine.Random.Range(0, ammo.bulletHitSounds.sfxs.Count)]);
			}
			if (ammo.bulletHitEffects != null) {
				GameObject hitEffect = DI_PoolManager.getPooledObject(ammo.bulletHitEffects);
				hitEffect.transform.position = bodyPart.transform.position;
				hitEffect.SetActive(true);
			}

			if (settings.getHealth() > 0f) {
				settings.removeHealth(amount);
				currentHealth = settings.getHealth();
				if (periodicDamageTimeRemaining <= 0.0f) {
					enemyAnimator.setTrigger("Hit");
				}
				DI_SFX.playRandomClipAtPoint(gameObject, transform.position, Vector3.Distance(player.transform.position, transform.position), settings.painSoundFX);
				DI_EventCenter<Enemy, Limb>.invoke("OnHit", settings, bodyPart);
			}

			// Entity is dead
			if (settings.getHealth() <= 0f) {
				settings.onDespawn();
				DI_EventCenter<Enemy>.invoke("OnDespawn", settings);
				StopCoroutine(updateNavTarget());
				agent.enabled = false;
				enemyAnimator.ragdoll(true);
				StartCoroutine(removeBody());
				DI_EventCenter<Enemy, Limb>.invoke("OnKill", settings, bodyPart);
			}
		}

		public void LateUpdate()
		{
			if (Vector3.Distance(transform.position, player.transform.position) <= settings.attackRange) {
				if (attackDelayRemaining <= 0.0f) {
					enemyAnimator.setTrigger("Attack");
					StartCoroutine(attack());
					attackDelayRemaining = attackDelay;
				}
			}

			if (attackDelayRemaining > 0.0f) {
				attackDelayRemaining -= DI.Core.DI_Time.getTimeDelta();
			}

			enemyAnimator.setBool("Falling", !enemyMotor.isGrounded);
		}

		public IEnumerator attack()
		{
			yield return new WaitForSecondsRealtime(settings.attackAnimationDelay);

			Vector3 castPosition = new Vector3(transform.position.x, transform.position.y + agent.height / 2, transform.position.z);

			var colliders = Physics.OverlapBox(castPosition + (Vector3.forward * settings.attackRange), new Vector3(settings.attackRange, 1f, settings.attackRange));
			for (int index = 0; index < colliders.Length; index++) {
				if (colliders[index].CompareTag("Player")) {
					DI_EventCenter<Enemy, Vector3>.invoke("OnHitPlayer", settings, colliders[index].ClosestPointOnBounds(transform.position));
				}
			}
			if (settings.attackSoundFX.hasSFX) {
				DI_SFX.playRandomClipAtPoint(gameObject, transform.position, settings.attackSoundFX);
			}
		}

		public IEnumerator updateNavTarget()
		{
			yield return new WaitForSeconds(Random.Range(0, aiDelay));
			while (settings.getHealth() > 0f) {
				yield return new WaitForSeconds(aiDelay);
				if (agent.enabled) {
					enemyMotor.move(player.transform.position, movementSpeed);
					// 2f = run for animations
					enemyAnimator.speed = 2f;
				}
				else {
					break;
				}
			}
		}
	}
}