﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using System.Collections.Generic;
using DI.Core.StateMachine;

using Zenject;

namespace DI.Entities
{
	[RequireComponent(typeof(Animator))]
	public class EntityAnimator : MonoBehaviour
	{
		[HideInInspector]
		public Animator animator;
		private Dictionary<HumanBodyBones, Transform> bonePositions;

		public bool startAsRagdoll = false;
		public Transform rootBone;
		private List<Rigidbody> ragdollComponents;

		public void OnEnable()
		{
			animator = this.GetComponent<Animator>();
			bonePositions = new Dictionary<HumanBodyBones, Transform>();
			// Torso
			bonePositions.Add(HumanBodyBones.Head, animator.GetBoneTransform(HumanBodyBones.Head));
			bonePositions.Add(HumanBodyBones.Chest, animator.GetBoneTransform(HumanBodyBones.Chest));
			bonePositions.Add(HumanBodyBones.Spine, animator.GetBoneTransform(HumanBodyBones.Spine));
			bonePositions.Add(HumanBodyBones.Hips, animator.GetBoneTransform(HumanBodyBones.Hips));

			// Arms
			bonePositions.Add(HumanBodyBones.LeftLowerArm, animator.GetBoneTransform(HumanBodyBones.LeftLowerArm));
			bonePositions.Add(HumanBodyBones.LeftUpperArm, animator.GetBoneTransform(HumanBodyBones.LeftUpperArm));
			bonePositions.Add(HumanBodyBones.RightLowerArm, animator.GetBoneTransform(HumanBodyBones.RightLowerArm));
			bonePositions.Add(HumanBodyBones.RightUpperArm, animator.GetBoneTransform(HumanBodyBones.RightUpperArm));
			bonePositions.Add(HumanBodyBones.LeftHand, animator.GetBoneTransform(HumanBodyBones.LeftHand));
			bonePositions.Add(HumanBodyBones.RightHand, animator.GetBoneTransform(HumanBodyBones.RightHand));

			// Legs
			bonePositions.Add(HumanBodyBones.LeftLowerLeg, animator.GetBoneTransform(HumanBodyBones.LeftLowerLeg));
			bonePositions.Add(HumanBodyBones.LeftUpperLeg, animator.GetBoneTransform(HumanBodyBones.LeftUpperLeg));
			bonePositions.Add(HumanBodyBones.RightLowerLeg, animator.GetBoneTransform(HumanBodyBones.RightLowerLeg));
			bonePositions.Add(HumanBodyBones.RightUpperLeg, animator.GetBoneTransform(HumanBodyBones.RightUpperLeg));
			bonePositions.Add(HumanBodyBones.LeftFoot, animator.GetBoneTransform(HumanBodyBones.LeftFoot));
			bonePositions.Add(HumanBodyBones.RightFoot, animator.GetBoneTransform(HumanBodyBones.RightFoot));

			var rigidBodies = rootBone.GetComponentsInChildren<Rigidbody>();
			ragdollComponents = new List<Rigidbody>();

			for (int index = 0; index < rigidBodies.Length; index++) {
				ragdollComponents.Add(rigidBodies[index]);
			}

			ragdoll(startAsRagdoll);
		}

		public void ragdoll(bool enabled)
		{
			for (int index = 0; index < ragdollComponents.Count; index++) {
				ragdollComponents[index].isKinematic = !enabled;
				ragdollComponents[index].useGravity = enabled;
			}
			animator.enabled = !enabled;
		}

		public void setBool(string name, bool value)
		{
			animator.SetBool(name, value);
		}

		public Transform getBoneTransform(HumanBodyBones bone)
		{
			Transform boneTransform;
			if (!bonePositions.TryGetValue(bone, out boneTransform))
			{
				Debug.Log("Asked for a bone that is not being recorded. " + bone.ToString());
				boneTransform = null;
			}
			return boneTransform;
		}
	}
}