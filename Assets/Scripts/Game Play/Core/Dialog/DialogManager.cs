// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Collections;

using DI.Core.Events;
using DI.Core.Debug;
using DI.Core.Behaviours;
using DI.Core.GameState;
using DI.Core.GameState.GUI;
using DI.Core.Input;
using DI.Controllers;
using DI.Dialog.UI;
using UnityEngine.SceneManagement;

namespace DI.Dialog
{
	[AddComponentMenu("Devil's Inc Studios/Managers/Dialog")]
	public class DialogManager : DI_MonoBehaviourSingleton<DialogManager>
	{
		[Header("GUI Element Setup")]
		public Image portrait;
		public RawImage portraitRender;
		public Text textBox;
		public Canvas dialogCanvas;
		public Text namePlate;
		public Text namePlateRender;
		public Text dialogNextInfo;

		[Header("Player Setting - Will auto populate at runtime")]
		public Player player;

		[HideInInspector]
		public bool useTypeWritter;
		protected DI_TypeWritter typeWritter;

		[Header("These are set by events and do not need to be modified")]
		public DI_DialogScript currentScript;

		[HideInInspector]
		public bool isReadyToAdvance = false;
		protected bool isPlayingDialog = false;

		[Header("Actor Setup")]
		protected List<DI_DialogActorGameObjects> actors;
		protected List<GameObject> cleanUpAvatars;

		public void Awake()
		{
			// If we have another instance active, destroy it and replace it with this one.
			if (instance != null) {
				Destroy(instance);
			}
			makeSingleton(this);
		}

		public void OnEnable()
		{
			typeWritter = GetComponent<DI_TypeWritter>();
			dialogNextInfo.gameObject.SetActive(false);
			player = GameObject.Find("Player").GetComponent<Player>();
			actors = new List<DI_DialogActorGameObjects>();
			cleanUpAvatars = new List<GameObject>();
			SceneManager.sceneLoaded += OnLevelFinishedLoading;
			// TODO figure out how to change the [SPACE] to the registered key, Should only be an issue with PC builds though.
		}

		public void Update()
		{
			if (!isReadyToAdvance) {
				if (player != null) {
					if (player.isInteracting) {
						isReadyToAdvance = true;
					}
				}
			}
		}

		public void OnLevelFinishedLoading(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
		{
			player = GameObject.Find("Player").GetComponent<Player>();
		}

		#region Dialog Internals
		protected void changeSpeaker(string speakersName, Sprite newSpeakerPortrait)
		{
			#if DEBUG
			DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Change Speaker - Sprite: " + speakersName);
			#endif
			namePlate.text = speakersName;
			portrait.sprite = newSpeakerPortrait;
			portrait.gameObject.SetActive(true);
			portraitRender.gameObject.SetActive(false);
		}
		protected void changeSpeaker(string speakersName, RenderTexture newSpeakerPortrait)
		{
			#if DEBUG
			DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Change Speaker - Render: " + speakersName);
			#endif
			namePlateRender.text = speakersName;
			portraitRender.texture = newSpeakerPortrait;
			portrait.gameObject.SetActive(false);
			portraitRender.gameObject.SetActive(true);
		}
		protected void updateDialogText(string message, bool useTypeWritter)
		{
			if (useTypeWritter) {
				typeWritter.typeMessage(message);
			}
			else {
				textBox.text = message;
			}
		}
		protected void readyActor(string actorName)
		{
			#if DEBUG
			DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Ready Actor");
			#endif
			for (int actorIndex = 0; actorIndex < actors.Count; actorIndex++) {
				if (actors[actorIndex].actor.actorName == actorName) {
					if (actors[actorIndex].actorAvatar != null) {
						if (!actors[actorIndex].actorAvatar.activeSelf) {
							actors[actorIndex].actorAvatar.SetActive(true);
							#if DEBUG
							DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Adding actor to cleanup list - readyActor()");
							#endif
							cleanUpAvatars.Add(actors[actorIndex].actorAvatar);
						}
					}
				}
			}
		}
		protected GameObject getActorAvatar(string actorName)
		{
			for (int actorIndex = 0; actorIndex < actors.Count; actorIndex++) {
				if (actors[actorIndex].actor.actorName == actorName) {
					if (actors[actorIndex].actorAvatar != null) {
						if (!actors[actorIndex].actorAvatar.activeSelf) {
							actors[actorIndex].actorAvatar.SetActive(true);
							#if DEBUG
							DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Adding actor to cleanup list - getActorAvatar()");
							#endif
							cleanUpAvatars.Add(actors[actorIndex].actorAvatar);
						}
					}
					return actors[actorIndex].actorAvatar;
				}
			}
			return null;
		}
		protected IEnumerator playScript()
		{
			for (int currentLine = 0; currentLine < currentScript.lines.Count; currentLine++) {		
				DI_DialogLine line = currentScript.lines[currentLine];
				if (!line.Equals(null)) {
					if (line.actor == null) {
						DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Actor is null.");
					}
					else {
						#if DEBUG
						DI_Debug.writeLog(DI_DebugLevel.ENGINE,""+line.actor);
						#endif
						readyActor(line.actor.actorName);
						if (line.actor.actorRender == null) {
							changeSpeaker(line.actor.actorName, line.actor.actorPortrait);
						}
						else {
							changeSpeaker(line.actor.actorName, line.actor.actorRender);
						}
					}
					if (line.hasAnimation) {
						GameObject avatar = getActorAvatar(line.actor.actorName);
						if (avatar != null) {
							avatar.GetComponent<Animator>().Play(line.animationName);
							#if DEBUG
							DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Attempting to play animation: " + line.animationName);
							#endif
						}
					}

					if (line.firesEvent) {
						if (line.eventName != null) {
							#if DEBUG
							DI_Debug.writeLog(DI_DebugLevel.ENGINE,"Playing Event: " + line.eventName + " with a delay of " + line.eventDelay);
							#endif
							yield return new WaitForSeconds(line.eventDelay);
							DI_EventCenter.invoke(line.eventName);
						}
					}

					if (line.hasVoiceOver) {
						DI_EventCenter<AudioClip, float>.invoke("OnVoiceOver", line.voiceOverClip, line.voiceOverVolume);
					}

					#if DEBUG
					DI_Debug.writeLog(DI_DebugLevel.ENGINE,line.line);
					#endif
					typeWritter.typeMessage(line.line);

					// Wait for the player to press the next button [Default space]
					while (!isReadyToAdvance) {
						dialogNextInfo.gameObject.SetActive(true);
						yield return new WaitForSeconds(0.1f);
					}
					dialogNextInfo.gameObject.SetActive(false);
					isReadyToAdvance = false;
					typeWritter.stopTyping();
				}
			}

			endScript();
			hideDialog();
		}
		#endregion

		#region Public methods
		public void startScript(DI_DialogScript script)
		{
			currentScript = script;
			startScript();
		}
		public void startScript()
		{
			if (!isPlayingDialog) {
				DI_EventCenter<DI_DialogScript>.invoke("OnDialogStart", currentScript);
				showDialog();
				StartCoroutine("playScript");
			}
		}
		public void endScript()
		{
			isPlayingDialog = false;
			DI_EventCenter<DI_DialogScript>.invoke("OnDialogEnd", currentScript);
			if (cleanUpAvatars.Count > 0) {
				foreach (GameObject avatar in cleanUpAvatars.ToArray()) {
					avatar.SetActive(false);
				}
			}
			cleanUpAvatars.Clear();
			actors.Clear();
			typeWritter.stopTyping();
			StopCoroutine("playScript");
			hideDialog();
		}
		public void showDialog()
		{
			dialogCanvas.gameObject.SetActive(true);
		}
		public void hideDialog()
		{
			dialogCanvas.gameObject.SetActive(false);
		}
		#endregion
	}
}
