// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Core.Debug;

namespace DI.Dialog
{
	[AddComponentMenu("Devil's Inc Studios/Dialog/Dialog Trigger")]
	public class DialogTrigger : DI_MonoBehaviour
	{
		[Header("Script Setup")]
		public DI_DialogScript script;

		[Header("Trigger Setup")]
		public DI_DialogTriggerTypes triggerType;
		public float dialogTimer = 0.0f;
		public string triggerTagFilter;

		public void OnEnable()
		{
			if (triggerType == DI_DialogTriggerTypes.ON_START) {
				StartCoroutine("delayedStart");
			}
			if (triggerType == DI_DialogTriggerTypes.ON_EVENT) {
				DI_EventCenter<string, DI_DialogEventTypes>.addListener("OnDialogEvent", handleDialogEvent);
			}
			if (triggerType == DI_DialogTriggerTypes.ON_TIMER) {
				StartCoroutine("delayedStart");
			}
		}

		public void OnDisable()
		{
			if (triggerType == DI_DialogTriggerTypes.ON_EVENT) {
				DI_EventCenter<string, DI_DialogEventTypes>.removeListener("OnDialogEvent", handleDialogEvent);
			}
		}

		public void OnTriggerEnter(Collider other) {
			if (triggerType == DI_DialogTriggerTypes.ON_TRIGGER) {
				if (other.tag == triggerTagFilter) {
					DialogManager.instance.startScript(script);
				}
			}
		}

		public void handleDialogEvent(string dialog, DI_DialogEventTypes eventType) {
			if (dialog == script.name) {
				switch (eventType) {
					case DI_DialogEventTypes.EVENT_START_DIALOG:
						DialogManager.instance.startScript(script);
						break;
					case DI_DialogEventTypes.EVENT_END_DIALOG:
						DialogManager.instance.endScript();
						break;
				}
			}
		}

		public IEnumerator delayedStart()
		{
			yield return new WaitForSeconds(dialogTimer);
			DialogManager.instance.startScript(script);
		}
	}
}