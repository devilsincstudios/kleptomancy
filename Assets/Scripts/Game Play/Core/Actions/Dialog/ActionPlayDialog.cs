﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Dialog;

namespace DI.Core.Actions
{
	public class ActionPlayDialog : Action
	{
		public DI_DialogScript script;

		public override ActionTypes getType()
		{
			return ActionTypes.dialog_play;
		}
		public override void doAction()
		{
			DialogManager.instance.startScript(script);
		}
	}
}