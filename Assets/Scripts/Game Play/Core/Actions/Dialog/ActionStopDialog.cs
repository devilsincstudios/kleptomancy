﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Dialog;

namespace DI.Core.Actions
{
	public class ActionStopDialog : Action
	{
		public override ActionTypes getType()
		{
			return ActionTypes.dialog_stop;
		}
		public override void doAction()
		{
			DialogManager.instance.endScript();
		}
	}
}