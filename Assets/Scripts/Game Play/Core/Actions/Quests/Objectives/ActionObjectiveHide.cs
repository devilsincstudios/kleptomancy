﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Quests;

namespace DI.Core.Actions
{
	public class ActionObjectiveHide : Action
	{
		public Objective target;
		public override ActionTypes getType()
		{
			return ActionTypes.objective_hide;
		}
		public override void doAction()
		{
			target.setHidden(true);
		}
	}
}