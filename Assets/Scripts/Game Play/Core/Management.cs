﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

using DI.Core.Behaviours;

namespace DI.Core
{
	[AddComponentMenu("Devil's Inc Studios/Managers/Management")]
	public class Management : DI_MonoBehaviourSingleton<Management>
	{
		public void Awake()
		{
			// If we have another instance active, destroy it and replace it with this one.
			if (instance != null) {
				Destroy(instance);
			}
			makeSingleton(this);
		}
	}
}