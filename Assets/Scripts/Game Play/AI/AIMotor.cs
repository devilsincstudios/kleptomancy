// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;

using DI.Core.StateMachine;
using DI.Core.Debug;
using DI.Mechanics;
using DI.Core.Input;
using DI.Core;
using DI.Mechanics.Abilities;
using DI.Core.Events;
using DI.AI;
using DI.Entities.Properties;
using DI.SFX;

using System.Collections.Generic;

namespace DI.AI
{
	public class AIMotor : StateMachine
	{
		[Header("Speed Settings")]
		public float walkSpeed = 1.0f;
		public float runSpeed = 2.0f;

		[Header("Agent Settings")]
		public UnityEngine.AI.NavMeshAgent agent;
		public AIAnimator aiAnimator;
		public float closeEnoughDistance = 0.1f;
		public bool canMove = true;
		public bool hasTarget { get { return (targetLocation != Vector3.zero); } }

		[Header("Settings - Sound: Footsteps")]
		public float strideLength = 0.10f;
		public DI_SFXProperty footstepsWalking;
		public DI_SFXProperty footstepsRunning;

		[Header("Debug Settings")]
		public string debugCurrentState;
		[SerializeField]
		protected bool playedLeftFoot;
		[SerializeField]
		protected bool playedRightFoot;
		[SerializeField]
		protected Vector3 leftFootPos;
		[SerializeField]
		protected Vector3 rightFootPos;
		[SerializeField]
		protected Vector3 targetLocation;
		private bool moving;

		#region Setup
		protected void Awake()
		{
			InitializeStateMachine<AIMotorStates>(AIMotorStates.idling, true);

			#region Transistions idling
			AddTransitionsToState(
				AIMotorStates.idling,
				new Enum[] {
					AIMotorStates.moving,
				}
			);
			#endregion

			#region Transistions walking
			AddTransitionsToState(
				AIMotorStates.moving,
				new Enum[] {
					AIMotorStates.idling,
					AIMotorStates.moving,
				}
			);
			#endregion
		}

		#endregion

		public void OnAnimatorIK(int layerIndex)
		{
			leftFootPos = aiAnimator.animator.GetIKPosition (AvatarIKGoal.LeftFoot);
			rightFootPos = aiAnimator.animator.GetIKPosition (AvatarIKGoal.RightFoot);
		}

		public bool reachedLocation()
		{
			if (targetLocation == Vector3.zero) {
				targetLocation = transform.position;
			}

			return (Vector3.Distance(transform.position, targetLocation) <= closeEnoughDistance);
		}

		public bool isMoving()
		{
			return moving;
		}

		public void walkToLocation(Vector3 location)
		{
			moveToLocation(location, walkSpeed);
			aiAnimator.speed = walkSpeed;
			aiAnimator.changeState(AIAnimationStates.walking);
		}

		public void runToLocation(Vector3 location)
		{
			moveToLocation(location, runSpeed);
			aiAnimator.speed = runSpeed;
			aiAnimator.changeState(AIAnimationStates.running);
		}

		public void enableMovement()
		{
			canMove = true;
		}

		public void disableMovement()
		{
			canMove = false;
		}

		public void teleportToLocation(Vector3 location)
		{
			agent.enabled = false;
			agent.gameObject.transform.position = location;
			agent.enabled = true;
		}

		protected void moveToLocation(Vector3 location, float speed)
		{
			if (canMove) {
				agent.speed = speed;
//				if (hasTarget && !reachedLocation()) {
//					DI_EventCenter<GameObject, Vector3, Vector3>.invoke("OnAIChangeMovementTarget", this.gameObject, targetLocation, location);
//				}
				targetLocation = location;
				if (currentStateName != "moving") {
					ChangeCurrentState(AIMotorStates.moving, false);
				}
				else {
					agent.SetDestination(targetLocation);
				}
			}
			else {
				DI_Debug.writeLog(DI_DebugLevel.INFO, "AIMotor is in a restricted state, ignoring request for movement.");
			}
		}

		#region Updates
		public new void Update()
		{
			EarlyGlobalUpdate();
			base.Update();
			LateGlobalUpdate();
			debugCurrentState = currentStateName;
		}
		#endregion

		public Vector3 generateWanderTarget(float wanderRadius)
		{
			UnityEngine.AI.NavMeshHit hit;
			for (int attempt = 0; attempt < 30; attempt++) {
				Vector3 point = transform.position + UnityEngine.Random.insideUnitSphere * wanderRadius;
				if (UnityEngine.AI.NavMesh.SamplePosition(point, out hit, 1.0f, UnityEngine.AI.NavMesh.AllAreas)) {
					return hit.position;
				}
			}

			DI_Debug.writeLog(DI_DebugLevel.INFO, "Unable to determine a wander point");
			return transform.position;
		}

		// State Definitions
		#region Idling
		protected void idling_Enter(Enum previous)
		{
			DI_Debug.writeLog(DI_DebugLevel.INFO, "AIMotor Entered: " + debugCurrentState);
			moving = false;
			aiAnimator.changeState(AIAnimationStates.idling);
		}

		protected void idling_Update()
		{
		}
		#endregion Idling

		#region Moving
		protected void moving_Enter(Enum previous)
		{
			DI_Debug.writeLog(DI_DebugLevel.INFO, "AIMotor Entered: " + debugCurrentState);
			moving = true;
			agent.SetDestination(targetLocation);
			agent.Resume();
			agent.updateRotation = true;
		}

		protected void moving_Update()
		{
			if (!canMove) {
				agent.Stop();
				ChangeCurrentState(AIMotorStates.idling);
				DI_EventCenter<GameObject, Vector3>.invoke("OnAIMovementDisabled", this.gameObject, targetLocation);
				return;
			}

			if (reachedLocation()) {
				DI_Debug.writeLog(DI_DebugLevel.INFO, "Requested move to: " + targetLocation + " completed.");
				DI_EventCenter<GameObject, Vector3>.invoke("OnAIReachedMovementTarget", this.gameObject, targetLocation);
				moving = false;
				agent.Stop();
				ChangeCurrentState(AIMotorStates.idling);
			}
			else {
				// Footsteps
				if (!playedLeftFoot && Vector3.Distance (leftFootPos, aiAnimator.animator.pivotPosition) <= strideLength) {
					playFootStep();
					playedLeftFoot = true;
					playedRightFoot = false;
				}
				if (!playedRightFoot && Vector3.Distance (rightFootPos, aiAnimator.animator.pivotPosition) <= strideLength) {
					playFootStep();
					playedRightFoot = true;
					playedLeftFoot = false;
				}
			}
		}
		#endregion Moving

		protected void playFootStep()
		{
			if (agent.speed == walkSpeed) {
				if (footstepsWalking.hasSFX) {
					DI_SFX.playClipAtPoint(gameObject, transform.position, footstepsWalking.sfxs[UnityEngine.Random.Range(0, footstepsWalking.sfxs.Count)]);
				}
			}
			else if (agent.speed == runSpeed) {
				if (footstepsRunning.hasSFX) {
					DI_SFX.playClipAtPoint(gameObject, transform.position, footstepsRunning.sfxs[UnityEngine.Random.Range(0, footstepsRunning.sfxs.Count)]);
				}
			}
		}

		public void OnDrawGizmos()
		{
			if (moving) {
				Gizmos.color = Color.yellow;
				Gizmos.DrawSphere(targetLocation, 0.25f);
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(transform.position, targetLocation);
			}
		}
	}
}