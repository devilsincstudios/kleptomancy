﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Entities;
using UnityEngine;
using System;
using DI.Core.Debug;


namespace DI.AI
{
	public class AIAnimator : EntityAnimator
	{
		public float speed;
		public float direction;
		public float distanceFromGround;

		#region Awake / Define States
		protected void Awake() 
		{
			InitializeStateMachine<AIAnimationStates>(AIAnimationStates.idling, true);
			#region Transistions
			AddTransitionsToState(AIAnimationStates.idling, new Enum[0], true);
			AddTransitionsToState(AIAnimationStates.walking, new Enum[0], true);
			AddTransitionsToState(AIAnimationStates.running, new Enum[0], true);
			AddTransitionsToState(AIAnimationStates.interacting, new Enum[0], true);
			AddTransitionsToState(AIAnimationStates.cinema, new Enum[0], true);
			AddTransitionsToState(AIAnimationStates.custom, new Enum[0], true);
			#endregion
		}

		#endregion

		private void resetState()
		{
			animator.SetBool("Idling", false);
			animator.SetBool("Walking", false);
			animator.SetBool("Running", false);
			animator.SetBool("Interacting", false);
			animator.SetBool("Cinema", false);
			animator.SetBool("Custom", false);
			animator.SetFloat("Speed", speed);
			animator.SetFloat("Direction", direction);
		}

		public void changeState(AIAnimationStates newState)
		{
			resetState();
			ChangeCurrentState(newState, true);
		}

		#region Idling
		protected void idling_Enter(Enum previous)
		{
			animator.SetBool("Idling", true);
		}
		#endregion
		#region Walking
		protected void walking_Enter(Enum previous)
		{
			animator.SetBool("Walking", true);
		}
		#endregion
		#region Running
		protected void running_Enter(Enum previous)
		{
			animator.SetBool("Running", true);
		}
		#endregion
		#region Cinema
		protected void cinema_Enter(Enum previous)
		{
			animator.SetBool("Cinema", true);
		}
		#endregion
		#region Custom
		protected void custom_Enter(Enum previous)
		{
			animator.SetBool("Custom", true);
		}
		#endregion
	}
}