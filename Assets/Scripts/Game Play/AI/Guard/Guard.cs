﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;

using DI.Core.StateMachine;
using DI.Core.Debug;
using DI.Mechanics;
using DI.Core.Input;
using DI.Core;
using DI.Mechanics.Abilities;
using DI.Core.Events;
using DI.AI;
using DI.Entities;
using DI.Entities.Properties;
using DI.SFX;
using DI.Waypoints;

using UnityEngine.UI;
using System.Collections.Generic;

/*

Guards will patrol around the overworld level.
They will have a field of view in front of them and be able to hear sounds.
Guards will need to have multiple states of alertness.
0 - Safe | No usual activity has been seen/heard.
1 - Suspicious | Heard a noise, saw something briefly.
2 - Alert | Heard a noise again / saw something again.
3 - Searching | Actively searching for the source of the sound/visual.
4 - Hunting | Found the player and is actively hunting them.

If a player is seen for more than 3 seconds, they are considered to have been found.
While on alert, if the AI finds nothing at the last known source of noise/visual then they will revert back to suspicious for 30 seconds before finally going back to safe.
While Hunting , if the AI loses line of sight on the player they will have 5 seconds to regain it before the player is considered to have lost the AI.
While searching if nothing is found then wait 10 seconds and revert back to Alert.

The guard needs to obey line of sight.
In order to "hear" a sound. The guard should register to the OnNoise event and check the distance to the object as well as its noise value.

Guards should be able to talk to each other:
If one guard spots the player, they should emit an OnPlayerSpotted event with the location they saw the player.
Guards should be listening for this event, they should check the distance and see if its within their range.

If a guard loses sight of the player or is unable to find them after investigating, an OnPlayerLost event should be sent.
Guards in the area should go back to Alert.

 */
namespace DI.AI.Guard
{
	[RequireComponent(typeof(AIMotor))]
	[RequireComponent(typeof(GuardAnimator))]
	public class Guard : StateMachine
	{
		#region Guard Setup
		#region Projectile Settings
		[Header("Projectile Settings")]
		public Transform projectileSpawnPoint;
		#endregion
		#region Team Settings
		[Header("Team Settings")]
		[Tooltip("What team is this AI on?")]
		public Teams team = Teams.GUARD;
		#endregion
		#region Entity Settings
		[Header("Entity Settings")]
		public DI.Entities.NPC.DI_NPCEntity entity;
		#endregion
		#region Guard Settings
		[Header("Guard Settings")]
		public FieldOfView fov;
		public AIMotor guardMotor;
		public GuardAnimator guardAnimator;
		public float assistRadius = 20f;
		public float wanderRadius = 20f;
		public bool isBored = false;
		public ViewColoring viewConeColoring;
		public bool moving = false;
		public bool patrols = false;
		public WaypointRoute patrolRoute;
		public int currentWaypointId = 0;
		public Waypoint currentWaypoint;
		public float sleepingPowderTime = 0f;
		public float sleepingPowderTimeRemaining = 0f;
		public GameObject sleepingIcon;
		public Image sleepingFill;
		public bool eliteGuard = false;

		#endregion
		#region Guard Settings: Sound | Status
		[Header("Guard Settings - Sound: Status")]
		public DI_SFXProperty sfxTargetFound;
		public DI_SFXProperty sfxTargetLost;
		public DI_SFXProperty sfxSawSomething;
		public DI_SFXProperty sfxHeardSomething;
		public DI_SFXProperty sfxHuntingTarget;
		public DI_SFXProperty sfxCallForHelp;
		public DI_SFXProperty sfxRespondingToHelp;
		public DI_SFXProperty sfxAbandonSearch;
		#endregion
		#region Guard Settings: Sound | Annoucements
		[Header("Guard Settings - Sound: Annoucements")]
		public DI_SFXProperty sfxAnnoucementBoredom;
		public DI_SFXProperty sfxAnnoucementHunger;
		public DI_SFXProperty sfxAnnoucementThirst;
		public DI_SFXProperty sfxAnnoucementSleep;
		public DI_SFXProperty sfxAnnoucementReturnToWork;
		#endregion
		#region Fulfilment: Boredom
		[Header("Guard Settings - Fulfilment: Boredom")]
		// Guard Boredom
		[SerializeField]
		FulfilmentProperty boredomSettings;
		#endregion
		#region Fulfilment: Hunger
		[Header("Guard Settings - Fulfilment: Hunger")]
		// Guard Hunger
		[SerializeField]
		FulfilmentProperty hungerSettings;
		#endregion
		#region Fulfilment: Thirst
		[Header("Guard Settings - Fulfilment: Thirst")]
		// Guard Thirst
		[SerializeField]
		FulfilmentProperty thirstSettings;
		#endregion
		#region Fulfilment: Sleep
		[Header("Guard Settings - Fulfilment: Sleep")]
		// Guard Tiredness
		[SerializeField]
		FulfilmentProperty tirednessSettings;
		#endregion
		#region Fulfilment: Bathroom
		[Header("Guard Settings - Fulfilment: Bathroom")]
		// Guard Bathroom
		[SerializeField]
		FulfilmentProperty bathroomSettings;
		#endregion
		#region Guard Settings: Searching
		[Header("Guard Settings - Searching")]
		// Delay between seeing something and going into hunting mode.
		[SerializeField]
		protected float visableTimer = 0.0f;
		[Tooltip("Length of time it takes for guard to \"see\" the player")]
		[SerializeField]
		protected float visableDelay = 0.5f;
		protected float searchTimeRemaining = 0f;
		protected float searchTime = 30f;
		#endregion
		#region Guard Debug
		[Header("Visible for Debug - Do not set manually")]
		[SerializeField]
		protected string debugCurrentState;
		[SerializeField]
		protected GameObject currentTarget;
		[SerializeField]
		protected Vector3 targetLastLocation;
		[SerializeField]
		protected bool hasTarget = false;
		[SerializeField]
		protected GameObject nearestFood;
		[SerializeField]
		protected GameObject nearestDrink;
		public Vector3 leftFootPos;
		public Vector3 rightFootPos;
		private Vector3 startingLocation;
		private Quaternion startingRotation;
		#endregion
		#endregion

		#region StateMachine Setup
		protected void Awake()
		{
			InitializeStateMachine<GuardState>(GuardState.idling, true);
			
			#region Transistions idling
			AddTransitionsToState(
				GuardState.idling,
				new Enum[] {
					GuardState.searching,
					GuardState.alert,
					GuardState.hunting,
					GuardState.suspicious,
					GuardState.fulfilment,
					GuardState.sleeping,
				}
			);
			#endregion

			#region Transistions suspicious
			AddTransitionsToState(
				GuardState.suspicious,
				new Enum[] {
					GuardState.idling,
					GuardState.alert,
					GuardState.hunting,
					GuardState.searching,
					GuardState.sleeping,
				}
			);
			#endregion

			#region Transistions searching
			AddTransitionsToState(
				GuardState.searching,
				new Enum[] {
					GuardState.alert,
					GuardState.suspicious,
					GuardState.hunting,
					GuardState.idling,
					GuardState.sleeping,
				}
			);
			#endregion

			#region Transistions alert
			AddTransitionsToState(
				GuardState.alert,
				new Enum[] {
					GuardState.searching,
					GuardState.hunting,
					GuardState.suspicious,
					GuardState.sleeping,
				}
			);
			#endregion

			#region Transistions hunting
			AddTransitionsToState(
				GuardState.hunting,
				new Enum[] {
					GuardState.searching,
					GuardState.attacking,
					GuardState.sleeping,
				}
			);
			#endregion

			#region Transistions Fulfilment
			AddTransitionsToState(
				GuardState.fulfilment,
				new Enum[] {
					GuardState.idling,
					GuardState.suspicious,
					GuardState.searching,
					GuardState.hunting,
					GuardState.sleeping,
				}
			);
			#endregion

			#region Transistions Sleeping
			AddTransitionsToState(
				GuardState.sleeping,
				new Enum[] {
					GuardState.idling,
				}
			);
			#endregion
		}

		public void OnEnable()
		{
			DI_EventCenter<GameObject, GameObject>.addListener("OnTargetEnteredView", handleOnEnteredView);
			DI_EventCenter<GameObject, GameObject>.addListener("OnTargetLeftView", handleOnLeftView);
			DI_EventCenter<GameObject, GameObject, Vector3>.addListener("OnSoundDetected", handleOnSoundDetected);
			DI_EventCenter<GameObject, GameObject>.addListener("OnRequestAssistance", handleOnRequestAssistance);

			// Boredom
			if (boredomSettings.randomGain) {
				boredomSettings.gainRate = UnityEngine.Random.Range(boredomSettings.minGainRate, boredomSettings.maxGainRate);
			}
			if (boredomSettings.randomDecay) {
				boredomSettings.decayRate = UnityEngine.Random.Range(boredomSettings.minDecayRate, boredomSettings.maxDecayRate);
			}
			// Thirst
			if (thirstSettings.randomGain) {
				thirstSettings.gainRate = UnityEngine.Random.Range(thirstSettings.minGainRate, thirstSettings.maxGainRate);
			}
			if (thirstSettings.randomDecay) {
				thirstSettings.decayRate = UnityEngine.Random.Range(thirstSettings.minDecayRate, thirstSettings.maxDecayRate);
			}
			// Hunger
			if (hungerSettings.randomGain) {
				hungerSettings.gainRate = UnityEngine.Random.Range(hungerSettings.minGainRate, hungerSettings.maxGainRate);
			}
			if (hungerSettings.randomDecay) {
				hungerSettings.decayRate = UnityEngine.Random.Range(hungerSettings.minDecayRate, hungerSettings.maxDecayRate);
			}
			// Tiredness
			if (tirednessSettings.randomGain) {
				tirednessSettings.gainRate = UnityEngine.Random.Range(tirednessSettings.minGainRate, tirednessSettings.maxGainRate);
			}
			if (tirednessSettings.randomDecay) {
				tirednessSettings.decayRate = UnityEngine.Random.Range(tirednessSettings.minDecayRate, tirednessSettings.maxDecayRate);
			}
			// Bathroom
			if (bathroomSettings.randomGain) {
				bathroomSettings.gainRate = UnityEngine.Random.Range(bathroomSettings.minGainRate, bathroomSettings.maxGainRate);
			}
			if (bathroomSettings.randomDecay) {
				bathroomSettings.decayRate = UnityEngine.Random.Range(bathroomSettings.minDecayRate, bathroomSettings.maxDecayRate);
			}
			startingLocation = transform.position;
			startingRotation = transform.rotation;

			nearestFood = findNearestFood();
			nearestDrink = findNearestDrink();

			if (patrols) {
				currentWaypoint = patrolRoute.getClosestWaypoint(transform.position);
				currentWaypointId = patrolRoute.getWaypointId(currentWaypoint);
			}
		}

		public void OnDisable()
		{
			DI_EventCenter<GameObject, GameObject>.removeListener("OnTargetEnteredView", handleOnEnteredView);
			DI_EventCenter<GameObject, GameObject>.removeListener("OnTargetLeftView", handleOnLeftView);
			DI_EventCenter<GameObject, GameObject, Vector3>.removeListener("OnSoundDetected", handleOnSoundDetected);
			DI_EventCenter<GameObject, GameObject>.removeListener("OnRequestAssistance", handleOnRequestAssistance);
		}

		#endregion

		public float getBoredomLevel()
		{
			return boredomSettings.currentLevel / boredomSettings.maxLevel;
		}

		public float getHungerLevel()
		{
			return hungerSettings.currentLevel / hungerSettings.maxLevel;
		}

		public float getThirstLevel()
		{
			return thirstSettings.currentLevel / thirstSettings.maxLevel;
		}

		public float getTirednessLevel()
		{
			return tirednessSettings.currentLevel / tirednessSettings.maxLevel;
		}

		public float getBathroomLevel()
		{
			return bathroomSettings.currentLevel / bathroomSettings.maxLevel;
		}

		protected void announceState(GuardState state)
		{
			switch (state) {
				case GuardState.alert:
					break;
				case GuardState.attacking:
					break;
				case GuardState.fulfilment:
					break;
				case GuardState.hunting:
					break;
				case GuardState.idling:
					break;
				case GuardState.searching:
					break;
				case GuardState.sleeping:
					break;
				case GuardState.suspicious:
					break;
				default:
					break;
			}
		}

		protected void handleOnRequestAssistance(GameObject guard, GameObject target)
		{
			// Ignore our own call for help.
			if (guard != gameObject) {
				// Is the call within our range
				if (Vector3.Distance(guard.transform.position, transform.position) <= assistRadius) {
					if (currentTarget != target) {
						currentTarget = target;
						if (sfxRespondingToHelp.hasSFX) {
							DI_SFX.playClipAtPoint(gameObject, transform.position, sfxRespondingToHelp.sfxs[UnityEngine.Random.Range(0, sfxRespondingToHelp.sfxs.Count)]);
						}
					}
					ChangeCurrentState(GuardState.hunting, false);
				}
			}
		}

		protected void handleOnEnteredView(GameObject guard, GameObject target)
		{
			if (guard == gameObject) {
				targetLastLocation = target.transform.position;
				if (currentTarget != target) {
					if (sfxSawSomething.hasSFX) {
						DI_SFX.playClipAtPoint(gameObject, transform.position, sfxSawSomething.sfxs[UnityEngine.Random.Range(0, sfxSawSomething.sfxs.Count)]);
					}
				}
				currentTarget = target;

				switch (currentStateName) {
					case "idling":
						ChangeCurrentState(GuardState.suspicious, false);
						break;
					case "suspicious":
						ChangeCurrentState(GuardState.hunting, false);
						break;
					case "alert":
						ChangeCurrentState(GuardState.hunting, false);
						break;
					case "searching":
						ChangeCurrentState(GuardState.hunting, false);
						if (sfxHuntingTarget.hasSFX) {
							DI_SFX.playClipAtPoint(gameObject, transform.position, sfxHuntingTarget.sfxs[UnityEngine.Random.Range(0, sfxHuntingTarget.sfxs.Count)]);
						}
						break;
				}
			}
		}

		protected void handleOnLeftView(GameObject guard, GameObject target)
		{
			if (guard == gameObject) {
				targetLastLocation = target.transform.position;
				switch (currentStateName) {
					case "hunting":
						ChangeCurrentState(GuardState.searching, false);
						break;
				}
			}
		}

		protected void handleOnSoundDetected(GameObject guard, GameObject emitter, Vector3 target)
		{
			if (guard == gameObject) {
				if (emitter != null) {
					Debug.Log("Heard something!");
					if (emitter.tag != "Guard") {
						if (currentTarget == null) {
							targetLastLocation = target;
						}

						switch (currentStateName) {
							case "idling":
								ChangeCurrentState(GuardState.suspicious, false);
								break;
							case "suspicious":
							case "alert":
								ChangeCurrentState(GuardState.searching, false);
								break;
							case "searching":
							case "hunting":
								//Debug.Log("Heard sound");
								targetLastLocation = target;
								break;
						}
					}
				}
			}
		}


		#region Updates
		public new void Update()
		{
			EarlyGlobalUpdate();
			base.Update();
			LateGlobalUpdate();
			debugCurrentState = currentStateName;
		}

		public new void EarlyGlobalUpdate()
		{
			if (!eliteGuard) {
				hungerSettings.currentLevel += hungerSettings.gainRate * DI_Time.getTimeDelta();
				thirstSettings.currentLevel += thirstSettings.gainRate * DI_Time.getTimeDelta();
				tirednessSettings.currentLevel += tirednessSettings.gainRate * DI_Time.getTimeDelta();
				bathroomSettings.currentLevel += bathroomSettings.gainRate * DI_Time.getTimeDelta();
			}
		}
		#endregion
		
		// State Definitions
		#region Idling
		protected void idling_Enter(Enum previous)
		{
			viewConeColoring.setSafe();
		}
		
		protected void idling_Update()
		{
			if (!entity.isAsleep()) {
				
				// We can only move to one state, this must be enforced.
				// Guards will only leave idle if an event happens or they get bored.

				if (!guardMotor.isMoving()) {
					if (patrols) {
						if (Vector3.Distance(currentWaypoint.transform.position, transform.position) < guardMotor.closeEnoughDistance) {
							currentWaypoint = patrolRoute.getNextWaypoint(currentWaypointId);
							currentWaypointId = patrolRoute.getWaypointId(currentWaypoint);
							Debug.Log("Asking for next waypoint");
						}
						if (currentWaypoint.getShouldTeleport()) {
							guardMotor.teleportToLocation(currentWaypoint.transform.position);
						}

						guardMotor.walkToLocation(currentWaypoint.transform.position);
					}
				}

				boredomSettings.currentLevel += boredomSettings.gainRate * DI_Time.getTimeDelta();

				if (boredomSettings.currentLevel > boredomSettings.maxLevel) {
					boredomSettings.currentLevel = boredomSettings.maxLevel;
					ChangeCurrentState(GuardState.fulfilment, false);
				}
			}
		}
		#endregion Fulfilment

		#region Fulfilment
		protected void fulfilment_Enter(Enum previous)
		{
			guardMotor.walkToLocation(guardMotor.generateWanderTarget(wanderRadius));
			isBored = true;
			viewConeColoring.setBored();
		}

		protected void fulfilment_Update()
		{
			if (guardMotor.reachedLocation()) {
				if (isBored) {
					if (boredomSettings.currentLevel >= boredomSettings.maxLevel) {
						moving = false;
					}

					if (boredomSettings.currentLevel > 0f) {
						boredomSettings.currentLevel -= boredomSettings.decayRate * DI_Time.getTimeDelta();
					}
					else {
						// Return to post after becoming "unbored?"
						guardMotor.walkToLocation(startingLocation);
						moving = true;
						isBored = false;
					}
				}
				else {
					// Reached starting point again.
					ChangeCurrentState(GuardState.idling, false);
					transform.rotation = startingRotation;
				}
			}
		}
		#endregion Fulfilment

		#region Suspicious
		protected void suspicious_Enter(Enum previous)
		{
			viewConeColoring.setSuspicious();
			searchTimeRemaining = searchTime;
		}
		
		protected void suspicious_Update()
		{
			
			// We can only move to one state, this must be enforced.
			// Guards will only leave idle if an event happens or they get bored.
			if (searchTimeRemaining >= 0f) {
				searchTimeRemaining -= DI_Time.getTimeDelta();
				if (!guardMotor.isMoving()) {
					guardMotor.walkToLocation(targetLastLocation);
				}

				if (fov.canSeeTarget(currentTarget)) {
					searchTimeRemaining = searchTime;
					ChangeCurrentState(GuardState.hunting);
					targetLastLocation = currentTarget.transform.position;
					DI_EventCenter<GameObject, GameObject>.invoke("OnRequestAssistance", gameObject, currentTarget);
					return;
				}
			}
			// The player has evaded us
			else {
				searchTimeRemaining = 0f;
				guardMotor.walkToLocation(startingLocation);
				ChangeCurrentState(GuardState.idling, false);
			}
		}
		#endregion Suspicious

		#region Searching
		protected void searching_Enter(Enum previous)
		{
			viewConeColoring.setSearching();
			searchTimeRemaining = searchTime;
		}
		
		protected void searching_Update()
		{
			
			// We can only move to one state, this must be enforced.
			// Guards will only leave idle if an event happens or they get bored.
			if (searchTimeRemaining >= 0f) {
				searchTimeRemaining -= DI_Time.getTimeDelta();
				if (!moving) {
					guardMotor.runToLocation(targetLastLocation);
				}

				if (fov.canSeeTarget(currentTarget)) {
					searchTimeRemaining = searchTime;
					ChangeCurrentState(GuardState.hunting);
					targetLastLocation = currentTarget.transform.position;
					DI_EventCenter<GameObject, GameObject>.invoke("OnRequestAssistance", gameObject, currentTarget);
				}
			}
			// The player has evaded us
			else {
				searchTimeRemaining = 0f;
				guardMotor.walkToLocation(startingLocation);
				moving = true;
				ChangeCurrentState(GuardState.idling, false);
			}
		}
		#endregion Searching

		#region Hunting
		protected void hunting_Enter(Enum previous)
		{
			viewConeColoring.setHunting();
			searchTimeRemaining = searchTime;
			announceState(GuardState.hunting);
			guardAnimator.headLookWeight = 1f;
		}

		protected void hunting_Update()
		{
			
//			// We can only move to one state, this must be enforced.
			if (fov.canSeeTarget(currentTarget)) {
				if (!moving) {
					moving = true;
				}
				targetLastLocation = currentTarget.transform.position;
				guardMotor.runToLocation(targetLastLocation);
				guardAnimator.headLookTarget = targetLastLocation;
				DI_EventCenter<GameObject, GameObject>.invoke("OnRequestAssistance", gameObject, currentTarget);
			}
			// We lost sight of the player, check the last known location
			else {
				ChangeCurrentState(GuardState.searching, false);
			}
		}

		protected void hunting_Exit(Enum previous)
		{
			guardAnimator.headLookWeight = 0f;
		}

		#endregion Hunting

		#region Sleeping
		protected void sleeping_Enter(Enum previous)
		{
			sleepingIcon.SetActive(true);
			guardAnimator.changeState(AIAnimationStates.sleeping);
			guardMotor.disableMovement();
			guardAnimator.sleeping = true;
		}

		protected void sleeping_Update()
		{

			if (sleepingPowderTimeRemaining >= 0f) {
				sleepingPowderTimeRemaining -= DI_Time.getTimeDelta();
				sleepingFill.fillAmount =  sleepingPowderTimeRemaining / sleepingPowderTime;
			}
			else {
				ChangeCurrentState(GuardState.idling, false);
			}
		}

		protected void sleeping_Exit(Enum previous)
		{
			sleepingIcon.SetActive(false);
			guardMotor.enableMovement();
			guardAnimator.sleeping = false;
		}

		#endregion Sleeping

		protected GameObject findNearestFood()
		{
			GameObject[] foodSpots = GameObject.FindGameObjectsWithTag("Food");
			GameObject nearestFood = null;
			if (foodSpots.Length != 0) {
				float nearestFoodDistance = Vector3.Distance(transform.position, foodSpots[0].transform.position);
				nearestFood = foodSpots[0];
				
				for (int spot = 0; spot < foodSpots.Length; spot++) {
					float checkedDistance = Vector3.Distance(transform.position, foodSpots[spot].transform.position);
					if (checkedDistance < nearestFoodDistance) {
						nearestFoodDistance = checkedDistance;
						nearestFood = foodSpots[spot];
					}
				}
			}

			return nearestFood;
		}

		protected GameObject findNearestDrink()
		{
			GameObject[] drinkSpots = GameObject.FindGameObjectsWithTag("Drink");
			GameObject nearestDrink = null;
			if (drinkSpots.Length != 0) {
				float nearestDrinkDistance = Vector3.Distance(transform.position, drinkSpots[0].transform.position);
				nearestDrink = drinkSpots[0];

				for (int spot = 0; spot < drinkSpots.Length; spot++) {
					float checkedDistance = Vector3.Distance(transform.position, drinkSpots[spot].transform.position);
					if (checkedDistance < nearestDrinkDistance) {
						nearestDrinkDistance = checkedDistance;
						nearestDrink = drinkSpots[spot];
					}
				}
			}

			return nearestDrink;
		}

		public void putToSleep(float duration)
		{
			if (currentStateName != "sleeping") {
				ChangeCurrentState(GuardState.sleeping, false);
				sleepingPowderTime = duration;
				sleepingPowderTimeRemaining = duration;
			}
		}

		public void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(transform.position, assistRadius);

			// Display the nearest Drink source
			if (nearestDrink == null) {
				nearestDrink = findNearestDrink();
			}
			if (nearestDrink != null) {
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(transform.position, nearestDrink.transform.position);
				Gizmos.DrawWireSphere(nearestDrink.transform.position, 0.2f);
			}

			// Display the nearest food source
			if (nearestFood == null) {
				nearestFood = findNearestFood();
			}
			if (nearestFood != null) {
				Gizmos.color = Color.green;
				Gizmos.DrawLine(transform.position, nearestFood.transform.position);
				Gizmos.DrawWireSphere(nearestFood.transform.position, 0.2f);
			}

			Gizmos.color = Color.cyan;
			Gizmos.DrawWireSphere(guardAnimator.animator.pivotPosition, 0.1f);

			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(leftFootPos, 0.2f);
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(rightFootPos, 0.2f);
		}

		public void OnBecameInvisible()
		{
			Debug.Log("Invisible");
		}

		public void OnBecomeVisible()
		{
			Debug.Log("Visible");
		}
	}
}