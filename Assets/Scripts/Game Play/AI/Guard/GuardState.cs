﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.AI.Guard
{
    public enum GuardState
    {
        idling,
        suspicious,
        alert,
        searching,
        hunting,
		attacking,
		fulfilment,
		sleeping,
    }
}
