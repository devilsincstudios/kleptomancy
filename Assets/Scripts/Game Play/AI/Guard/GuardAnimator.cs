﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.AI;
using System;

namespace DI.AI.Guard
{
	public class GuardAnimator : AIAnimator
	{
		// Fulfilment
		public bool eating;
		public bool drinking;
		public bool sleeping;
		// Actions
		public bool searching;
		public bool alerted;
		public bool attacking;

		// IK Goals
		public Vector3 headLookTarget;
		public float headLookWeight;

		public void OnAnimatorIK(int layerIndex)
		{
			animator.SetLookAtPosition(headLookTarget);
			animator.SetLookAtWeight(headLookWeight);
		}

		public new void Update()
		{
			base.Update();

			// Fulfilment
			animator.SetBool("Eating", eating);
			animator.SetBool("Drinking", drinking);
			animator.SetBool("Sleeping", sleeping);

			// Actions
			animator.SetBool("Searching", searching);
			animator.SetBool("Alerted", alerted);
			animator.SetBool("Attacking", attacking);
		}
	}
}