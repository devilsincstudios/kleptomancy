﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.SFX;
using UnityEngine;
using System.Collections;

namespace DI.AI.Guard
{
    public class SoundDetection : DI_MonoBehaviour
    {

        public float hearingRange;

        public void OnEnable()
        {
			DI_EventCenter<GameObject, Vector3, DI_SFXClipProperties>.addListener("OnPlaySFX", handlePlaySFX);
        }
		public void OnDisable()
		{
			DI_EventCenter<GameObject, Vector3, DI_SFXClipProperties>.removeListener("OnPlaySFX", handlePlaySFX);
		}

		public void handlePlaySFX(GameObject owner, Vector3 position, DI_SFXClipProperties clipProperties)
        {
			if (owner != null) {
				if (owner != gameObject) {
					if(Vector3.Distance(position, transform.position) < hearingRange)
		            {
						// TODO Raycast to the sound source and cut noise by 50% per wall.
						// "Hearability" is determined by (volume / distance) * 50 >= 1
						float distance = Vector3.Distance(position, transform.position);
						float heardVolume = (clipProperties.volume / distance) * 50;
						if (heardVolume >= 1f) {
							DI_EventCenter<GameObject, GameObject, Vector3>.invoke("OnSoundDetected", gameObject, owner, position);
						}
		            }
				}
			}
			else {
				Debug.Log("Owner is null");
			}
        }

		public void OnDrawGizmosSelected()
		{
			Gizmos.DrawWireSphere(transform.position, hearingRange);
		}
    }
}
