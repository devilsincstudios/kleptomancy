﻿using UnityEngine;
using DI.SFX;

public class TestSound : MonoBehaviour
{
    public DI_SFXClipProperties text;
    void Update()
    {
		DI_SFX.playClipAtPoint(gameObject, transform.position,text);
    }
}
