﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;
using DI.Mechanics.Abilities;
using DI.Core.Events;
using DI.UI.Misc;

namespace DI.AI.Guard
{
	public class GuardDisplay : DI_MonoBehaviour
	{
		public GameObject guardGlow;
		public Image boredomBar;
		public Text boredomValue;
		public Image tirednessBar;
		public Text tirednessValue;
		public Image hungerBar;
		public Text hungerValue;
		public Image thirstBar;
		public Text thirstValue;
		public Image bathroomBar;
		public Text bathroomValue;

		public Guard guard;
		public bool updating = false;
		public FacePlayer facePlayer;

		public void OnEnable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.addListener("OnUseAbility", handleOnUseAbility);
		}
		public void OnDisable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.removeListener("OnUseAbility", handleOnUseAbility);
		}

		public void handleOnUseAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
			if (ability.ability == Abilities.umbral_sight) {
				updating = !updating;
				guardGlow.SetActive(updating);
				facePlayer.gameObject.SetActive(updating);
			}
		}

		public void Update()
		{
			if (updating) {
				boredomBar.fillAmount = guard.getBoredomLevel();
				boredomValue.text = "Boredom: " + Mathf.Round(guard.getBoredomLevel() * 100) + "%";

				hungerBar.fillAmount = guard.getHungerLevel();
				hungerValue.text = "Hunger: " + Mathf.Round(guard.getHungerLevel() * 100) + "%";

				tirednessBar.fillAmount = guard.getTirednessLevel();
				tirednessValue.text = "Tiredness: " + Mathf.Round(guard.getTirednessLevel() * 100) + "%";

				thirstBar.fillAmount = guard.getThirstLevel();
				thirstValue.text = "Thirst: " + Mathf.Round(guard.getThirstLevel() * 100) + "%";

				bathroomBar.fillAmount = guard.getBathroomLevel();
				bathroomValue.text = "Bathroom: " + Mathf.Round(guard.getBathroomLevel() * 100) + "%";
			}
		}
	}
}