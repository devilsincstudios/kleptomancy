﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;
using UnityEngine;

namespace DI.AI.Guard
{
	public class ViewColoring : DI.Core.Behaviours.DI_MonoBehaviour
	{
		public Material viewConeMaterial;
		public Color safeColor;
		public Color suspiciousColor;
		public Color alertColor;
		public Color searchingColor;
		public Color huntingColor;
		public Color attackingColor;
		public Color boredColor;

		public void OnEnable()
		{
			viewConeMaterial = gameObject.GetComponent<MeshRenderer>().material;
			setSafe();
		}

		public void setSafe()
		{
			viewConeMaterial.color = safeColor;
		}

		public void setSuspicious()
		{
			viewConeMaterial.color = suspiciousColor;
		}

		public void setAlert()
		{
			viewConeMaterial.color = alertColor;
		}

		public void setSearching()
		{
			viewConeMaterial.color = searchingColor;
		}

		public void setHunting()
		{
			viewConeMaterial.color = huntingColor;
		}

		public void setAttacking()
		{
			viewConeMaterial.color = attackingColor;
		}

		public void setBored()
		{
			viewConeMaterial.color = boredColor;
		}
	}
}

