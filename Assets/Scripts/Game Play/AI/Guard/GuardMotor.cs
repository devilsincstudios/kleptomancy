﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.AI;
using UnityEngine;
using DI.Core.Pooling;

namespace DI.AI.Guard
{
	public class GuardMotor : AIMotor
	{
		public GameObject leftFootPrintDecal;
		public GameObject rightFootPrintDecal;
		public Vector3 lastFootPrintPos;
		public bool leavesFootprints = true;

		protected new void moving_Update()
		{
			if (leavesFootprints) {
				if (!reachedLocation()) {
					// Footsteps
					if (Vector3.Distance(transform.position, lastFootPrintPos) > strideLength) {
						if (!playedLeftFoot && Vector3.Distance (leftFootPos, aiAnimator.animator.pivotPosition) <= strideLength) {
							GameObject footPrint = DI_PoolManager.getPooledObject(leftFootPrintDecal);
							footPrint.transform.position = new Vector3(leftFootPos.x, transform.position.y, leftFootPos.z);
							footPrint.transform.rotation = transform.rotation;
							footPrint.SetActive(true);
							lastFootPrintPos = footPrint.transform.position;
						}
						if (!playedRightFoot && Vector3.Distance (rightFootPos, aiAnimator.animator.pivotPosition) <= strideLength) {
							GameObject footPrint = DI_PoolManager.getPooledObject(rightFootPrintDecal);
							footPrint.transform.position = new Vector3(rightFootPos.x, transform.position.y, rightFootPos.z);
							footPrint.transform.rotation = transform.rotation;
							footPrint.SetActive(true);
							lastFootPrintPos = footPrint.transform.position;
						}
					}
				}
			}
			base.moving_Update();
		}
	}
}