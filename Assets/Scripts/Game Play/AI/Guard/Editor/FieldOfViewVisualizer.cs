﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
// Thanks to Sebastian Lague for the tutorial on FoV @ https://github.com/SebLague/Field-of-View
//
//

using UnityEditor;
using UnityEngine;
using DI.Core.Helpers;

namespace DI.AI.Guard
{
	[CustomEditor(typeof(FieldOfView))]
	public class FieldOfViewVisualizer : UnityEditor.Editor
	{
		public FieldOfView fov;

		public void OnEnable()
		{
			fov = (FieldOfView)target;
		}

		//public void 
		public void OnSceneGUI ()
		{
			// Draw the vision range
			Handles.DrawWireArc(fov.gameObject.transform.position, Vector3.up, Vector3.forward, 360, fov.viewRadius);

			// Draw the vision cone
			Handles.color = Color.yellow;
			Vector3 viewAngleA = DI_Math.DirectionFromAngle(-fov.viewAngle / 2, fov.transform);
			Vector3 viewAngleB = DI_Math.DirectionFromAngle(fov.viewAngle / 2, fov.transform);
			Handles.DrawLine (fov.transform.position, fov.transform.position + viewAngleA * fov.viewRadius);
			Handles.DrawLine (fov.transform.position, fov.transform.position + viewAngleB * fov.viewRadius);

			Handles.color = Color.red;
			for (int index = 0; index < fov.objectsInView.Count; index++) {
				Handles.DrawLine(fov.gameObject.transform.position, fov.objectsInView[index].transform.position);
			}
		}
	}
}
