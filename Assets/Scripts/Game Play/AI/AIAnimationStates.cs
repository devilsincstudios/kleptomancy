﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.AI
{
	public enum AIAnimationStates
	{
		idling,
		walking,
		running,
		interacting,
		cinema,
		custom,
		sleeping,
	}
}