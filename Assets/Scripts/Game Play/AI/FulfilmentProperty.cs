﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;

namespace DI.AI
{
	[Serializable]
	public struct FulfilmentProperty
	{
		public float currentLevel;
		public float maxLevel;
		public bool randomGain;
		public bool randomDecay;
		public float gainRate;
		public float minGainRate;
		public float maxGainRate;
		public float decayRate;
		public float minDecayRate;
		public float maxDecayRate;
	}
}

