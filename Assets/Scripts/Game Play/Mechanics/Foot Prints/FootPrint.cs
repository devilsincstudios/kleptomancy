﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.AI;
using DI.Core;

using UnityEngine;

namespace DI.Mechanics
{
	public class FootPrint : DI_MonoBehaviour
	{
		public Teams team;
		public float lifeSpan = 10f;
		public float timeRemaining = 0f;

		public void OnEnable()
		{
			timeRemaining = lifeSpan;
			RaycastHit hit;
			if (Physics.Raycast(new Ray(transform.position, -transform.up), out hit, 1.0f)) {
				transform.position = hit.point;
			}
		}

		public void Update()
		{
			if (timeRemaining > 0f) {
				timeRemaining -= DI_Time.getTimeDelta();
			}
			if (timeRemaining <= 0f) {
				gameObject.SetActive(false);
			}
		}
	}
}