﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

// DI_EventCenter<Abilities, Transform, Vector3>.invoke("OnUseAbility", selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);

using DI.Core.Events;
using DI.Core.Behaviours;
using UnityEngine;
using UnityStandardAssets.Cameras;
using DI.Controllers;
using DI.AI;

namespace DI.Mechanics.Abilities
{
	public class UmbralCloneSpawner : AbilitySpawner
	{
		public AIMotor cloneController;

		public override void useAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
			cloneController.transform.position = target;
			cloneController.gameObject.SetActive(true);
		}
	}
}