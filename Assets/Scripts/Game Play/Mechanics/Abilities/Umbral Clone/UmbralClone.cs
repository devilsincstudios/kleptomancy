﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.StateMachine;
using System;
using DI.AI;
using DI.Core.Events;
using UnityEngine;
using System.Collections;
using DI.Core.Pooling;
using DI.SFX;
using System.Collections.Generic;
using DI.Entities.Properties;
using DI.Core;

namespace DI.Mechanics.Abilities
{
	public class UmbralClone : StateMachine
	{
		public float cloneLifeTime = 10f;
		public AIMotor aiMotor;
		public GameObject despawnEffects;
		public DI_SFXProperty distractEffects;
		public float delayBetweenTaunts = 2.0f;
		public float delayBetweenTauntsRemaining;

		#region Setup
		protected void Awake() 
		{
			InitializeStateMachine<UmbralCloneStates>(UmbralCloneStates.idle, true);
			#region Transistions idle
			AddTransitionsToState(
				UmbralCloneStates.idle,
				new Enum[] {
					UmbralCloneStates.distract,
					UmbralCloneStates.move
				}
			);
			#endregion
			#region Transistions distract
			AddTransitionsToState(
				UmbralCloneStates.distract,
				new Enum[] {
					UmbralCloneStates.idle,
					UmbralCloneStates.distract_ready,
				}
			);
			#endregion
			#region Transistions distract ready
			AddTransitionsToState(
				UmbralCloneStates.distract_ready,
				new Enum[] {
					UmbralCloneStates.idle,
					UmbralCloneStates.move,
				}
			);
			#endregion
			#region Transistions move
			AddTransitionsToState(
				UmbralCloneStates.move,
				new Enum[] {
					UmbralCloneStates.idle,
				}
			);
			#endregion
		}
		#endregion

		public void OnEnable()
		{
			aiMotor.agent.enabled = true;
			delayBetweenTauntsRemaining = delayBetweenTaunts;
			ChangeCurrentState(UmbralCloneStates.idle, false);
			DI_EventCenter<GameObject, Vector3>.addListener("OnAIReachedMovementTarget", handleOnReachedTarget);
			DI_EventCenter<Ability, Transform, Vector3>.addListener("OnUseAbility", handleOnUseAbility);
			StartCoroutine("despawn");
		}

		public void OnDisable()
		{
			aiMotor.agent.enabled = false;
			DI_EventCenter<GameObject, Vector3>.removeListener("OnAIReachedMovementTarget", handleOnReachedTarget);
			DI_EventCenter<Ability, Transform, Vector3>.removeListener("OnUseAbility", handleOnUseAbility);
		}

		public void handleOnUseAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
			if (ability.ability == Abilities.clone_distract) {
				aiMotor.runToLocation(target);
				ChangeCurrentState(UmbralCloneStates.distract);
				return;
			}

			if (ability.ability == Abilities.clone_move) {
				aiMotor.walkToLocation(target);
				ChangeCurrentState(UmbralCloneStates.move);
				return;
			}
		}

		public void handleOnReachedTarget(GameObject mover, Vector3 target)
		{
			if (mover == gameObject) {
				if (currentStateName == "distract")
				{
					ChangeCurrentState(UmbralCloneStates.distract_ready, false);
				}
			}
		}

		public void playDistraction()
		{
			DI_SFX.playClipAtPoint(gameObject, transform.position, distractEffects.sfxs[UnityEngine.Random.Range(0, distractEffects.sfxs.Count)]);
		}

		public IEnumerator despawn()
		{
			yield return new WaitForSeconds(cloneLifeTime);
			GameObject effects = DI_PoolManager.getPooledObject(despawnEffects);
			effects.transform.position = transform.position;
			effects.SetActive(true);
			gameObject.SetActive(false);
		}

		// State Definitions
		#region Idling
		protected void distract_ready_Enter(Enum previous)
		{
			delayBetweenTauntsRemaining = 0f;
		}

		protected void distract_ready_Update()
		{
			delayBetweenTauntsRemaining -= DI_Time.getTimeDelta();
			if (delayBetweenTauntsRemaining <= 0f) {
				playDistraction();
				delayBetweenTauntsRemaining = delayBetweenTaunts;
			}
		}

		#endregion
	}
}