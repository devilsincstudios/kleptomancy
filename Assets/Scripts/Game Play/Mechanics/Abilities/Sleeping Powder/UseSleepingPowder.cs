﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

// DI_EventCenter<Abilities, Transform, Vector3>.invoke("OnUseAbility", selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);

using DI.Core.Events;
using DI.Core.Behaviours;
using UnityEngine;
using DI.Controllers;
using DI.AI.Guard;
using DI.Core.Pooling;

namespace DI.Mechanics.Abilities
{
	public class UseSleepingPowder : AbilitySpawner
	{
		public GameObject sleepingPowderPrefab;

		public override void useAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
			GameObject sleepingPowder = DI_PoolManager.getPooledObject(sleepingPowderPrefab);
			sleepingPowder.transform.position = target;
			sleepingPowder.SetActive(true);
		}
	}
}