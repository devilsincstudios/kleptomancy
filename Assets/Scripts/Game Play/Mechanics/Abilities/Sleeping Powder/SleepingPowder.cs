﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Mechanics.Abilities
{
	public class SleepingPowder : Ability
	{
		public float effectRadius = 5f;
		public float effectDuration = 10f;
		public float abilityLifeTime = 2.5f;
	}
}