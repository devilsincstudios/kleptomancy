﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DI.AI.Guard;
using DI.Controllers;
using DI.Core;

namespace DI.Mechanics.Abilities
{
	public class SleepingPowderEffect : DI_MonoBehaviour
	{
		public SleepingPowder abilityInfo;
		public LayerMask targetsLayerMask;
		public float checkTime = 0.2f;
		protected float sleepTimeRemaining = 0f;

		public void OnDrawGizmosSelected()
		{
			Gizmos.DrawWireSphere(transform.position, abilityInfo.effectRadius);
		}

		public void OnEnable()
		{
			StartCoroutine("despawn");
			StartCoroutine("checkForTargets");
			sleepTimeRemaining = abilityInfo.effectDuration;
			transform.localScale = new Vector3(abilityInfo.effectRadius * 2, abilityInfo.effectRadius * 2, abilityInfo.effectRadius * 2);
		}

		public void OnDisable()
		{
			StopAllCoroutines();
		}

		public void LateUpdate()
		{
			sleepTimeRemaining -= DI_Time.getTimeDelta();
		}

		public IEnumerator despawn()
		{
			yield return new WaitForSeconds(abilityInfo.abilityLifeTime);
			gameObject.SetActive(false);
		}

		public IEnumerator checkForTargets()
		{
			while (true) {
				yield return new WaitForSeconds(checkTime);
				Collider[] targets = Physics.OverlapSphere(transform.position, abilityInfo.effectRadius, targetsLayerMask);
				for (int index = 0; index < targets.Length; index++) {
					if (targets[index].tag == "Guard") {
						targets[index].GetComponent<Guard>().putToSleep(sleepTimeRemaining);
					}
					else if (targets[index].tag == "Player") {
						targets[index].GetComponent<Player>().putToSleep(sleepTimeRemaining);
					}
					Debug.Log("Found Target: " + targets[index].name);
				}
			}
		}
	}
}