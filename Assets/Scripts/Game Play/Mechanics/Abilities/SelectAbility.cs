﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Input;
using DI.Core.Events;
using DI.Core.Behaviours;
using System.Collections.Generic;
using UnityEngine;
using DI.Controllers;
using DI.Core;
using System.Collections;

namespace DI.Mechanics.Abilities
{
	public class SelectAbility : DI_MonoBehaviour
	{
		public Locomotion playerController;
		public List<Ability> playerAbilities;
		public List<Ability> cloneAbilities;
		public List<Ability> abilities;
		public Ability selectedAbility;
		public int selectedAbilityIndex;
		public bool isClone = false;

		#region Key Indexes
		private int abilityOneIndex = 0;
		private int abilityTwoIndex = 0;
		private int abilityThreeIndex = 0;
		private int abilityFourIndex = 0;
		#endregion

		#region Game Specific Input
		protected void updateKeyCache()
		{
			abilityOneIndex = DI_BindManager.getKeyIndex("Select Ability One", playerController.playerId);
			abilityTwoIndex = DI_BindManager.getKeyIndex("Select Ability Two", playerController.playerId);
			abilityThreeIndex = DI_BindManager.getKeyIndex("Select Ability Three", playerController.playerId);
			abilityFourIndex = DI_BindManager.getKeyIndex("Select Ability Four", playerController.playerId);
		}

		protected void updateInput()
		{
			switch (DI_BindManager.getKeyState(abilityOneIndex, playerController.playerId)) {
				case DI_KeyState.KEY_PRESSED:
				case DI_KeyState.AXIS_PRESSED_POSITIVE:
				case DI_KeyState.AXIS_PRESSED_NEGATIVE:
					selectAbility(0);
					break;
			}
			switch (DI_BindManager.getKeyState(abilityTwoIndex, playerController.playerId)) {
				case DI_KeyState.KEY_PRESSED:
				case DI_KeyState.AXIS_PRESSED_POSITIVE:
				case DI_KeyState.AXIS_PRESSED_NEGATIVE:
					selectAbility(1);
					break;
			}
			switch (DI_BindManager.getKeyState(abilityThreeIndex, playerController.playerId)) {
				case DI_KeyState.KEY_PRESSED:
				case DI_KeyState.AXIS_PRESSED_POSITIVE:
				case DI_KeyState.AXIS_PRESSED_NEGATIVE:
					selectAbility(2);
					break;
			}
			switch (DI_BindManager.getKeyState(abilityFourIndex, playerController.playerId)) {
				case DI_KeyState.KEY_PRESSED:
				case DI_KeyState.AXIS_PRESSED_POSITIVE:
				case DI_KeyState.AXIS_PRESSED_NEGATIVE:
					selectAbility(3);
					break;
			}
		}
		#endregion

		public void selectAbility(int index)
		{
			if (abilities.Count > index) {
				if (abilities[index] != null) {
					selectedAbility = abilities[index];
					selectedAbilityIndex = index;
					DI_EventCenter<Ability>.invoke("OnSelectAbility", selectedAbility);
				}
			}
		}

		public IEnumerator lowerCooldowns()
		{
			while (true) {
				yield return new WaitForSeconds(0.1f);
				// Reduce cooldowns
				for (int index = 0; index < playerAbilities.Count; index++) {
					if (playerAbilities[index].cooldownRemaining > 0f) {
						playerAbilities[index].cooldownRemaining -= DI_Time.getTimeDelta();
					}
				}
				for (int index = 0; index < cloneAbilities.Count; index++) {
					if (cloneAbilities[index].cooldownRemaining > 0f) {
						cloneAbilities[index].cooldownRemaining -= DI_Time.getTimeDelta();
					}
				}
				for (int index = 0; index < abilities.Count; index++) {
					if (abilities[index].cooldownRemaining > 0f) {
						abilities[index].cooldownRemaining -= DI_Time.getTimeDelta();
					}
				}
			}
		}

		public void OnEnable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.addListener("OnUseAbility", handleOnUseAbility);
			updateKeyCache();
			abilities = playerAbilities;

			// We do not want to pass the actual scriptable object as it will be written to and saved.
			// Create a clone instead and save against the clone.
			for (int index = 0; index < playerAbilities.Count; index++) {
				playerAbilities[index] = Object.Instantiate(playerAbilities[index]);
			}
			for (int index = 0; index < cloneAbilities.Count; index++) {
				cloneAbilities[index] = Object.Instantiate(cloneAbilities[index]);
			}
			StartCoroutine("lowerCooldowns");
		}

		public void OnDisable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.removeListener("OnUseAbility", handleOnUseAbility);
			StopAllCoroutines();
		}
		
		public void handleOnUseAbility(Ability ability, Transform spawnLocation, Vector3 target)
		{
			if (selectedAbility.costType == CostTypes.charge) {
				selectedAbility.currentCharges -= 1;
				if (isClone) {
					abilities = cloneAbilities;
				}
				else {
					abilities = playerAbilities;
				}
			}

			// Player swaped to the clone
			if (ability.ability == Abilities.umbral_clone) {
				abilities = cloneAbilities;
				isClone = true;
				selectAbility(0);
			}

			// Player returned from the clone
			if (ability.ability == Abilities.clone_return) {
				abilities = playerAbilities;
				isClone = false;
				selectAbility(0);
			}

			if (isClone) {
				cloneAbilities[selectedAbilityIndex].cooldownRemaining = cloneAbilities[selectedAbilityIndex].cooldown;
			}
			else {
				playerAbilities[selectedAbilityIndex].cooldownRemaining = playerAbilities[selectedAbilityIndex].cooldown;
			}
			abilities[selectedAbilityIndex].cooldownRemaining = abilities[selectedAbilityIndex].cooldown;
		}

		public void Update()
		{
			updateInput();
		}
	}
}