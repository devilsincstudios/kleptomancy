﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;

namespace DI.Mechanics.Abilities
{
    [Serializable]
	public class Ability : ScriptableObject
    {
        public Abilities ability;
        public CostTypes costType;
        public float resourceCost;
		public Sprite icon;
		public float maxCharges;
		public float currentCharges;
		public float maxRange = 20f;
		public float cooldown = 0f;
		public float cooldownRemaining = 0f;
    }
}
