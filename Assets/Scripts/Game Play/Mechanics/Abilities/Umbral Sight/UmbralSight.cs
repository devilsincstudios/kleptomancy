﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

// DI_EventCenter<Abilities, Transform, Vector3>.invoke("OnUseAbility", selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);

using DI.Core.Events;
using DI.Core.Behaviours;
using UnityEngine;
using UnityStandardAssets.Cameras;
using UnityStandardAssets.ImageEffects;
using System.Collections;

using DI.Controllers;

namespace DI.Mechanics.Abilities
{
	public class UmbralSight : AbilitySpawner
	{
		public Camera activeCamera;
		public VignetteAndChromaticAberration vignette;
		public EdgeDetection edgeDetection;
		public LayerMask normalMask;
		public LayerMask bonusMask;
		public bool isActive = false;

		protected Ability thisAbility;

		public void Awake()
		{
			normalMask = activeCamera.cullingMask;
			vignette.enabled = isActive;
			edgeDetection.enabled = isActive;
		}

		public override void useAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
			thisAbility = ability;

			if (!isActive) {
				isActive = true;
				activeCamera.cullingMask = bonusMask;
				StartCoroutine(recurringCost());
			}
			else {
				isActive = false;
				activeCamera.cullingMask = normalMask;
				StopCoroutine(recurringCost());
			}
			vignette.enabled = isActive;
			edgeDetection.enabled = isActive;
		}

		public IEnumerator recurringCost()
		{
			while (true) {
				yield return new WaitForSeconds(1);

				if (thisAbility.costType == CostTypes.mana) {
					if (player.getCurrentMana() >= thisAbility.resourceCost) {
						player.removeMana(thisAbility.resourceCost);
					}
					else {
						// Disable invis
						DI_EventCenter<Ability, Transform, Vector3>.invoke("OnUseAbility", thisAbility, transform, Vector3.zero);
						break;
					}
				}
			}
		}
	}
}