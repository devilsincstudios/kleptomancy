﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

// DI_EventCenter<Abilities, Transform, Vector3>.invoke("OnUseAbility", selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);

using DI.Core.Events;
using DI.Core.Behaviours;
using UnityEngine;
using UnityStandardAssets.Cameras;
using UnityStandardAssets.ImageEffects;
using DI.Controllers;
using System.Collections;

namespace DI.Mechanics.Abilities
{
	public class SightHighlight : AbilitySpawner
	{
		public Component glowEffect;
		public bool isActive = false;
		public bool hasRange = true;
		public float maxRange = 15f;
		public float updateCheckDelay = 0.4f;

		public void Awake()
		{
			glowEffect.GetType().GetProperty("enabled").SetValue(glowEffect, isActive, null);
		}

		public void setGlow(bool active)
		{
			glowEffect.GetType().GetProperty("enabled").SetValue(glowEffect, active, null);
		}

		public IEnumerator updateGlow()
		{
			yield return new WaitForSeconds(UnityEngine.Random.Range(0, updateCheckDelay));
			while (true) {
				float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
				if (distanceToPlayer < maxRange && isActive) {
					setGlow(isActive);
				}
				else {
					setGlow(false);
				}
				yield return new WaitForSeconds(updateCheckDelay);
			}
		}

		public override void useAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
			if (!isActive) {
				isActive = true;
				if (!hasRange) {
					setGlow(isActive);
				}
				else {
					StartCoroutine(updateGlow());
				}
			}
			else {
				isActive = false;
				if (hasRange) {
					StopCoroutine(updateGlow());
				}
				setGlow(isActive);
			}
		}
	}
}