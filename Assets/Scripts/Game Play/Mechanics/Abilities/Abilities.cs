﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

namespace DI.Mechanics.Abilities
{
    public enum Abilities
    {
        invisibility,
		umbral_clone,
        grappling_hook,
		umbral_sight,
		clone_distract,
		clone_wander,
		clone_move,
		clone_return,
		sleeping_powder,
    }
}
