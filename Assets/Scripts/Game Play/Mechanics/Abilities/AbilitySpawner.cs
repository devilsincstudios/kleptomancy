﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

// DI_EventCenter<Abilities, Transform, Vector3>.invoke("OnUseAbility", selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);

using DI.Core.Events;
using DI.Core.Behaviours;
using UnityEngine;
using DI.Controllers;

namespace DI.Mechanics.Abilities
{
	public class AbilitySpawner : DI_MonoBehaviour
	{
		public Abilities ability;
		public Player player;

		public void OnEnable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.addListener("OnUseAbility", handleOnUseAbility);
			player = GameObject.Find("Player").GetComponent<Player>();
		}

		public void OnDisable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.removeListener("OnUseAbility", handleOnUseAbility);
		}

		public void handleOnUseAbility(Ability usedAbility, Transform spawnPoint, Vector3 target)
		{
			if (usedAbility.ability == ability) {
				useAbility(usedAbility, spawnPoint, target);
			}
		}

		public virtual void useAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
		}
	}
}