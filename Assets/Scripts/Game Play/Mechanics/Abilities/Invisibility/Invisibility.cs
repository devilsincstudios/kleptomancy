﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

// DI_EventCenter<Abilities, Transform, Vector3>.invoke("OnUseAbility", selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);

using DI.Core.Events;
using DI.Core.Behaviours;
using UnityEngine;
using UnityStandardAssets.Cameras;
using DI.Controllers;
using System.Collections;

namespace DI.Mechanics.Abilities
{
	public class Invisibility : AbilitySpawner
	{
		public GameObject invisibilityParticles;
		public SkinnedMeshRenderer playerMeshRenderer;
		public Material playerMaterial;
		public Material invisibilityMaterial;
		public string playerLayer;
		public string invisibilityLayer;
		public bool isInvisible = false;

		protected Ability thisAbility;

		public override void useAbility(Ability ability, Transform spawnPoint, Vector3 target)
		{
			thisAbility = ability;

			if (!isInvisible) {
				isInvisible = true;
				player.gameObject.layer = LayerMask.NameToLayer(invisibilityLayer);
				playerMeshRenderer.material = invisibilityMaterial;
				StartCoroutine(recurringCost());
			}
			else {
				isInvisible = false;
				player.gameObject.layer = LayerMask.NameToLayer(playerLayer);
				playerMeshRenderer.material = playerMaterial;
				StopCoroutine(recurringCost());
			}

			invisibilityParticles.SetActive(isInvisible);
		}

		public IEnumerator recurringCost()
		{
			while (true) {
				yield return new WaitForSeconds(1);

				if (thisAbility.costType == CostTypes.mana) {
					if (player.getCurrentMana() >= thisAbility.resourceCost) {
						player.removeMana(thisAbility.resourceCost);
					}
					else {
						// Disable invis
						useAbility(thisAbility, transform, Vector3.zero);
						break;
					}
				}
			}
		}
	}
}
