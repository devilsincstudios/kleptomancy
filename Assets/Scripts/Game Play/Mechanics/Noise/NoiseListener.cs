﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.SFX;

namespace DI.Mechanics.Noise
{
	public class NoiseListener : DI_MonoBehaviour
	{
		public float maxNoiseLevel = 1f;
		public float currentNoiseLevel = 0f;
		[Tooltip("Amount of noise to remove each decay cycle")]
		public float noiseDecayAmount = 1f;
		public float noiseDecayTime = 0.5f;
		public int loadSceneOnFailure;

		public float hearingRange;

		public void OnEnable()
		{
			DI_EventCenter<GameObject, Vector3, DI_SFXClipProperties>.addListener("OnPlaySFX", handlePlaySFX);
			StartCoroutine(noiseDecay());
		}

		public void OnDisable()
		{
			DI_EventCenter<GameObject, Vector3, DI_SFXClipProperties>.removeListener("OnPlaySFX", handlePlaySFX);
			StopAllCoroutines();
		}

		public IEnumerator noiseDecay()
		{
			while (true) {
				yield return new WaitForSeconds(noiseDecayTime);
				currentNoiseLevel -= noiseDecayAmount;
				if (currentNoiseLevel < 0f) {
					currentNoiseLevel = 0f;
				}
				DI_EventCenter<float>.invoke("OnNoiseMeterUpdate", (currentNoiseLevel / maxNoiseLevel) * 100);
			}
		}

		public void handlePlaySFX(GameObject owner, Vector3 position, DI_SFXClipProperties clipProperties)
		{
			if (owner.tag != "Management") {
				float heardVolume;
				if(Vector3.Distance(position, transform.position) < hearingRange)
				{
					// TODO Raycast to the sound source and cut noise by 50% per wall.
					// "Hearability" is determined by (volume / distance) * 50 >= 1
					float distance = Vector3.Distance(position, transform.position);
					heardVolume = (clipProperties.volume / distance) * 50;
					if (owner.tag != "Player") {
						heardVolume = (clipProperties.volume / distance) * 200;
					}
					else {
						heardVolume = (clipProperties.volume / distance) * 50;
					}
					Debug.Log("heardVolume: " + heardVolume);
					Debug.Log("object: " + owner.tag);
					currentNoiseLevel += heardVolume;
					DI_EventCenter<float>.invoke("OnNoiseMeterUpdate", (currentNoiseLevel / maxNoiseLevel) * 100);
				}

				if (currentNoiseLevel >= maxNoiseLevel) {
					SceneManager.LoadScene(loadSceneOnFailure);
				}
			}
		}

		public void OnDrawGizmosSelected()
		{
			Gizmos.DrawWireSphere(transform.position, hearingRange);
		}
	}
}