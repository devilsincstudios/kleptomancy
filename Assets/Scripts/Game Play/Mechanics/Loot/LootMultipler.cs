﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public class LootMultipler : ScriptableObject
	{
		public List<LootMultiplierQuality> qualityValues;
		public List<LootMultiplierRarity> rarityValues;
	}
}