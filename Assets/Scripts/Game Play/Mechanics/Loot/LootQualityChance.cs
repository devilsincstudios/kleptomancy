﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public struct LootQualityChance
	{
		public LootQuality quality;
		public float chance;
	}
}