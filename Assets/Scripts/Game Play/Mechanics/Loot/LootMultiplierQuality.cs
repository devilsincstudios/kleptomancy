﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public struct LootMultiplierQuality
	{
		public LootQuality quality;
		public float multipler;
	}
}