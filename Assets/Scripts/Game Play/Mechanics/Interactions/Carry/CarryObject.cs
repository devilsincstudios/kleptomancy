﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using System;
using DI.Core.Events;
using DI.SFX;
using DI.Entities.Properties;
using DI.Controllers;

namespace DI.Mechanics.Interactions
{
	public class CarryObject : DI.Core.Behaviours.DI_MonoBehaviour, InteractionInterface
	{
		public float interactionTime;
		public string animationName;
		public DI_SFXProperty useSound;
		public Player player;

		public void OnEnable()
		{
			player = GameObject.Find("Player").GetComponent<Player>();
		}

		public float getInteractionTime()
		{
			return interactionTime;
		}

		public string getInteractionAnimationName()
		{
			return animationName;
		}

		public virtual void interact()
		{
			player.carryItemRequest(gameObject);
		}

		public virtual bool isInteractable()
		{
			return true;
		}
	}
}
