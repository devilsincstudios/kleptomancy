﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using DI.Entities.Door;
using DI.Mechanics.Loot;
using System;
using DI.Core.Events;
using DI.SFX;

using System.Collections.Generic;

namespace DI.Mechanics.Interactions
{
	public class LockedDoor : OpenableDoor
	{
		public LootItem key;
		public bool isLocked;
		public DI_SFXClipProperties lockedSound;
		public DI_SFXClipProperties unlockSound;
		public LootInventory playerInventory;

		public void OnEnable()
		{
			if (isLocked) {
				playerInventory = GameObject.Find("Player").GetComponent<LootInventory>();
			}
		}

		public override void interact()
		{
			if (isLocked) {
				if (key != null) {
					if (playerInventory.hasItem(key)) {
						Debug.Log("Unlocking door with key");
						isLocked = false;
						DI_SFX.playClipAtPoint(gameObject, transform.position, unlockSound);
						doorController.toggleDoor();
					}
					else {
						Debug.Log("Door is locked");
						DI_SFX.playClipAtPoint(gameObject, transform.position, lockedSound);
					}
				}
				else {
					Debug.Log("Door is locked");
					DI_SFX.playClipAtPoint(gameObject, transform.position, lockedSound);
				}
			}
			else {
				doorController.toggleDoor();
			}
		}
	}
}