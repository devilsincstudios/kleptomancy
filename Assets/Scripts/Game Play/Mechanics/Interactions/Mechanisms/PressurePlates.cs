﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using System.Collections.Generic;
using DI.SFX;
using DI.Core.Events;

namespace DI.Mechanics.Interactions.Mechanisms
{
	public class PressurePlates : Mechanism
    {
        public float weight = 0.0f;
        public float weightRequirement = 5f;

		public List<GameObject> objectsOnPlate;

		public void OnEnable()
		{
			objectsOnPlate = new List<GameObject>();
		}

        public void OnTriggerEnter(Collider other)
        {
			if (!objectsOnPlate.Contains(other.gameObject)) {
				Rigidbody rigidBody = other.GetComponent<Rigidbody>();
				if (rigidBody != null) {
					objectsOnPlate.Add(other.gameObject);
					weight += rigidBody.mass;
				}
			}
        }

		public void OnTriggerExit(Collider other)
        {
			if (objectsOnPlate.Contains(other.gameObject)) {
				Rigidbody rigidBody = other.GetComponent<Rigidbody>();
				if (rigidBody != null) {
					objectsOnPlate.Remove(other.gameObject);
					weight -= rigidBody.mass;
				}
			}
        }

		// This accepts no input from the player directly.
		public override void interact()
		{
		}

		// And as such is always not interactable.
		public override bool isInteractable()
		{
			return false;
		}

        public void Update()
        {
			if (weight > weightRequirement) {
				if (!isActive) {
					DI_SFX.playClipAtPoint(gameObject, transform.position, useSound.sfxs[Random.Range(0, useSound.sfxs.Count)]);
					isActive = true;
					animator.SetBool("Pressed", true);
					DI_EventCenter<Mechanism, bool>.invoke("OnMechanismStateChange", this, isActive);
				}
			}
			else {
				if (isActive) {
					isActive = false;
					DI_SFX.playClipAtPoint(gameObject, transform.position, useSound.sfxs[Random.Range(0, useSound.sfxs.Count)]);
					animator.SetBool("Pressed", false);
					DI_EventCenter<Mechanism, bool>.invoke("OnMechanismStateChange", this, isActive);
				}
			}
        }
    }
}
