﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using DI.Entities.Door;
using DI.Mechanics.Loot;
using DI.SFX;
using DI.Entities.Properties;
using System.Collections.Generic;

namespace DI.Mechanics.Interactions
{
	public class LootableChest : OpenChest
	{
		public LootInventory inventory;
		public GameObject lootParticles;
		public LootInventory playerInventory;
		public DI_SFXProperty lootSound;

		public void OnEnable()
		{
			lootParticles.SetActive(false);
			playerInventory = GameObject.Find("Player").GetComponent<LootInventory>();
		}

		public override void interact()
		{
			base.interact();

			for (int index = 0; index < inventory.items.Count; index++) {
				LootItem item = inventory.items[index];
				playerInventory.addItem(item);
				if (lootSound.hasSFX) {
					DI_SFX.playClipAtPoint(gameObject, transform.position, lootSound.sfxs[UnityEngine.Random.Range(0, lootSound.sfxs.Count)]);
				}
				Debug.Log("Looted item: " + item.itemName + " from " + gameObject.name);
			}

			inventory.removeAllItems();

			if (inventory.getMoney() > 0f) {
				Debug.Log("Looted money: " + inventory.getMoney() + " from " + gameObject.name);
				playerInventory.addMoney(inventory.getMoney());
				inventory.removeMoney(inventory.getMoney());
			}
		}
	}
}