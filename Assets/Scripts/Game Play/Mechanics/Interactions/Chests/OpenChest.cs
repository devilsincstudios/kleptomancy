﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using DI.Entities.Door;
using DI.Mechanics.Loot;

using System.Collections.Generic;

namespace DI.Mechanics.Interactions
{
	public class OpenChest : DI.Core.Behaviours.DI_MonoBehaviour, InteractionInterface
	{
		public float interactionTime;
		public string animationName;
		public DoorController doorController;
		public bool hasBeenUsed = false;

		public float getInteractionTime()
		{
			return interactionTime;
		}
		public string getInteractionAnimationName()
		{
			return animationName;
		}

		public virtual void interact()
		{
			// Chests don't close
			doorController.openDoor();
			hasBeenUsed = true;
		}

		public bool isInteractable()
		{
			return !hasBeenUsed;
		}
	}
}