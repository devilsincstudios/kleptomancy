﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using DI.Entities.Door;
using DI.Mechanics.Loot;
using System;
using DI.Core.Events;

using System.Collections.Generic;

namespace DI.Mechanics.Interactions
{
	public class PickPocket : DI.Core.Behaviours.DI_MonoBehaviour, InteractionInterface
	{
		public LootInventory inventory;
		public float interactionTime;
		public string animationName;
		public bool hasBeenLooted = false;
		public LootInventory playerInventory;

		public void OnEnable()
		{
			playerInventory = GameObject.Find("Player").GetComponent<LootInventory>();
		}

		public float getInteractionTime()
		{
			return interactionTime;
		}

		public string getInteractionAnimationName()
		{
			return animationName;
		}

		public virtual void interact()
		{
			hasBeenLooted = true;

			for (int index = 0; index < inventory.items.Count; index++) {
				LootItem item = inventory.items[index];
				playerInventory.addItem(item);
				Debug.Log("Stole item: " + item.itemName + " from " + gameObject.name);
			}
			inventory.removeAllItems();

			Debug.Log("Stole money: " + inventory.getMoney() + " from " + gameObject.name);
			playerInventory.addMoney(inventory.getMoney());
			inventory.removeMoney(inventory.getMoney());
		}

		public bool isInteractable()
		{
			return !hasBeenLooted;
		}
	}
}
