﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Pooling;
using DI.Core.Events;
using System.Collections.Generic;

namespace DI.Mechanics.Abilities.GrapplingHook
{
	public static class GrapplingRopeFactory
	{
		public static void destroyRope(GameObject hookHead)
		{
			GameObject[] children = hookHead.transform.GetComponentsInChildren<GameObject>(true);
			hookHead.transform.DetachChildren();

			for (int index = 0; index < children.Length; index++) {
				children[index].SetActive(false);
			}
		}

		public static void createRope(GameObject hookHead, GameObject ropePrefab, float length, float segments)
		{
			Debug.Log("Create Rope");
			if (segments == 0) {
				throw new UnityException("Segements can not be 0");
			}

			float size = length / segments;
			Debug.Log("Create Rope: Divisons: " + segments + " Size: " + size);

			GameObject[] ropeSegments = new GameObject[(int)segments + 1];
			Rigidbody hookBody = hookHead.GetComponent<Rigidbody>();

			for (int index = 0; index < segments; index++) {
				GameObject segment = DI_PoolManager.getPooledObject(ropePrefab.name);
				if (segment != null) {
					Debug.Log("Requested object from pool and got back: " + segment);
					ropeSegments[index] = segment;
					Vector3 scale = segment.transform.localScale;
					scale.y = size;
					segment.transform.localScale = scale;

					if (index == 0) {
						segment.GetComponent<HingeJoint>().connectedBody = hookBody;
					}
					else {
						Debug.Log("Connecting segment: " + index + " to segment: " + (index  -1));
						segment.GetComponent<HingeJoint>().connectedBody = ropeSegments[index - 1].GetComponent<Rigidbody>();
					}

					segment.transform.parent = hookHead.transform;
					Vector3 position = segment.transform.localPosition;
					position.y = -size;
					segment.transform.localPosition = position;
					segment.SetActive(true);
				}
			}
		}
	}
}