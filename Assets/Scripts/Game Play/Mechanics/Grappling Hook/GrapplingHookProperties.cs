﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI.Mechanics.Abilities.GrapplingHook
{
	[Serializable]
	public struct GrapplingHookProperties
	{
		[TooltipAttribute("How long is the line allowed to extend.")]
		public float lineRange;
		[TooltipAttribute("How wide is the line.")]
		public float lineWidth;
		[TooltipAttribute("How much force should be applied to the line when its shot.")]
		public float shotForce;
		[TooltipAttribute("What prefab should be spawned to represent the head of the grapple.")]
		public GameObject grappleHeadPrefab;
		[TooltipAttribute("What prefab should be spawned to represent the coil of rope of the grapple.")]
		public GameObject grappleRopeCoilPrefab;
		[TooltipAttribute("Delay before the hook fires, use this to sync the animations.")]
		public float fireDelay;
		[TooltipAttribute("How many sections of rope should be created, the higher the number the more fluid but the more resource intensive.")]
		public int segments;
	}
}