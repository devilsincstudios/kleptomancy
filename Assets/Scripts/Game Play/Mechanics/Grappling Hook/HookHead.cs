﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;

using UnityEngine;

namespace DI.Mechanics.Abilities.GrapplingHook
{
	public class HookHead : DI_MonoBehaviour
	{
		private Vector3 startingLocation;
		private float maxDistance = 0f;
		private bool endOfRope = false;

		public void setMaxDistance(float distance)
		{
			this.maxDistance = distance;
		}

		public void OnEnable()
		{
			startingLocation = transform.position;
			endOfRope = false;
		}

		public void Update()
		{
			float distanceTraveled = (startingLocation - transform.position).magnitude;
			if (distanceTraveled > maxDistance && endOfRope == false) {
				this.GetComponent<Rigidbody>().velocity = Physics.gravity;
				endOfRope = true;
			}
		}

		public void OnCollisionEnter(Collision other)
		{
			Debug.Log(other.gameObject.tag);
			DI_EventCenter<GameObject, Collision, bool>.invoke("OnGrapplingHookCollision", this.gameObject, other, endOfRope);
		}
	}
}