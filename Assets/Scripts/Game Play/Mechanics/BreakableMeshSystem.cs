﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Events;
using DI.Core.Behaviours;
using DI.SFX;

namespace DI.Mechanics
{
    public class BreakableMeshSystem : DI_MonoBehaviour
    {
        public float breakPoint = 0;
        public float relativeVelocity;

		public GameObject breakEffect;
		public bool broken = false;

		public DI_SFXClipProperties breakSound;

        void OnCollisionEnter(Collision col)
        {
            relativeVelocity = col.relativeVelocity.magnitude;
			if (!broken) {
	            if (relativeVelocity > breakPoint)
	            {
					broken = true;
					DI_SFX.playClipAtPoint(gameObject, transform.position, breakSound);
	                gameObject.GetComponent<MeshRenderer>().enabled = false;
					breakEffect.SetActive(true);
					ParticleSystem particles = breakEffect.GetComponent<ParticleSystem>();
					Destroy(gameObject, particles.main.duration);
	            }
			}
        }
    }
}
