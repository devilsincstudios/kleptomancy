﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Controllers
{
	public enum LocomotionState
	{
		idling,
		double_jump,
		falling,
		jumping,
		running,
		sneaking,
		sprinting,
		climbing_ladder,
		climbing_rope,
		walking_ledge,
		walking,
		sleeping,
	}
}