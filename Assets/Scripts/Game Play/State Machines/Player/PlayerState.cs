﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Controllers
{
	public enum PlayerState
	{
		idling,
		aiming,
		hiding,
		edging,
		interacting,
		lifting,
		pulling,
		pushing,
		using_clone,
		using_grapple,
		using_object,
		using_ladder,
		using_rope,
		sleeping
	}
}