﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;

using DI.Core.StateMachine;
using DI.Core.Debug;
using DI.Mechanics;
using DI.Core.Input;
using DI.Core;
using DI.Mechanics.Abilities;
using DI.Core.Events;
using DI.Mechanics.Interactions;
using DI.Core.Helpers;
using System.Collections.Generic;
using DI.Entities;
using DI.Entities.Items;
using System.Collections;
using UnityEngine.UI;

namespace DI.Controllers
{
	[RequireComponent(typeof(Locomotion))]
	public class Player : StateMachine
	{
		[Header("Player Controller")]
		public Locomotion playerController;
		public bool isClone = false;
		public float sleepingPowderTime = 0f;
		public float sleepingPowderTimeRemaining = 0f;

		[Header("Projectile Settings")]
		public Transform projectileSpawnPoint;
		public Transform aimingIndicator;
		public List<Transform> aimingIndicators;

		#region Object Manipulation Variables
		[Header("Object Manipulation Flags")]
		public bool isCarryingObject = false;
		public bool isPushingObject = false;
		public bool isPullingObject = false;
		public GameObject interactingObject;
		#endregion
		#region Interaction Icon Settings Varaibles
		[Header("Interaction Icon Settings")]
		public GameObject moveObjectHint;
		public GameObject interactObjectHint;
		public GameObject hidingPlaceHint;
		public GameObject sleepingIcon;
		public Image sleepingFill;

//		public float interactionDelay = 0.2f;
//		public float interactionDelayTimer = 0.0f;
		public float lastTouchedMovable = 0.0f;
		public SelectAbility selectAbility;

		#endregion
		#region Stealth Settings Variables
		[Header("Stealth Settings")]
		public bool isHiding = false;
		public Vector3 hidingPlaceEntrancePosition;
		public Quaternion hidingPlaceEntranceRotation;
		public LayerMask hidingExitLayer;

		#endregion
		#region Key Indexes
		private int fireKeyIndex = 0;
		private int interactKeyIndex = 0;
		private int aimingKeyIndex = 0;
		#endregion
		#region Action Variables
		[Header("Action Settings")]
		public bool canMove = false;
		public bool isAiming = false;
		public bool isFiring = false;
		public bool isUsingGrapple = false;
		public bool isInteracting = false;
		public bool isUsingObject = false;

		[Header("Mana Settings")]
		public float manaRegenDelay = 0.5f;
		public float manaRegenDelayTimer;
		public float manaRegenPercent = 2f;
		public float manaGain;
		public float manaGainMultipler = 1f;
		public float manaGainMultiplerTimer = 0f;

		[Header("Interaction Settings")]
		public float interactionTime;
		public float interactionTimeRemaining;
		public string interactionAnimation;

		#endregion
		#region Setup
		public void setManaRegenMultipler(float multiplier, float duration)
		{
			manaGainMultipler += multiplier;
			manaGainMultiplerTimer += duration;
		}

		protected void Awake() 
		{
			updateKeyCache();

			InitializeStateMachine<PlayerState>(PlayerState.idling, true);

			#region Transistions Idling
			// From idle the player can do what?
			/*
			 * Walk
			 * Crouch
			 * Aim
			 * Jump
			 * Fall
			 * using_clone
			 * interact
			 */
			AddTransitionsToState(
				PlayerState.idling,
				new Enum[] {
					PlayerState.aiming,
					PlayerState.using_clone,
					PlayerState.pulling,
					PlayerState.pushing,
					PlayerState.lifting,
					PlayerState.hiding,
					PlayerState.using_grapple,
					PlayerState.using_ladder,
					PlayerState.using_rope,
					PlayerState.using_object,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Aiming
			// From Aiming the player can do what?
			/*
			 * Idle
			*/

			AddTransitionsToState(
				PlayerState.aiming,
				new Enum[] {
					PlayerState.idling,
					PlayerState.using_grapple,
					PlayerState.using_clone,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Pulling
			// Returning from interaction states results in idling.
			AddTransitionsToState(
				PlayerState.pulling,
				new Enum[] {
					PlayerState.idling,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Pushing
			AddTransitionsToState(
				PlayerState.pushing,
				new Enum[] {
					PlayerState.idling,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Hiding
			AddTransitionsToState(
				PlayerState.hiding,
				new Enum[] {
					PlayerState.idling,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Edging
			// From Edging the player can do what?
			/*
			 * Idle
			*/
			AddTransitionsToState(
				PlayerState.edging,
				new Enum[] {
					PlayerState.idling,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Using Grapple
			AddTransitionsToState(
				PlayerState.using_grapple,
				new Enum[] {
					PlayerState.idling,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Using Object
			AddTransitionsToState(
				PlayerState.using_object,
				new Enum[] {
					PlayerState.idling,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Lifting Object
			AddTransitionsToState(
				PlayerState.lifting,
				new Enum[] {
					PlayerState.idling,
					PlayerState.sleeping,
				}
			);
			#endregion
			#region Transistions Sleeping
			AddTransitionsToState(
				PlayerState.sleeping,
				new Enum[] {
					PlayerState.idling,
				}
			);
			#endregion
			StartCoroutine(regenMana());
			manaGain = playerController.characterProperties.manaProperties.maxMana * (manaRegenPercent / 100f);
		}
		#endregion
		#region Game Specific Input
		protected void updateKeyCache()
		{
			aimingKeyIndex = DI_BindManager.getKeyIndex("Aim", playerController.playerId);
			interactKeyIndex = DI_BindManager.getKeyIndex("Interact", playerController.playerId);
			fireKeyIndex = DI_BindManager.getKeyIndex("Fire", playerController.playerId);
		}

		protected void updateInput()
		{
			switch (DI_BindManager.getKeyState(aimingKeyIndex, playerController.playerId)) {
				case DI_KeyState.KEY_HELD:
					isAiming = true;
				break;
				case DI_KeyState.AXIS_HELD_POSITIVE:
					isAiming = true;
				break;
				default:
					isAiming = false;
				break;
			}

			switch (DI_BindManager.getKeyState(interactKeyIndex, playerController.playerId)) {
			case DI_KeyState.KEY_PRESSED:
				isInteracting = true;
				break;
			case DI_KeyState.AXIS_PRESSED_POSITIVE:
				isInteracting = true;
				break;
			case DI_KeyState.AXIS_PRESSED_NEGATIVE:
				isInteracting = true;
				break;
			default:
				isInteracting = false;
				break;
			}

			switch (DI_BindManager.getKeyState(fireKeyIndex, playerController.playerId)) {
			case DI_KeyState.KEY_PRESSED:
				isFiring = true;
				break;
			case DI_KeyState.AXIS_PRESSED_POSITIVE:
				isFiring = true;
				break;
			default:
				isFiring = false;
				break;
			}
		}
		#endregion
		#region Debug
		protected void OnGUI()
		{
			GUI.Box(new Rect(0, 35f, 300f, 30f), this.GetType().ToString() + " Current State: " + currentStateName);
		}
		#endregion
		#region Updates
		public new void Update()
		{
			EarlyGlobalUpdate();
			base.Update();
			LateGlobalUpdate();
		}

		public new void EarlyGlobalUpdate()
		{
			updateInput();
			if (manaRegenDelayTimer > 0.0f) {
				manaRegenDelayTimer -= DI_Time.getTimeDelta();
			}

			if (manaGainMultiplerTimer > 0.0f) {
				manaGainMultiplerTimer -= DI_Time.getTimeDelta();
			}
			else if (manaGainMultiplerTimer <= 0.0f) {
				manaGainMultipler = 1f;
			}
		}

		public new void LateGlobalUpdate()
		{
			
		}

		#endregion

		public float getCurrentMana()
		{
			if (playerController.characterProperties.manaProperties.hasUnlimitedMana) {
				return getMaxMana();
			}
			else {
				return playerController.characterProperties.manaProperties.currentMana;
			}
		}

		public float getMaxMana()
		{
			return playerController.characterProperties.manaProperties.maxMana;
		}

		public void removeMana(float amount)
		{
			playerController.characterProperties.removeMana(amount);
			manaRegenDelayTimer = manaRegenDelay;
		}

		public void addMana(float amount)
		{
			playerController.characterProperties.addMana(amount);
		}

		protected IEnumerator regenMana()
		{
			while (true) {
				yield return new WaitForSeconds(1);
				if (manaRegenDelayTimer <= 0.0f) {
					playerController.characterProperties.addMana(manaGain * manaGainMultipler);
					Debug.Log("Regaining Mana: " + manaGain * manaGainMultipler);
				}
			}
		}

		// State Definitions
		#region Idling
		protected void idling_Enter(Enum previous)
		{
			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "idling");
			canMove = false;
		}

		protected void idling_Update()
		{
			// We can only move to one state, this must be enforced.
			if (playerController.inControl) {
				if (isAiming) {
					ChangeCurrentState(PlayerState.aiming, false);
				}
				else if (isCarryingObject) {
					ChangeCurrentState(PlayerState.lifting, false);
				}
				else if (isPullingObject) {
					ChangeCurrentState(PlayerState.pulling, false);
				}
				else if (isPushingObject) {
					ChangeCurrentState(PlayerState.pushing, false);
				}
				else if (isUsingObject) {
					ChangeCurrentState(PlayerState.using_object, false);
				}
				else if (isUsingGrapple) {
					ChangeCurrentState(PlayerState.using_grapple, false);
				}

				if (interactionTimeRemaining >= 0f) {
					interactionTimeRemaining -= DI_Time.getTimeDelta();
				}
			}
		}

		#endregion Idling
		#region Pulling
		protected void pulling_Enter(Enum previous)
		{
			canMove = true;
		}

		protected void pulling_Update()
		{
			if (!isPullingObject) {
				ChangeCurrentState(PlayerState.idling, false);
			}
			// TODO code to do the pulling
		}
		#endregion
		#region Pushing
		protected void pushing_Update()
		{
			if (!isPushingObject) {
				ChangeCurrentState(PlayerState.idling, false);
			}
			// TODO code to do the pushing
		}
		#endregion
		#region Using Object
		protected void using_object_Enter(Enum previous)
		{
			isUsingObject = true;
			playerController.disablePlayerControl();
		}

		protected void using_object_Update()
		{
			if (interactionTimeRemaining >= 0f) {
				interactionTimeRemaining -= DI_Time.getTimeDelta();
			}
			else {
				ChangeCurrentState(PlayerState.idling, false);
			}
		}

		protected void using_object_Exit(Enum previous)
		{
			isUsingObject = false;
			playerController.enablePlayerControl();
		}

		#endregion
		#region Hiding
		protected void hiding_Enter(Enum previous)
		{
			isHiding = true;
			hidingPlaceEntrancePosition = playerController.transform.position;
			hidingPlaceEntranceRotation = playerController.transform.rotation;
			playerController.transform.position = interactingObject.transform.position;
			playerController.disablePlayerControl();
			playerController.GetComponent<CharacterController>().enabled = false;
			playerController.characterProperties.getAnimator().SetTrigger("Enter Hiding");
			isInteracting = false;
			DI_EventCenter<GameObject>.invoke("OnEnterHiding", interactingObject);
		}
		protected void hiding_Update()
		{
			if (isInteracting) {
				ChangeCurrentState(PlayerState.idling, false);
			}
			// TODO add code for hiding
			Debug.DrawRay(transform.position, transform.forward, Color.blue);
		}
		protected void hiding_Exit(Enum previous)
		{
			isHiding = false;
			RaycastHit hit;
			if (Physics.Raycast(transform.position, transform.forward, out hit, 3f, hidingExitLayer)) {
				playerController.transform.LookAt(hit.point);
				playerController.transform.position = hit.point;
			}
			else {
				playerController.transform.position = hidingPlaceEntrancePosition;
				playerController.transform.rotation = hidingPlaceEntranceRotation;
			}
			playerController.canMove = true;
			playerController.enablePlayerControl();
			playerController.characterProperties.getAnimator().SetTrigger("Exit Hiding");
			playerController.GetComponent<CharacterController>().enabled = true;
			DI_EventCenter<GameObject>.invoke("OnExitHiding", interactingObject);
		}
		#endregion
		#region Aiming
		protected void aiming_Enter(Enum previous)
		{
			playerController.animator.animationProperties.headLookWeight = 1f;
			playerController.aimingTarget.gameObject.SetActive(true);
			playerController.isAiming = true;
			aimingIndicator = aimingIndicators[selectAbility.selectedAbilityIndex];
		}

		protected void aiming_Update()
		{
			if (!isAiming) {
				ChangeCurrentState(PlayerState.idling, false);
				return;
			}

			aimingIndicator.gameObject.SetActive(false);
			aimingIndicator = aimingIndicators[selectAbility.selectedAbilityIndex];
			aimingIndicator.gameObject.SetActive(true);
			playerController.aim();
			aimingIndicator.position = playerController.aimingWorldPos;

			if (isFiring) {
				if (selectAbility.abilities[selectAbility.selectedAbilityIndex].cooldownRemaining <= 0f) {
					// Fire the OnUseAbility event.
					// Pass the ability type, spawn point, and the aiming target.
					if (selectAbility.abilities[selectAbility.selectedAbilityIndex].costType == CostTypes.charge) {
						if (selectAbility.abilities[selectAbility.selectedAbilityIndex].currentCharges >= 1) {
							Debug.Log("Firing: " + selectAbility.selectedAbility.ToString());
							DI_EventCenter<Ability, Transform, Vector3>.invoke("OnUseAbility", selectAbility.selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);
						}
					}
					else if (selectAbility.abilities[selectAbility.selectedAbilityIndex].costType == CostTypes.mana) {
						if (playerController.characterProperties.manaProperties.currentMana >= selectAbility.abilities[selectAbility.selectedAbilityIndex].resourceCost) {
							playerController.characterProperties.removeMana(selectAbility.abilities[selectAbility.selectedAbilityIndex].resourceCost);
							manaRegenDelayTimer = manaRegenDelay;
							Debug.Log("Firing: " + selectAbility.selectedAbility.ToString());
							DI_EventCenter<Ability, Transform, Vector3>.invoke("OnUseAbility", selectAbility.selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);
						}
						else {
							Debug.Log("Not enough mana!");
						}
					}
					else {
						Debug.Log("Firing: " + selectAbility.selectedAbility.ToString());
						DI_EventCenter<Ability, Transform, Vector3>.invoke("OnUseAbility", selectAbility.selectedAbility, projectileSpawnPoint, playerController.aimingWorldPos);
					}
				}
			}
		}

		protected void aiming_Exit(Enum previous)
		{
			playerController.animator.animationProperties.headLookWeight = 0f;
			playerController.aimingTarget.gameObject.SetActive(false);
			playerController.isAiming = false;
			aimingIndicator.gameObject.SetActive(false);
		}

		#endregion
		#region Lifting Object
		protected void lifting_Enter(Enum previous)
		{
			interactingObject.transform.parent = projectileSpawnPoint;
			interactingObject.GetComponent<Rigidbody>().isKinematic = true;
			interactingObject.transform.localPosition = new Vector3(0f, 0f, 0f);
			interactingObject.transform.localEulerAngles = Vector3.zero;
			isInteracting = false;
			playerController.encumbered = true;
		}

		protected void lifting_Update()
		{
			if (isInteracting) {
				isCarryingObject = false;
				ChangeCurrentState(PlayerState.idling, false);
			}
		}

		protected void lifting_Exit(Enum previous)
		{
			interactingObject.transform.parent = null;
			interactingObject.GetComponent<Rigidbody>().isKinematic = false;
			playerController.encumbered = false;
			interactingObject = null;
			interactionTimeRemaining = 0.1f;
		}
		#endregion
		#region Sleeping
		protected void sleeping_Enter(Enum previous)
		{
			playerController.disablePlayerControl();
			sleepingIcon.SetActive(true);
			playerController.requestStateChange(LocomotionState.sleeping);
		}

		protected void sleeping_Update()
		{
			if (sleepingPowderTimeRemaining >= 0f) {
				sleepingPowderTimeRemaining -= DI_Time.getTimeDelta();
				sleepingFill.fillAmount =  sleepingPowderTimeRemaining / sleepingPowderTime;
			}
			else {
				ChangeCurrentState(PlayerState.idling, false);
			}
		}

		protected void sleeping_Exit(Enum previous)
		{
			playerController.enablePlayerControl();
			playerController.requestStateChange(LocomotionState.idling);
			sleepingIcon.SetActive(false);
			sleepingPowderTime = 0f;
		}

		#endregion
		public void OnTriggerEnter(Collider other)
		{
			// Player ran into a pickup
			if (other.tag == "Powerups") {
				Powerup item = other.GetComponent<Powerup>();
				item.onUse();
			}
		}

		public void OnTriggerStay(Collider other)
		{
			if (interactionTimeRemaining > 0f) {
				interactObjectHint.SetActive(false);
				if (isInteracting) {
					isInteracting = false;
					return;
				}
			}

			if (!isInteracting && interactingObject == null) {
				if (other.tag == "Pickable") {
					// TODO Update ui to show its pickable
					moveObjectHint.SetActive(true);
				}
				else if (other.tag == "Interactable") {
					interactObjectHint.SetActive(true);
				}
				else if (other.tag == "Hiding Spot") {
					hidingPlaceHint.SetActive(true);
				}
			}
			else if (isInteracting) {
				if (currentStateName == "Idling") {
					if (other.tag == "Pickable") {
						isInteracting = false;
						interactingObject = other.gameObject;
						moveObjectHint.SetActive(false);
						ChangeCurrentState(PlayerState.lifting, false);
						return;
					}
					else if (other.tag == "Hiding Spot") {
						isInteracting = false;
						interactingObject = other.gameObject;
						hidingPlaceHint.SetActive(false);
						ChangeCurrentState(PlayerState.hiding, false);
						return;
					}
				}
				if (currentStateName != "using_object") {
					if (other.tag == "Interactable") {
						InteractionInterface interaction = (InteractionInterface) other.gameObject.GetComponent(typeof(InteractionInterface));
						if (interaction != null) {
							if (interaction.isInteractable()) {
								isInteracting = false;
								interactObjectHint.SetActive(false);
								interactionTime = interaction.getInteractionTime();
								interactionTimeRemaining = interactionTime;
								interactionAnimation = interaction.getInteractionAnimationName();
								interaction.interact();
								ChangeCurrentState(PlayerState.using_object, false);
								return;
							}
						}
					}
				}
				else {
					Debug.Log("other");
				}
			}
		}

		public void OnTriggerExit(Collider other)
		{
			if (other.tag == "Pickable") {
				moveObjectHint.SetActive(false);
			}

			if (other.tag == "Interactable") {
				interactObjectHint.SetActive(false);
			}
		}

		public void carryItemRequest(GameObject item)
		{
			if (!isCarryingObject) {
				interactingObject = item;
				ChangeCurrentState(PlayerState.lifting, false);
			}
		}

		public void putToSleep(float duration)
		{
			if (currentStateName != "sleeping") {
				ChangeCurrentState(PlayerState.sleeping, false);
				sleepingPowderTime = duration;
				sleepingPowderTimeRemaining = duration;
			}
		}
	}
}