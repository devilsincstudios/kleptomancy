﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Mechanics.Abilities.GrapplingHook
{
	public enum GrapplingHookStates
	{
		attached,
		extended,
		extending,
		retracted,
	}
}