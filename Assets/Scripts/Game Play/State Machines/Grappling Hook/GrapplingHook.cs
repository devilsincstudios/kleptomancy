﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using UnityEngine.UI;

using DI.Core.StateMachine;
using DI.Core.Debug;
using DI.Entities.Properties;
using DI.Core.Events;
using DI.Core.Pooling;

namespace DI.Mechanics.Abilities.GrapplingHook
{
	public class GrapplingHook : StateMachine
	{
		private Abilities abilityType = Abilities.grappling_hook;
		private bool onUse = false;
		private GameObject hookPrefabInstance;
		private GameObject ropeCoilInstance;
		private Vector3 target;
		private Transform origin;
		private float fireDelayTimer = 0.0f;

		private LineRenderer lineRenderer;
		private CapsuleCollider ropeCollider;

		public GrapplingHookProperties properties;

		#region Setup
		protected void Awake()
		{
			InitializeStateMachine<GrapplingHookStates>(GrapplingHookStates.retracted, true);

			#region Transistions attached
			AddTransitionsToState(
				GrapplingHookStates.attached,
				new Enum[] {
					GrapplingHookStates.retracted
				}
			);
			#endregion
			#region Transistions retracted
			AddTransitionsToState(
				GrapplingHookStates.retracted,
				new Enum[] {
					GrapplingHookStates.extending
				}
			);
			#endregion
			#region Transistions extending
			AddTransitionsToState(
				GrapplingHookStates.extending,
				new Enum[] {
					GrapplingHookStates.attached,
					GrapplingHookStates.extended
				}
			);
			#endregion
			#region Transistions extended
			AddTransitionsToState(
				GrapplingHookStates.extended,
				new Enum[] {
					GrapplingHookStates.retracted
				}
			);
			#endregion
		}

		public void OnEnable()
		{
			DI_EventCenter<Abilities, Transform, Vector3>.addListener("OnUseAbility", handleOnUseAbility);
			DI_EventCenter<GameObject, Collision, bool>.addListener("OnGrapplingHookCollision", handleOnGrapplingHookCollision);
		}

		private void handleOnUseAbility(Abilities ability, Transform origin, Vector3 target)
		{
			if (ability == abilityType) {
				onUse = true;
				this.origin = origin;
				this.target = target;
			}
		}

		private void handleOnGrapplingHookCollision(GameObject hookHead, Collision other, bool endOfRope)
		{
			Rigidbody hookRigidBody = hookHead.GetComponent<Rigidbody>();
			hookRigidBody.isKinematic = true;
			if (endOfRope) {
				ChangeCurrentState(GrapplingHookStates.extended, false);
			}
			else {
				ChangeCurrentState(GrapplingHookStates.attached, false);
			}
		}
		#endregion
		#region Debug
		protected void OnGUI()
		{
			GUI.Box(new Rect(0, 75f, 500f, 30f), this.GetType().ToString() + " Current State: " + currentStateName);
		}
		#endregion
		#region Updates
		public new void Update()
		{
			base.Update();
		}
		#endregion

		// Fetches the grappling head from the pool controller.
		private GameObject getPooledHook()
		{
			GameObject grappleHead = DI_PoolManager.getPooledObject(properties.grappleHeadPrefab);
			if (grappleHead == null) {
				log(DI_DebugLevel.CRITICAL, "Attempted to fetch the grapple head prefab but its not currently being pooled.");
			}
			return grappleHead;
		}

		private void resetHookHead(Rigidbody hookHead)
		{
			log(DI_DebugLevel.INFO, "Resetting the hook head to position: " + origin.position.x + "," + origin.position.y + "," + origin.position.z);
			hookHead.Sleep();
			hookHead.isKinematic = true;
			hookHead.velocity.Set(0f,0f,0f);
			hookHead.angularVelocity.Set(0f,0f,0f);
			hookHead.transform.position = origin.position;
			hookHead.transform.rotation = origin.rotation;
			hookHead.isKinematic = false;
			hookHead.useGravity = true;
			hookHead.WakeUp();
			lineRenderer.enabled = false;
			ropeCoilInstance.SetActive(false);
		}

		private void shootHook()
		{
			log(DI_DebugLevel.INFO, "Shooting a new hook");
			GameObject hook = getPooledHook();
			hookPrefabInstance = hook;
			ropeCoilInstance = DI_PoolManager.getPooledObject(properties.grappleRopeCoilPrefab);
			log(DI_DebugLevel.INFO, "Activating the returned prefab.");
			log(DI_DebugLevel.INFO, "Id: " + hook.GetInstanceID());
			Rigidbody hookRigidbody = hook.GetComponent<Rigidbody>();
			lineRenderer = hookPrefabInstance.transform.GetComponentInChildren<LineRenderer>();
			ropeCollider = hookPrefabInstance.transform.GetComponentInChildren<CapsuleCollider>();
			resetHookHead(hookRigidbody);
			hook.SetActive(true);

			ropeCollider.center = Vector3.zero;
			ropeCollider.direction = 2;
			lineRenderer.SetWidth(properties.lineWidth, properties.lineWidth);
			ropeCollider.radius = properties.lineWidth;
			hook.gameObject.transform.LookAt(target);
			hook.GetComponent<HookHead>().setMaxDistance(properties.lineRange);
			hookRigidbody.AddForce(hook.transform.forward * properties.shotForce, ForceMode.Impulse);
			//Debug.Log("Calling create rope with length: " + properties.lineRange + " and segments: " + properties.segments);
			//GrapplingRopeFactory.createRope(hook, properties.grappleRopePrefab, properties.lineRange, properties.segments);
		}

		// State Definitions
		#region Retracted
		protected void retracted_Enter(Enum previous)
		{
			fireDelayTimer = 0.0f;
			// Clean up the rope.
			if (previous.ToString() != "retracted") {
				//GrapplingRopeFactory.destroyRope(hookPrefabInstance);
			}
		}

		protected void retracted_Update()
		{
			if (onUse) {
				fireDelayTimer = properties.fireDelay;
				onUse = false;
			}

			if (fireDelayTimer > 0.0f) {
				fireDelayTimer -= Time.deltaTime;
			}
			else if (fireDelayTimer < 0.0f) {
				ChangeCurrentState(GrapplingHookStates.extending, false);
			}
		}
		#endregion
		#region Extending
		protected void extending_Enter(Enum previous)
		{
			// Spawn the prefab.
			shootHook();
		}

		protected void extending_Update()
		{
			// Check to see if the extension is complete.
			lineRenderer.SetPosition(0, hookPrefabInstance.transform.position);
		}
		#endregion
		#region Extended
		protected void extended_Update()
		{
			if (onUse) {
				onUse = false;
				ChangeCurrentState(GrapplingHookStates.retracted, false);
			}
		}
		protected void extended_Exit(Enum previous)
		{
			// Destroy the hook
			hookPrefabInstance.SetActive(false);
		}
		#endregion
		#region Attached
		protected void attached_Enter(Enum previous)
		{
			Ray ropeCast = new Ray(hookPrefabInstance.transform.position, Vector3.down);
			RaycastHit hit;
			if (Physics.Raycast(ropeCast, out hit)) {
				lineRenderer.enabled = true;
				lineRenderer.SetPosition(0, hookPrefabInstance.transform.position);
				lineRenderer.SetPosition(1, hit.point);
				ropeCollider.transform.position = hookPrefabInstance.transform.position + (hit.point - hookPrefabInstance.transform.position) / 2;
				ropeCollider.transform.LookAt(hookPrefabInstance.transform);
				ropeCollider.height = (hit.point - hookPrefabInstance.transform.position).magnitude;
				ropeCollider.enabled = true;
				ropeCoilInstance.transform.position = hit.point;
				ropeCoilInstance.SetActive(true);
			}
		}
		protected void attached_Update()
		{
			if (onUse) {
				onUse = false;
				ChangeCurrentState(GrapplingHookStates.retracted, false);
			}
		}
		protected void attached_Exit(Enum previous)
		{
			// Destroy the hook
			hookPrefabInstance.SetActive(false);
			ropeCoilInstance.SetActive(false);
		}
		#endregion
	}
}