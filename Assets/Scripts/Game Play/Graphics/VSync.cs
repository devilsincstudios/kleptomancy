﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Graphics
{
	public static class VSync
	{
		public static void setValue(float value)
		{
			QualitySettings.vSyncCount = (int)value;
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-VSync", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-VSync", 0f);
			setValue(value);
			return value;
		}
	}
}