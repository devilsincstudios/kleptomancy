﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine.UI;
using UnityEngine;
using UnityEditor;

using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using DI.Core.Input;

namespace DI.UI.Menu.Input
{
	public static class Input_BuildAxisList
	{
		[MenuItem("Devil's Inc Studios/Input/Build Axis List")]
		public static void BuildAxisList()
		{
			UnityEngine.Object inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];
			SerializedObject serializedObject = new SerializedObject(inputManager);
			SerializedProperty axisArray = serializedObject.FindProperty("m_Axes");
			DI_AxisNames axisNames = new DI_AxisNames();
			axisNames.names = new List<string>();

			for( int iteration = 0; iteration < axisArray.arraySize; ++iteration )
			{
				SerializedProperty axisElement = axisArray.GetArrayElementAtIndex(iteration);
				axisNames.names.Add(axisElement.FindPropertyRelative("m_Name").stringValue);
			}

			string json = JsonUtility.ToJson(axisNames, true);
			try {
				File.WriteAllText("Assets/Resources/Config/axes.json", json);
				Debug.Log("Axis names successfully wrote to file.");
			}
			catch (Exception e) {
				Debug.LogException(e);
			}
		}
	}
}