﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Quests
{
	public enum ObjectiveTypes
	{
		bring,
		collect,
		mechanism,
		money,
		puzzle,
		travel,
		use,
		unset,
	}
}