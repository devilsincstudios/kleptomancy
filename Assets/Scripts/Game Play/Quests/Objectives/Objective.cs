﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Mechanics.Loot;
using DI.Core.Events;
using DI.Core.Behaviours;
using System.Collections.Generic;
using DI.Core.Actions;

namespace DI.Quests
{
	public class Objective : DI_MonoBehaviour
	{
		public int objectiveId;
		public string objectiveName;
		public string objectiveDescription;
		[SerializeField]
		protected bool hidden = false;
		[SerializeField]
		protected bool optional = false;
		[SerializeField]
		protected bool completed = false;
		public List<Action> update;
		public List<Action> reveal;
		public List<Action> hide;
		public List<Action> complete;
		public List<Action> add;
		public List<Action> remove;

		public bool isOptional()
		{
			return optional;
		}
		public bool isHidden()
		{
			return hidden;
		}
		public void setHidden(bool state)
		{
			hidden = state;
			if (hidden) {
				onReveal();
			}
			else {
				onHide();
			}
		}
		public virtual bool isComplete()
		{
			return false;
		}
		public virtual float getProgress()
		{
			return 0f;
		}
		public virtual ObjectiveTypes getType()
		{
			return ObjectiveTypes.unset;
		}

		public void Awake()
		{
			// Set the objective id automaticly
			for (int index = 0; index < transform.parent.childCount; index++) {
				if (transform.parent.GetChild(index).gameObject == this.gameObject) {
					objectiveId = index;
					break;
				}
			}
		}

		protected void onUpdate()
		{
			for (int index = 0; index < update.Count; index++) {
				update[index].doAction();
			}
			DI_EventCenter<Objective>.invoke("OnObjectiveUpdate", this);
		}
		protected void onReveal()
		{
			for (int index = 0; index < reveal.Count; index++) {
				reveal[index].doAction();
			}
			DI_EventCenter<Objective>.invoke("OnObjectiveReveal", this);
		}
		protected void onHide()
		{
			for (int index = 0; index < hide.Count; index++) {
				hide[index].doAction();
			}
			DI_EventCenter<Objective>.invoke("OnObjectiveHide", this);
		}
		protected void onComplete()
		{
			for (int index = 0; index < complete.Count; index++) {
				Debug.Log("Doing complete action");
				Debug.Log(complete[index].getType().ToString());
				complete[index].doAction();
			}
			DI_EventCenter<Objective>.invoke("OnObjectiveComplete", this);
		}
		protected void onAdd()
		{
			for (int index = 0; index < add.Count; index++) {
				add[index].doAction();
			}
			DI_EventCenter<Objective>.invoke("OnObjectiveAdd", this);
		}
		protected void onRemove()
		{
			for (int index = 0; index < remove.Count; index++) {
				remove[index].doAction();
			}
			DI_EventCenter<Objective>.invoke("OnObjectiveRemove", this);
		}
	}
}