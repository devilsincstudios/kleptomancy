﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.Audio;
using UnityEngine;
using System.Collections;

namespace DI.Audio
{
	public class AudioSettings : DI_MonoBehaviour
	{
		public AudioMixer mixer;

		public void OnEnable()
		{
			// There is a delay between start up and the audio system registering new volume settings.
			// Run a coroutine with a slight delay to work around this.
			StartCoroutine(delayedLoad());
		}

		public IEnumerator delayedLoad()
		{
			yield return new WaitForEndOfFrame();
			loadSettings();
		}

		public void loadSettings()
		{
			Volume.loadValue(mixer, "Master");
			Volume.loadValue(mixer, "Ambient");
			Volume.loadValue(mixer, "BackgroundMusic");
			Volume.loadValue(mixer, "Menu");
			Volume.loadValue(mixer, "SoundFX");
			Volume.loadValue(mixer, "Voices");
		}
	}
}
