﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using System.Collections;

namespace DI.Core
{
	public class TimedDestructor : DI_MonoBehaviour
	{
		public bool destroy = false;
		public float lifeTime = 2f;

		public void OnEnable()
		{
			StartCoroutine("despawn");
		}

		public IEnumerator despawn()
		{
			yield return new WaitForSeconds(lifeTime);
			if (destroy) {
				GameObject.Destroy(this);
			}
			else {
				gameObject.SetActive(false);
			}
		}
	}
}