﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Cameras;

using DI.Core.Behaviours;
using DI.Core.Debug;
using DI.Controllers;

namespace DI.UI
{
	public class DevSettingsMenu : DI_MonoBehaviour
	{
		[Header("Player Movement Script")]
		public DI.Controllers.Locomotion controller;
		public AutoCam activeCamera;

		[Header("Movement Settings Fields")]
		public Text maxMoveSpeedValue;
		public Slider maxMoveSpeedSlider;

		public Text maxMoveSpeedRunningValue;
		public Slider maxMoveSpeedRunningSlider;

		public Text maxMoveSpeedSneakingValue;
		public Slider maxMoveSpeedSneakingSlider;

		public Text moveSpeedIncreaseRateValue;
		public Slider moveSpeedIncreaseRateSlider;

		public Text turnSpeedIncreaseRateValue;
		public Slider turnSpeedIncreaseRateSlider;

		public Text turnSpeedValue;
		public Slider turnSpeedSlider;

		[Header("Jump Settings Fields")]
		public Text jumpStrengthValue;
		public Slider jumpStrengthSlider;

		public Text jumpFallSpeedValue;
		public Slider jumpFallSpeedSlider;
		public Text jumpFramesValue;
		public Slider jumpFramesSlider;

		[Header("Camera Settings Fields")]
		public Text cameraTurnSpeedValue;
		public Slider cameraTurnSpeedSlider;
		public Text cameraSmoothTimeValue;
		public Slider cameraSmoothTimeSlider;

		private float maxSpeed;
		private float maxRunSpeed;
		private float maxSneakSpeed;
		private float moveSpeedIncrease;
		private float turnSpeedIncrease;
		private float turnSpeed;
		private float jumpStrength;
		private float jumpFallSpeed;
		private float jumpFrames;
		private float cameraTurnSpeed;
		private float cameraSmoothTime;

		public void Start()
		{
			maxMoveSpeedSlider.value = controller.characterProperties.getMaxMovementSpeed();
			maxSpeed = controller.characterProperties.getMaxMovementSpeed();

			maxMoveSpeedRunningSlider.value = controller.characterProperties.runSpeedSettings.maxMovementSpeed;
			maxRunSpeed = controller.characterProperties.runSpeedSettings.maxMovementSpeed;

			maxMoveSpeedSneakingSlider.value = controller.characterProperties.sneakSpeedSettings.maxMovementSpeed;
			maxSneakSpeed = controller.characterProperties.sneakSpeedSettings.maxMovementSpeed;

			turnSpeedIncreaseRateSlider.value = controller.characterProperties.getTurnSpeedIncreaseRate();
			turnSpeedIncrease = controller.characterProperties.getTurnSpeedIncreaseRate();

			moveSpeedIncreaseRateSlider.value = controller.characterProperties.getMovementSpeedIncreaseRate();
			moveSpeedIncrease = controller.characterProperties.getMovementSpeedIncreaseRate();

			turnSpeedSlider.value = controller.characterProperties.getMaxTurnSpeed();
			turnSpeed = controller.characterProperties.getMaxTurnSpeed();

			jumpFallSpeedSlider.value = controller.characterProperties.jumpSettings.maxFallSpeed;
			jumpFallSpeed = controller.characterProperties.jumpSettings.maxFallSpeed;

			jumpFramesSlider.value = controller.characterProperties.jumpSettings.jumpFrames;
			jumpFrames = controller.characterProperties.jumpSettings.jumpFrames;

			cameraTurnSpeedSlider.value = activeCamera.m_TurnSpeed;
			cameraTurnSpeed = activeCamera.m_TurnSpeed;

			cameraSmoothTimeSlider.value = activeCamera.m_SmoothTurnTime;
			cameraSmoothTime = activeCamera.m_SmoothTurnTime;
		}

		public void Reset()
		{
			maxMoveSpeedSlider.value = maxSpeed;
			maxMoveSpeedRunningSlider.value = maxRunSpeed;
			maxMoveSpeedSneakingSlider.value = maxSneakSpeed;
			turnSpeedIncreaseRateSlider.value = turnSpeedIncrease;
			moveSpeedIncreaseRateSlider.value = moveSpeedIncrease;
			turnSpeedSlider.value = turnSpeed;
			jumpFallSpeedSlider.value = jumpFallSpeed;
			jumpFramesSlider.value = jumpFrames;
			cameraTurnSpeedSlider.value = cameraTurnSpeed;
			cameraSmoothTimeSlider.value = cameraSmoothTime;
		}

		public void Update()
		{
			// ============================ Movement ============================

			// Max Speed
			maxMoveSpeedValue.text = maxMoveSpeedSlider.value.ToString();
			controller.characterProperties.setMaxMovementSpeed(maxMoveSpeedSlider.value);

			// Max Run Speed
			maxMoveSpeedRunningValue.text = maxMoveSpeedRunningSlider.value.ToString();
			controller.characterProperties.runSpeedSettings.maxMovementSpeed = maxMoveSpeedRunningSlider.value;

			// Max Sneak Speed
			maxMoveSpeedSneakingValue.text = maxMoveSpeedSneakingSlider.value.ToString();
			controller.characterProperties.sneakSpeedSettings.maxMovementSpeed = maxMoveSpeedSneakingSlider.value;

			// Speed Increase Rate
			moveSpeedIncreaseRateValue.text = moveSpeedIncreaseRateSlider.value.ToString();
			controller.characterProperties.setMovementSpeedIncreaseRate(moveSpeedIncreaseRateSlider.value);

			// Turn Speed Rate
			turnSpeedIncreaseRateValue.text = turnSpeedIncreaseRateSlider.value.ToString();
			controller.characterProperties.setTurnSpeedIncreaseRate(turnSpeedIncreaseRateSlider.value);

			// Turn Speed
			turnSpeedValue.text = turnSpeedSlider.value.ToString();
			controller.characterProperties.setMaxTurnSpeed(turnSpeedSlider.value);

			// ============================ Jumping ============================
			// Strength
			jumpStrengthValue.text = jumpStrengthSlider.value.ToString();
			controller.characterProperties.jumpSettings.jumpStrength = jumpStrengthSlider.value;

			// FallSpeed
			jumpFallSpeedValue.text = jumpFallSpeedSlider.value.ToString();
			controller.characterProperties.jumpSettings.maxFallSpeed = jumpFallSpeedSlider.value;

			// JumpFrames
			jumpFramesValue.text = jumpFramesSlider.value.ToString();
			controller.characterProperties.jumpSettings.jumpFrames = jumpFramesSlider.value;

			// ============================ Camera ============================
			// Turn Speed
			cameraTurnSpeedValue.text = cameraTurnSpeedSlider.value.ToString();
			activeCamera.m_TurnSpeed = cameraTurnSpeedSlider.value;
			// Smooth Time
			cameraSmoothTimeValue.text = cameraSmoothTimeSlider.value.ToString();
			activeCamera.m_SmoothTurnTime = cameraSmoothTimeSlider.value;
		}
	}
}
