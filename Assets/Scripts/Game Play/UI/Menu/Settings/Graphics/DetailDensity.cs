﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using ImageEffects;

namespace DI.UI.Menu.Settings
{
	public class DetailDensity : GraphicsSettings
	{
		public override void sliderInit()
		{
			slider.minValue = 0f;
			slider.maxValue = 100f;
			slider.value = 40f;
			slider.wholeNumbers = true;
		}

		public override void setDefaultLow()
		{
			setValue(20);
		}

		public override void setDefaultMedium()
		{
			setValue(30);
		}

		public override void setDefaultHigh()
		{
			setValue(40);
		}

		public override void setDefaultUltra()
		{
			setValue(60);
		}

		public override void setValue(float value)
		{
			DI.Graphics.DetailDensity.setValue(value);
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(DI.Graphics.DetailDensity.loadValue());
		}
	}
}