﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;

namespace DI.UI.Menus
{
	public class UpdateSlider : DI_MonoBehaviour
	{
		public Text value;

		public void updateValue(float newValue)
		{
			value.text = newValue + "";
		}
	}
}