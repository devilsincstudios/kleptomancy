// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections;

using DI.Core.Behaviours;

namespace DI.UI.Misc {
	[AddComponentMenu("Devil's Inc Studios/UI/Misc/Face Player")]
	public class FacePlayer : DI_MonoBehaviour
	{
		public bool updating = true;
		public bool correctRotation = false;
		public bool lockX = false;
		private float startingX = 0.0f;
		public float updateDelay = 0.2f;
		public float updateDistance = 30f;

		public void OnEnable()
		{
			if (lockX) {
				startingX = transform.rotation.x;
			}

			StartCoroutine("updateFacing");
		}

		public void OnDisable()
		{
			StopAllCoroutines();
		}
		
		public void OnBecameVisible()
		{
			updating = true;
		}

		public void OnBecameInvisible()
		{
			updating = false;
		}

		public IEnumerator updateFacing()
		{
			yield return new WaitForSeconds(UnityEngine.Random.Range(0, updateDelay));

			while (true)
			{
				if (updating) {
					if (Vector3.Distance(transform.position, Camera.main.transform.position) <= updateDistance) {
						transform.LookAt(UnityEngine.Camera.main.transform.position);

						if (correctRotation) {
							transform.Rotate(0, 180, 0);
						}
						if (lockX) {
							transform.Rotate(transform.rotation.x * -1, 0, 0);
							transform.Rotate(startingX, 0, 0);
						}
					}
				}
				yield return new WaitForSeconds(updateDelay);
			}
		}
	}
}