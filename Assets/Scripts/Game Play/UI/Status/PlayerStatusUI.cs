﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace DI.UI
{
	public class PlayerStatusUI : DI_MonoBehaviour
	{
		public Text staminaText;
		public Image staminaFillBar;
		public DI.Controllers.Locomotion playerController;

		public void Update()
		{
			staminaText.text = "Stamina: " + playerController.characterProperties.staminaSettings.currentStamina;
			staminaFillBar.fillAmount = playerController.characterProperties.staminaSettings.currentStamina / playerController.characterProperties.staminaSettings.maxStamina;
		}
	}
}