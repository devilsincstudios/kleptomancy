﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Quests;
using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;

namespace DI.UI
{
	public class QuestDisplayManager : DI_MonoBehaviour
	{
		public Quest trackedQuest;
		public Text questName;
		public RectTransform objectivesPanel;
		public GameObject objectivesPrefab;
		public float uiYOffset = 30f;
		public float objectiveYOffset = 15f;
		public Color completedColor;
		public Color inProgressColor;

		protected bool initalized = false;

		[Header("Viewable for debug reasons")]
		public List<GameObject> objectives;
		public Dictionary<int, int> objectiveIndexes;
		public void OnEnable()
		{
			DI_EventCenter<Objective>.addListener("OnObjectiveComplete", handleOnObjectiveUpdated);
			DI_EventCenter<Objective>.addListener("OnObjectiveUpdate", handleOnObjectiveUpdated);
			DI_EventCenter<Objective>.addListener("OnObjectiveReveal", handleOnObjectivesChanged);
			DI_EventCenter<Objective>.addListener("OnObjectiveHide", handleOnObjectivesChanged);
			DI_EventCenter<Objective>.addListener("OnObjectiveAdd", handleOnObjectivesChanged);
			DI_EventCenter<Objective>.addListener("OnObjectiveRemove", handleOnObjectivesChanged);

			objectiveIndexes = new Dictionary<int, int>();

			if (!initalized) {
				// Get the quest manager version of the quest
				// QM clones the quest so we want to use that version.
				trackedQuest = QuestManager.instance.getActiveQuest();
				addObjectives();
				initalized = true;
			}
		}

		public void OnDisable()
		{
			DI_EventCenter<Objective>.removeListener("OnObjectiveComplete", handleOnObjectiveUpdated);
			DI_EventCenter<Objective>.removeListener("OnObjectiveUpdate", handleOnObjectiveUpdated);
			DI_EventCenter<Objective>.removeListener("OnObjectiveReveal", handleOnObjectivesChanged);
			DI_EventCenter<Objective>.removeListener("OnObjectiveHide", handleOnObjectivesChanged);
			DI_EventCenter<Objective>.removeListener("OnObjectiveAdd", handleOnObjectivesChanged);
			DI_EventCenter<Objective>.removeListener("OnObjectiveRemove", handleOnObjectivesChanged);
		}

		public void removeObjectives()
		{
			objectiveIndexes.Clear();
			for (int index = objectivesPanel.transform.childCount - 1; index >= 0; index--) {
				Destroy(objectivesPanel.transform.GetChild(index).gameObject);
			}
		}

		public void updateObjectives()
		{
			int guiIndex = 0;
			for (int index = 0; index < trackedQuest.objectives.Count; index++) {
				Objective objective = trackedQuest.objectives[index];
				if (!objective.isHidden()) {
					GameObject objectiveGO = objectivesPanel.GetChild(guiIndex).gameObject;
					Text title = (Text) objectiveGO.transform.GetChild(0).GetComponent(typeof(Text));
					Image completed = (Image) objectiveGO.transform.GetChild(0).GetChild(0).GetComponent(typeof(Image));
					title.text = objective.objectiveName;

					if (objective.isComplete()) {
						completed.color = completedColor;
					}
					else {
						completed.color = inProgressColor;
					}
					++guiIndex;
				}
			}
		}

		public void addObjectives()
		{
			removeObjectives();
			int uiIndex = 0;
			float objectiveHeight = 0f;
			questName.text = trackedQuest.questName;
			for (int index = 0; index < trackedQuest.objectives.Count; index++) {
				if (!trackedQuest.objectives[index].isHidden()) {
					GameObject objective = GameObject.Instantiate(objectivesPrefab);
					RectTransform objectiveTransform = objective.GetComponent<RectTransform>();
					objectiveTransform.SetParent(objectivesPanel, false);
					objectiveTransform.anchoredPosition3D = new Vector3(0f, (uiIndex * -objectiveTransform.sizeDelta.y) + -objectiveYOffset);
					objectiveHeight = objectiveTransform.sizeDelta.y;
					objectiveIndexes.Add(index, uiIndex);
					uiIndex++;
				}
			}

			Debug.Log("uiIndex:" + (uiIndex));
			Debug.Log("Objectives Count: " + trackedQuest.objectives.Count);

			if (uiIndex != 0) {
				objectivesPanel.gameObject.SetActive(true);
				Vector3 position = objectivesPanel.anchoredPosition3D;
				float height = (uiIndex * objectiveHeight);
				// Find Center, Take Half
				float yOffset = -((height/2 + uiYOffset));

				objectivesPanel.sizeDelta = new Vector2(objectivesPanel.sizeDelta.x, height);

				objectivesPanel.anchoredPosition3D = new Vector3(position.x, yOffset, position.z);
			}
			else {
				objectivesPanel.gameObject.SetActive(false);
			}

			updateObjectives();
		}

		public void handleOnObjectiveUpdated(Objective objective)
		{
			Debug.Log("handleOnObjectiveUpdated");
			updateObjectives();
		}

		public void handleOnObjectivesChanged(Objective objective)
		{
			Debug.Log("handleOnObjectivesChanged");
			removeObjectives();
			addObjectives();
		}
	}
}