﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;
using DI.Entities.Player;
using DI.Mechanics.Weapons;
using DI.Mechanics.Loot;
using DI.Core.Events;

namespace DI.UI
{
	public class WeaponHUD : DI_MonoBehaviour
	{
		[Header("UI Settings")]
		[Header("Selected Weapon")]
		public RawImage selectedWeaponIcon;
		[Header("Primary Ammo")]
		public RawImage primaryAmmoIcon;
		public Text primaryAmmoCount;
		[Header("Secondary Ammo")]
		public RawImage secondaryAmmoIcon;
		public Text secondaryAmmoCount;

		[Header("Player Settings")]
		public PlayerController player;

		public void OnEnable()
		{
			DI_EventCenter<LootWeapon, LootAmmo>.addListener("OnFireWeapon", handleOnFireWeapon);
			OnChangeWeapon();
		}

		public void OnDisable()
		{
			DI_EventCenter<LootWeapon, LootAmmo>.removeListener("OnFireWeapon", handleOnFireWeapon);
		}

		public void OnChangeWeapon()
		{
			selectedWeaponIcon.texture = player.inventory.selectedWeapon.icon;
			primaryAmmoIcon.texture = player.inventory.selectedWeapon.primaryAmmo.icon;
			secondaryAmmoIcon.texture = player.inventory.selectedWeapon.secondaryAmmo.icon;
			primaryAmmoCount.text = "" + player.inventory.getAmmo(player.inventory.selectedWeapon.primaryAmmo);
			secondaryAmmoCount.text = "" + player.inventory.getAmmo(player.inventory.selectedWeapon.secondaryAmmo);
		}

		public void handleOnFireWeapon(LootWeapon weapon, LootAmmo ammo)
		{
			primaryAmmoCount.text = "" + player.inventory.getAmmo(player.inventory.selectedWeapon.primaryAmmo);
			secondaryAmmoCount.text = "" + player.inventory.getAmmo(player.inventory.selectedWeapon.secondaryAmmo);
		}
	}
}