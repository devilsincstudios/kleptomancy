﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Controllers;

namespace DI.UI
{
	public class ManaBar : DI_MonoBehaviour
	{
		[Header("UI Element Settings")]
		public Image barFill;
		public Text barText;

		protected Player player;

		public IEnumerator updateBar()
		{
			while (true) {
				float manaPercent = player.getCurrentMana() / player.getMaxMana();
				barText.text = "Mana: " + Mathf.Round(player.getCurrentMana() * 10) / 10 + " (" + Mathf.Round(manaPercent * 1000f) / 10f + "%)";
				barFill.fillAmount = manaPercent;
				yield return new WaitForSeconds(0.2f);
			}
		}

		public void OnEnable()
		{
			player = GameObject.Find("Player").GetComponent<Player>();
			StartCoroutine(updateBar());
		}

		public void OnDisable()
		{
			StopAllCoroutines();
		}
	}
}