﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using DI.Core.Behaviours;
using DI.Core.Events;

namespace DI.UI
{
	public class NoiseMeterDisplay : DI_MonoBehaviour
	{
		[Header("Noise Color Settings")]
		[Tooltip("0% - 20%")]
		public Color noiseStageOne;
		[Tooltip("20% - 50%")]
		public Color noiseStageTwo;
		[Tooltip("50% - 75%")]
		public Color noiseStageThree;
		[Tooltip("75% - 85%")]
		public Color noiseStageFour;
		[Tooltip("85% - 100%")]
		public Color noiseStageFive;

		[Header("UI Element Settings")]
		public Image noiseBarFill;
		public Text noiseBarText;

		public Animator animator;

		public void OnEnable()
		{
			DI_EventCenter<float>.addListener("OnNoiseMeterUpdate", handleOnNoiseMeterUpdate);
		}

		public void OnDisable()
		{
			DI_EventCenter<float>.removeListener("OnNoiseMeterUpdate", handleOnNoiseMeterUpdate);
		}

		public void handleOnNoiseMeterUpdate(float noisePercent)
		{
			if (noisePercent > 85f) {
				noiseBarFill.color = noiseStageFive;
			}
			else if (noisePercent > 75f) {
				noiseBarFill.color = noiseStageFour;
			}
			else if (noisePercent > 50f) {
				noiseBarFill.color = noiseStageThree;
			}
			else if (noisePercent > 20f) {
				noiseBarFill.color = noiseStageTwo;
			}
			else {
				noiseBarFill.color = noiseStageOne;
			}

			noiseBarText.text = "Noise: " + Mathf.Round(noisePercent * 100)/100 + "%";
			noiseBarFill.fillAmount = noisePercent / 100f;

			if (noisePercent < 85f) {
				animator.SetBool("Blinking", false);
			}
			else {
				animator.SetBool("Blinking", true);
			}
		}
	}
}