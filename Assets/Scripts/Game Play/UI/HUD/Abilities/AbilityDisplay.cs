﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Events;
using UnityEngine.UI;
using UnityEngine;
using DI.Core.Behaviours;
using DI.Mechanics.Abilities;
using System.Collections.Generic;
using System.Collections;

namespace DI.UI.HUD
{
	public class AbilityDisplay : DI_MonoBehaviour
	{
		public SelectAbility selectAbility;

		public AbilityDisplayProperty abilityOne;
		public AbilityDisplayProperty abilityTwo;
		public AbilityDisplayProperty abilityThree;
		public AbilityDisplayProperty abilityFour;

		public Color abilityColorNormal;
		public Color abilityColorSelected;
		public Color abilityColorBackground;

		public void OnEnable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.addListener("OnUseAbility", handleOnUseAbility);
			DI_EventCenter<Ability>.addListener("OnSelectAbility", handleOnSelectAbility);
			updateGUI();
			StartCoroutine("updateCooldowns");
		}

		public void OnDisable()
		{
			DI_EventCenter<Ability, Transform, Vector3>.removeListener("OnUseAbility", handleOnUseAbility);
			DI_EventCenter<Ability>.removeListener("OnSelectAbility", handleOnSelectAbility);
			StopAllCoroutines();
		}


		public IEnumerator updateCooldowns()
		{
			while (true) {
				yield return new WaitForSeconds(0.1f);
				updateCooldown(abilityOne.abilityIcon, selectAbility.abilities, 0);
				updateCooldown(abilityTwo.abilityIcon, selectAbility.abilities, 1);
				updateCooldown(abilityThree.abilityIcon, selectAbility.abilities, 2);
				updateCooldown(abilityFour.abilityIcon, selectAbility.abilities, 3);
			}
		}

		public void handleOnSelectAbility(Ability ability)
		{
			updateGUI();
		}

		public void handleOnUseAbility(Ability ability, Transform spawnLocation, Vector3 target)
		{
			updateGUI();
		}

		public void updateCooldown(Image uiElement, List<Ability> abilityList, int index)
		{
			if (abilityList.Count >= index) {
				if (abilityList[index] != null) {
					float fillAmount = 1f;
					if (selectAbility.abilities[index].cooldown != 0f) {
						if (selectAbility.abilities[index].cooldownRemaining > 0f) {
							fillAmount = selectAbility.abilities[index].cooldownRemaining / selectAbility.abilities[index].cooldown;
						}
					}
					uiElement.fillAmount = fillAmount;
				}
			}
		}

		public void updateCharges(Text uiElement, List<Ability> abilityList, int index)
		{
			if (abilityList.Count >= index) {
				if (abilityList[index] != null) {
					if (abilityList[index].costType == CostTypes.charge) {
						uiElement.text = "" + abilityList[index].currentCharges;
						uiElement.gameObject.SetActive(true);
					}
					else {
						uiElement.gameObject.SetActive(false);
					}
				}
			}
		}

		public void setIcon(Image uiElement, List<Ability> abilityList, int index, bool background)
		{
			if (abilityList.Count >= index) {
				if (abilityList[index] != null) {
					uiElement.sprite = abilityList[index].icon;
					if (!background) {
						if (selectAbility.selectedAbility.ability == abilityList[index].ability) {
							uiElement.color = abilityColorSelected;
						}
					}
				}
				else {
					uiElement.color = Color.clear;
				}
			}
		}

		public void updateGUI()
		{
			// Reset the coloring
			abilityOne.abilityIcon.color = abilityColorNormal;
			abilityTwo.abilityIcon.color = abilityColorNormal;
			abilityThree.abilityIcon.color = abilityColorNormal;
			abilityFour.abilityIcon.color = abilityColorNormal;
			abilityOne.abilityBackground.color = abilityColorBackground;
			abilityTwo.abilityBackground.color = abilityColorBackground;
			abilityThree.abilityBackground.color = abilityColorBackground;
			abilityFour.abilityBackground.color = abilityColorBackground;

			// Set the icons for the abilities
			setIcon(abilityOne.abilityIcon, selectAbility.abilities, 0, false);
			setIcon(abilityTwo.abilityIcon, selectAbility.abilities, 1, false);
			setIcon(abilityThree.abilityIcon, selectAbility.abilities, 2, false);
			setIcon(abilityFour.abilityIcon, selectAbility.abilities, 3, false);
			// Set the icons for the abilities
			setIcon(abilityOne.abilityBackground, selectAbility.abilities, 0, true);
			setIcon(abilityTwo.abilityBackground, selectAbility.abilities, 1, true);
			setIcon(abilityThree.abilityBackground, selectAbility.abilities, 2, true);
			setIcon(abilityFour.abilityBackground, selectAbility.abilities, 3, true);

			updateCharges(abilityOne.abilityCharges, selectAbility.abilities, 0);
			updateCharges(abilityTwo.abilityCharges, selectAbility.abilities, 1);
			updateCharges(abilityThree.abilityCharges, selectAbility.abilities, 2);
			updateCharges(abilityFour.abilityCharges, selectAbility.abilities, 3);
		}
	}
}