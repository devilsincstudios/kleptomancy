﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine.UI;
using System;
namespace DI.UI
{
	[Serializable]
	public struct AbilityDisplayProperty
	{
		public Image abilityIcon;
		public Image abilityBackground;
		public Text abilityCharges;
	}
}