﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Core.Helpers
{
	public struct DI_ViewInfo
	{
		public Transform transform;
		public float viewRadius;
		public LayerMask obstacleMask;

		public DI_ViewInfo(Transform _transform, float _viewRadius, LayerMask _obstacleMask)
		{
			transform = _transform;
			viewRadius = _viewRadius;
			obstacleMask = _obstacleMask;
		}
	}
}