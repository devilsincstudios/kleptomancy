﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
//
// Thanks to Sebastian Lague for the tutorial on FoV @ https://github.com/SebLague/Field-of-View
//
using UnityEngine;

namespace DI.Core.Helpers
{
	public static class DI_ViewCast
	{
		public static DI_ViewCastInfo ViewCast(DI_ViewInfo viewInfo, float globalAngle) {
			Vector3 dir = DI_Math.DirectionFromAngle(globalAngle);
			RaycastHit hit;
			if (Physics.Raycast (viewInfo.transform.position, dir, out hit, viewInfo.viewRadius, viewInfo.obstacleMask)) {
				return new DI_ViewCastInfo (true, hit.point, hit.distance, globalAngle);
			}
			else {
				return new DI_ViewCastInfo (false, viewInfo.transform.position + dir * viewInfo.viewRadius, viewInfo.viewRadius, globalAngle);
			}
		}
	}
}