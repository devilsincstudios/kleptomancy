// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Core.Helpers
{
	static public class DI_Math
	{
		// Helper to wrap angles greater than 360
		static public float wrapAngle(float angle)
		{
			if (angle < 0) {
				angle += 360;
			}
			if (angle < -360F) {
				while (angle < -360F) {
					angle += 360F;
				}
			}
			if (angle > 360F) {
				while (angle > 360F) {
					angle -= 360F;
				}
			}
			if (angle == 360f) {
				angle = 0f;
			}
			return angle;
		}

		static public Vector3 wrapAngle(Vector3 position)
		{
			return new Vector3(wrapAngle(position.x), wrapAngle(position.y), wrapAngle(position.z));
		}

		// http://answers.unity3d.com/questions/819273/force-to-velocity-scaling.html#answer-819474
		
		static public float getRequiredVelocityChange(float aFinalSpeed, float aDrag)
		{
			float m = Mathf.Clamp01(aDrag * Time.fixedDeltaTime);
			return aFinalSpeed * m / (1 - m);
		}
		static public float getRequiredAcceleraton(float aFinalSpeed, float aDrag)
		{
			return getRequiredVelocityChange(aFinalSpeed, aDrag) / Time.fixedDeltaTime;
		}
		
		static public Vector3 getAcceleration(Vector3 velocity, float drag)
		{
			float x = getRequiredAcceleraton(velocity.x, drag);
			float y = getRequiredAcceleraton(velocity.y, drag);
			float z = getRequiredAcceleraton(velocity.z, drag);
			return new Vector3(x, y, z);
		}

		static public float ConvertGlobalAngleToLocal(float angleInDegrees, Transform localTransform)
		{
			return angleInDegrees += localTransform.eulerAngles.y;
		}

		static public Vector3 DirectionFromAngle(float angleInDegrees, Transform localTransform)
		{
			return DirectionFromAngle(ConvertGlobalAngleToLocal(angleInDegrees, localTransform));
		}

		static public Vector3 DirectionFromAngle(float angleInDegrees)
		{
			return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),0,Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
		}

		static public float roundFloatToHundreths(float input)
		{
			return (Mathf.Round((input * 100f)) / 100f);
		}
	}
}