﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
//
// Thanks to Sebastian Lague for the tutorial on FoV @ https://github.com/SebLague/Field-of-View
//
using UnityEngine;

namespace DI.Core.Helpers
{
	public static class DI_EdgeCast
	{
		public static DI_EdgeInfo FindEdge(DI_ViewInfo viewInfo, DI_ViewCastInfo minViewCast, DI_ViewCastInfo maxViewCast, int edgeResolveIterations=10, float edgeDistanceThreshold=0.5f)
		{
			float minAngle = minViewCast.angle;
			float maxAngle = maxViewCast.angle;
			Vector3 minPoint = Vector3.zero;
			Vector3 maxPoint = Vector3.zero;

			for (int i = 0; i < edgeResolveIterations; i++) {
				float angle = (minAngle + maxAngle) / 2;
				DI_ViewCastInfo newViewCast = DI_ViewCast.ViewCast(viewInfo, angle);

				bool edgeDstThresholdExceeded = Mathf.Abs (minViewCast.distance - newViewCast.distance) > edgeDistanceThreshold;
				if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded) {
					minAngle = angle;
					minPoint = newViewCast.point;
				} else {
					maxAngle = angle;
					maxPoint = newViewCast.point;
				}
			}

			return new DI_EdgeInfo (minPoint, maxPoint);
		}
	}
}