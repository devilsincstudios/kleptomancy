﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// Based on the tutorial from here:
// https://unitygem.wordpress.com/state-machine-basic/
// 
// And this blog
// https://roystanross.wordpress.com/2014/05/11/custom-character-controller-in-unity-part-2-implementation/
//
// I added delegates for fixed update and late update, cleaned up the coding style and integrated DI_DEBUG with it.
// We should still credit them even though its not required
// 

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;

using DI.Core.Debug;
using DI.Core.Behaviours;

namespace DI.Core.StateMachine
{
	public class StateMachine : DI_MonoBehaviour
	{
		private Dictionary<Enum,State>  states;
		private State currentState = null;

		private bool inTransition = false;
		private bool initialized = false;

		private State transitionQueue = null;
		private bool allowMultiTransition = false;
		private bool inExitMethod = false;
		public Enum CurrentState { get { return this.currentState.name; } }
		private State transitionSource = null;
		public Enum PreviousState { get { return this.transitionSource.name; } }
		private Action onUpdate = null;
		public string currentStateName;
		private Action onFixedUpdate = null;
		private Action onLateUpdate = null;

		private static void emptyUpdate() { }
		private static void emptyEnterExit(Enum state) { }

		protected virtual void Update()
		{
			this.onUpdate();
		}

		protected virtual void FixedUpdate()
		{
			this.onFixedUpdate();
		}

		protected virtual void LateUpdate()
		{
			this.onLateUpdate();
		}

		protected virtual void EarlyGlobalUpdate()
		{
		}

		protected virtual void LateGlobalUpdate()
		{
		}

		protected bool Initialized()
		{
			if (!initialized) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, this.GetType().ToString() + ": StateMachine is not initialized. You need to call InitializeStateMachine(bool allowMultiTransition = false )");
				return false;
			}
			return true;
		}

		private static T GetMethodInfo<T> (object obj, Type type, string method, T Default) where T : class
		{
			Type baseType = type;
			while (baseType != typeof(MonoBehaviour))
			{
				MethodInfo methodInfo = baseType.GetMethod(method, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
				if (methodInfo != null) {
					return Delegate.CreateDelegate(typeof(T), obj, methodInfo) as T;
				}
				baseType = baseType.BaseType;
			}
			return Default;
		}

		protected void InitializeStateMachine<T>(Enum initialState, bool multiTransition = false)
		{
			if (this.initialized) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "The StateMachine component on " + this.GetType().ToString() + " is already initialized.");
				return;
			}
			this.initialized = true;
			this.allowMultiTransition = multiTransition;
			var values = Enum.GetValues(typeof(T));
			this.states = new Dictionary<Enum, State>();
			for (int iteration = 0; iteration < values.Length; iteration++) {
				this.initialized = this.CreateNewState((Enum)values.GetValue(iteration));
			}
			this.currentState = this.states[initialState];
			this.inTransition = false;
			DI_Debug.writeLog(DI_DebugLevel.INFO, "Initialized with :" + this.currentState.ToString());
			this.currentState.enterMethod(currentState.name);
			// Set the current life cycle delegates = to the state's.
			this.onUpdate = this.currentState.updateMethod;
			this.onFixedUpdate = this.currentState.fixedUpdateMethod;
			this.onLateUpdate = this.currentState.lateUpdateMethod;
			currentStateName = initialState.ToString();
		}

		private bool CreateNewState(Enum newstate)
		{
			if (!this.Initialized()) {
				return false;
			}
			if (this.states.ContainsKey(newstate)) {
				DI_Debug.writeLog(DI_DebugLevel.INFO, "State: " + newstate + " is already registered in " + this.GetType().ToString());
				return false;
			}
			State state = new State(newstate);
			Type type = this.GetType();

			// This is some assembly magic
			// Alternatively, this could be done by using an Interface and defining the enter/exit/update/fixedupdate/lateupdate.
			// But as its only called once, when a new state is created, its not worth converting as the overhead is so low.
			state.enterMethod = StateMachine.GetMethodInfo<Action<Enum>>(this, type, newstate + "_Enter", emptyEnterExit);
			state.updateMethod = StateMachine.GetMethodInfo<Action> (this, type, newstate + "_Update", emptyUpdate);
			state.fixedUpdateMethod = StateMachine.GetMethodInfo<Action> (this, type, newstate + "_FixedUpdate", emptyUpdate);
			state.lateUpdateMethod = StateMachine.GetMethodInfo<Action> (this, type, newstate + "_LateUpdate", emptyUpdate);
			state.exitMethod = StateMachine.GetMethodInfo<Action<Enum> >(this, type, newstate + "_Exit", emptyEnterExit);
			this.states.Add(newstate, state);
			return true;
		}

		protected bool CreateNewStateWithTransitions(Enum newState, Enum[] transitions, bool forceTransition = false)
		{
			if (this.Initialized() == false) { return false; }
			if (this.CreateNewState(newState) == false) { return false; }

			State s = states[newState];
			s.forceTransition = forceTransition;
			foreach (Enum t in transitions)
			{
				if (s.transitions.Contains(t) == true)
				{
					DI_Debug.writeLog(DI_DebugLevel.HIGH, "State: " + newState + " already contains a transition for " + t + " in " + this.GetType().ToString());
					continue;
				}
				s.transitions.Add(t);
			}
			return true;
		}

		// Adds a new transition to a state.
		protected bool AddTransitionsToState(Enum newState, Enum[] transitions, bool forceTransition = false)
		{
			if (!this.Initialized()) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "State: " + newState + " is not initalized.");
				return false;
			}

			if (!this.states.ContainsKey(newState)) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "State: " + newState + " can't be added because it already exists.");
				return false;
			}

			State state = states[newState];
			state.forceTransition = forceTransition;
			for (int iteration = 0; iteration < transitions.Length; iteration++) {
				Enum transition = transitions[iteration];
				if (state.transitions.Contains(transition)) {
					DI_Debug.writeLog(DI_DebugLevel.HIGH, "State: " + newState + " already contains a transition for " + transition + " in " + this.GetType().ToString());
					continue;
				}
				//DI_Debug.writeLog(DI_DebugLevel.HIGH, "State: " + newState + " adding transistion for " + transition + " in " + this.GetType().ToString());
				state.transitions.Add(transition);
			}
			return true;
		}

		// Checks the states to see if you can move from state one to state two.
		protected bool IsLegalTransition(Enum fromstate, Enum tostate)
		{
			if (!this.Initialized()) {
				return false;
			}

			if (this.states.ContainsKey(fromstate) && this.states.ContainsKey(tostate)) {
				if (this.states[fromstate].forceTransition == true || this.states[fromstate].transitions.Contains(tostate)) {
					return true;
				}
			}
			return false;
		}

		// Changes the current state.
		// If forceTransition is passed, then the rules for allowed states is ignored.
		protected bool ChangeCurrentState(Enum newstate, bool forceTransition = false)
		{
			if (!this.Initialized()) {
				return false;
			}

			if (this.inTransition) {
				if (!this.allowMultiTransition) {
					DI_Debug.writeLog(DI_DebugLevel.MEDIUM, this.GetType().ToString() + " requests transition to state " + newstate + " when still transitioning");
					return false;
				}
				else if (this.allowMultiTransition) {
					if (this.inExitMethod) {
						DI_Debug.writeLog(DI_DebugLevel.MEDIUM, this.GetType().ToString() + " requests new state in exit method is not recommended");
						return false;
					}
					this.transitionQueue = states[newstate];
					return true;
				}
			}

			if (this.IsLegalTransition(this.currentState.name, newstate)) {
				DI_Debug.writeLog(DI_DebugLevel.INFO, this.GetType().ToString() + " transition: " + this.currentState.name + " => " + newstate);

				this.transitionSource = this.currentState;
				State transitionTarget = this.states[newstate];
				this.inTransition = true;
				this.inExitMethod = true;
				this.currentState.exitMethod(transitionTarget.name);
				this.inExitMethod = false;
				transitionTarget.enterMethod(this.currentState.name);
				this.currentState = transitionTarget;

				if (transitionTarget == null || this.transitionSource == null) {
					DI_Debug.writeLog(DI_DebugLevel.HIGH, this.GetType().ToString() + " cannot finalize transition; source or target state is null!");
				}
				else {
					this.inTransition = false;
				}
			}
			else {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, this.GetType().ToString() + " requests transition: " + this.currentState.name + " => " + newstate + " is not a defined transition!");
				return false;
			}
			// Added
			if (this.allowMultiTransition == true && this.transitionQueue !=  null) {
				State temp = this.transitionQueue;
				this.transitionQueue = null;
				this.ChangeCurrentState(temp.name);
			}

			// Set the current life cycle delegates = to the state's.
			this.onUpdate = this.currentState.updateMethod;
			this.onFixedUpdate = this.currentState.fixedUpdateMethod;
			this.onLateUpdate = this.currentState.lateUpdateMethod;
			currentStateName = newstate.ToString();
			return true;
		}

		// Removes a transition from the list of allowed transitions for a state.
		protected bool RemoveTransitionToExistingState(Enum state, Enum[]transitions) 
		{
			if (!this.states.ContainsKey(state)) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "The given state " + state + " is not a registered state");
				return false;
			}
			State existingState = this.states[state];
			for (int iteration = 0; iteration < transitions.Length; iteration++) {
				Enum transition = transitions[iteration];

				if (!this.states.ContainsKey(transition)) {
					DI_Debug.writeLog(DI_DebugLevel.HIGH, transition + " is not a registered state");
					return false;
				}
				if (!existingState.transitions.Contains(transition)) {
					DI_Debug.writeLog(DI_DebugLevel.MEDIUM, existingState.name + " does not contain " + transition);
					continue;
				}
				existingState.transitions.Remove(transition);
			}
			return true;
		}

		// Gets an array of the states that are able to be transistioned to from this state.
		protected Enum[] GetTransitionsFromState(Enum state)
		{
			// Check to see if the requested state exists.
			if (!this.states.ContainsKey(state)) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "The given state " + state + " is not a registered state");
				return null;
			}
			// Return the array of transitionable states.
			return this.states[state].transitions.ToArray(); 
		}

		// Gets the list of all the states that are tied to this machine.
		protected State[] GetAllState() 
		{
			List<State> list = new List<State> ();
			foreach(KeyValuePair<Enum, State> keyValuePair in this.states) {
				list.Add(keyValuePair.Value);
			}
			return list.ToArray();
		}

		// Returns whether or not this machine contains a state.
		protected bool ContainsState(Enum state)
		{
			return this.states.ContainsKey(state);
		}
	}
}