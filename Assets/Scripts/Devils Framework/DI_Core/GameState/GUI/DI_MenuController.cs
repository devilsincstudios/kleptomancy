// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;

using DI.Core.Behaviours;
using DI.Core.GameState;

namespace DI.Core.GameState.GUI
{
	[AddComponentMenu("Game State/Menu Controller")]
	public class DI_MenuController : DI_MonoBehaviour
	{
		public Canvas menu;
		public Canvas pause;
		public Canvas options;
		public Canvas hud;
		public Canvas cheatMenu;

		[HideInInspector]
		public static DI_MenuController instance;

		public void OnEnable()
		{
			instance = this;
		}

		public void disableAllMenus()
		{
			menu.gameObject.SetActive(false);
			pause.gameObject.SetActive(false);
			cheatMenu.gameObject.SetActive(false);
			hud.gameObject.SetActive(true);
			options.gameObject.SetActive(false);
		}
	}
}