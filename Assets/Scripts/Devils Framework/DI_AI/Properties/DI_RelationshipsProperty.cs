// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;

namespace DI.AI.Properties
{
	public struct DI_RelationshipsProperty
	{
		public List<DI_RelationshipProperty> relationships;
	}
}