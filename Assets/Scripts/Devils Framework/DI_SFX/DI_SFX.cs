// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

using DI.Core.Events;
using DI.Entities.Properties;

namespace DI.SFX
{
	public static class DI_SFX
	{
		public static Transform parentObject;
		public static int maxSounds = 100;

		[Obsolete]
		public static void playClipAtPoint(Vector3 point, DI_SFXClipProperties sfx)
		{
			playClipAtPoint(null, point, sfx);
		}

		public static void playRandomClipAtPoint(GameObject owner, Vector3 point, float distanceFromListener, DI_SFXProperty sfx)
		{
			if (sfx.hasSFX) {
				if (distanceFromListener < sfx.sfxs[0].maxDistance) {
					playClipAtPoint(owner, point, sfx.sfxs[UnityEngine.Random.Range(0, sfx.sfxs.Count)]);
				}
			}
		}

		public static void playRandomClipAtPoint(GameObject owner, Vector3 point, DI_SFXProperty sfx)
		{
			playClipAtPoint(owner, point, sfx.sfxs[UnityEngine.Random.Range(0, sfx.sfxs.Count)]);
		}

		public static void playClipAtPoint(GameObject owner, Vector3 point, DI_SFXClipProperties sfx)
		{
			try {
				if (Application.isPlaying) {
					if (parentObject == null) {
						parentObject = GameObject.Find("Audio Manager").transform;
					}

					if (parentObject.childCount < maxSounds) {
						DI_EventCenter<GameObject, Vector3, DI_SFXClipProperties>.invoke("OnPlaySFX", owner, point, sfx);
						GameObject audioClip = new GameObject("Audio SFX - " + sfx.clip.name);
						audioClip.transform.parent = parentObject;
						AudioSource source = audioClip.AddComponent<AudioSource>();
						source.clip = sfx.clip;
						source.outputAudioMixerGroup = sfx.audioGroup;
						source.volume = sfx.volume;
						audioClip.transform.position = point;
						source.minDistance = sfx.minDistance;
						source.maxDistance = sfx.maxDistance;
						source.pitch = sfx.pitch;
						source.Play();
						UnityEngine.Object.Destroy(audioClip, sfx.clip.length);
					}
				}
			}
			catch (Exception) {};
		}
	}
}