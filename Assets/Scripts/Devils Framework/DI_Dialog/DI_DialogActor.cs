// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Dialog
{
	public class DI_DialogActor : ScriptableObject
	{
		public string actorName;
		public Sprite actorPortrait;
		public RenderTexture actorRender;
	}
}