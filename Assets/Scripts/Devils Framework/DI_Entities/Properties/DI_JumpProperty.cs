﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

namespace DI.Entities.Properties
{
	[Serializable]
	public struct DI_JumpProperty
	{
		public bool inJump;
		public float maxFallSpeed;
		public bool canJump;
		public float jumpFrames;
		public bool canDoubleJump;
		public float jumpStrength;
		public bool hasDoubleJumped;
	}
}