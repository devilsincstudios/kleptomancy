﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Doors_controls : DI.Core.Behaviours.DI_MonoBehaviour
{
    public Texture2D fadeoutTexture;
    public float fadeSpeed = 0.8f;
    public int depth = -1000;
    public float alpha = 0;
    public float fadeirection = -1;
    string stars;

    bool next_door = false, change = false;
    string levelName;
	// Use this for initialization
    Rect label_wind = new Rect(0, 0, 0, 0);
	void OnTriggerStay(Collider other) {
      
        if ( other.tag =="Door") {
        label_wind = new Rect(0, 0, 500, 500);
        levelName = other.name ;
        stars = PlayerPrefs.GetString(levelName);
        next_door =true;
        //Debug.Log(levelName);
        }
	}

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Door")
        {
            label_wind = new Rect(0, 0, 0, 0);
              next_door = false;
         }
    }
	// Update is called once per frame

    IEnumerator OnLevelChange() {
        change = true;
        float fadetime = BeingFade(1);
		yield return new WaitForSeconds(fadetime);
		SceneManager.LoadScene("Level01", LoadSceneMode.Single);
    }
	

    void OnGUI() {
        Rect window = new Rect(Screen.width /2 ,120,250,250);
        GUI.Label(label_wind, "" + stars);
        if (change)
        {
           window = new Rect(0, 0, 0, 0);
            alpha += fadeirection * fadeSpeed * Time.deltaTime;
            alpha = Mathf.Clamp01(alpha);
            GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
            GUI.depth = depth;
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeoutTexture);
        }


        if (next_door && GUI.Button(window, "" + levelName))
        {

            PlayerPrefs.SetString("levelName", levelName);
                                    next_door = false;
                                    StartCoroutine( OnLevelChange());
                                    
        }
    
    }

    public float BeingFade(int direction) {
        fadeirection = direction;
        return direction;
    }
}
