﻿using UnityEngine;
using System.Collections;

public class Fade_In : MonoBehaviour {
    public Texture2D fadeoutTexture;
    public float fadeSpeed = 0.8f;
    public int depth = -1000;
    public float alpha = 0;
    public float fadeirection = -1;

	
	void OnGUI () {
        alpha -= fadeirection * fadeSpeed * Time.deltaTime;
        alpha = Mathf.Clamp01(alpha);
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = depth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeoutTexture);

   	}
}
