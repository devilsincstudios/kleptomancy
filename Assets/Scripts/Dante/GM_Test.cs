﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GM_Test : MonoBehaviour {

	// Use this for initializatio
	
	// Update is called once per frame
    string LevelName;
    void Start() {
        LevelName = PlayerPrefs.GetString("levelName"); ;
    }
	void  OnGUI  () {
        if (GUI.Button(new Rect(0, 0, 100, 100), " 5 stars"))
        {
            PlayerPrefs.SetString(LevelName, "5 stars");
        }
        if (GUI.Button(new Rect(0, 100, 100, 100), " 4 stars"))
        {
            PlayerPrefs.SetString(LevelName, "4 stars");
        }
        if (GUI.Button(new Rect(0, 200, 100, 100), " 3 stars"))
        {
            PlayerPrefs.SetString(LevelName, "3 stars");
        }
        if (GUI.Button(new Rect(0, 300, 100, 100), " Main Level"))
        {
			SceneManager.LoadScene("Mainlevel", LoadSceneMode.Single);
        }
	}
}
